/**
 * @file
 * Datepicker Plugin.
 */

(function(theme, $) {

  theme = theme || {};

  var pluginName = 'datepicker';

  var Datepicker = function($element, options) {
    return this.init($element, options);
  };

  Datepicker.DEFAULTS = {};

  Datepicker.prototype = {
    init: function($element, options) {
      this.$element = $element;
      this.setDefaults();
      this.setData();
      this.setOptions(options);
      this.setSkin();
      this.addDatepicker();

      return this;
    },

    setData: function() {
      this.$element.data(window.instPrefix + pluginName, this);

      return this;
    },

    setDefaults: function () {
      this.defaults = Datepicker.DEFAULTS;
            return this;
          },

    setOptions: function(options) {
      this.options = $.extend(true, {}, this.defaults, options);
      return this;
    },

    setSkin: function() {
      this.skin = this.options.pluginSkin;
      return this;
    },

    addDatepicker: function() {
      this.$element.datepicker(this.options);

      if (!!this.skin) {
        var datepicker = this.$element.data('datepicker');
        if (datepicker.picker) {
          datepicker.picker.addClass('datepicker-' + this.skin);
        }
        else {
              $.each(datepicker.pickers, function(i, obj){
                obj.picker.addClass('datepicker-' + this.skin);
              });
        }
      }

      return this;
    }
  };

  // Expose to scope.
  $.extend(theme, {
    Datepicker: Datepicker
  });

  // Jquery plugin.
  $.fn.clDatepicker = function(options) {
    return this.map(function() {
      var $this = $(this);
      if (!$this.data(window.instPrefix + pluginName)) {
        new Datepicker($this, options);
      }
      else {
           $this.data(window.instPrefix + pluginName);
      }
    });
  };

}).apply(this, [ window.theme, jQuery ]);
