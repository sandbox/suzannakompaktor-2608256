/**
 * @file
 * TextArea AutoSize.
 */

(function(theme, $) {

  theme = theme || {};

  var pluginName = 'autosize';

  var AutoSize = function($element) {
    return this.init($element);
  };

  AutoSize.DEFAULTS = {};

  AutoSize.prototype = {
    init: function($element) {
      this.$element = $element;
      autosize(this.$element);

      return this;
    },
  };

  // Expose to scope.
  $.extend(theme, {
    AutoSize: AutoSize
  });

  // Jquery plugin.
  $.fn.clAutoSize = function() {
    return this.each(function() {
      var $this = $(this);
      new AutoSize($this);
    });
  };

}).apply(this, [ window.theme, jQuery ]);
