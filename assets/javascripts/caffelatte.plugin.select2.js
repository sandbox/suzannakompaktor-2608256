/**
 * @file
 * Select2 Plugin.
 */

(function(theme, $) {

  theme = theme || {};

  var pluginName = 'select2';

  var Select2 = function($element, options) {
    return this.init($element, options);
  };

  Select2.DEFAULTS = {};

  Select2.prototype = {
    init: function($element, options) {
      this.$element = $element;
      this.setDefaults();
      this.setData();
      this.setOptions(options);
      this.addSelect2();

      return this;
    },

    setData: function() {
      this.$element.data(window.instPrefix + pluginName, this);

      return this;
    },

    setDefaults: function () {
      this.defaults = Select2.DEFAULTS;
            return this;
          },

    setOptions: function(options) {
      this.options = $.extend(true, {}, this.defaults, options);
      return this;
    },

    addSelect2: function() {
      this.$element.select2(this.options);
      return this;
    }
  };

  // Expose to scope.
  $.extend(theme, {
    Select2: Select2
  });

  // Jquery plugin.
  $.fn.clSelect2 = function(options) {
    return this.map(function() {
      var $this = $(this);
      if (!$this.data(window.instPrefix + pluginName)) {
        new Select2($this, options);
      }
      else {
           $this.data(window.instPrefix + pluginName);
      }
    });
  };

}).apply(this, [ window.theme, jQuery ]);
