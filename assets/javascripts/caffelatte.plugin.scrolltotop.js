/**
 * @file
 * ScrollToTop Plugin.
 */

(function(theme, $) {

  theme = theme || {};

  var PluginScrollToTop = function($el, opts) {
    return this.initialize($el, opts);
  };

  PluginScrollToTop.DEFAULTS = {
    offset: 150,
    delay: 800,
    visibleMobile: false,
    label: false
  };

  PluginScrollToTop.prototype = {
    initialize: function($el, opts) {
      initialized = true;
      this.$el = $el;

      this
        .setOptions(opts)
        .build()
        .events();

      return this;
    },

    setOptions: function(opts) {
      this.options = $.extend(true, {}, PluginScrollToTop.DEFAULTS, opts);

      return this;
    },

    build: function() {
      var self = this;

      // Visible Mobile.
      if (!self.options.visibleMobile) {
        this.$el.addClass('hidden-xs');
      }

      return this;
    },

    events: function() {
      var self = this,
                _isScrolling = false;

      // Click Element Action.
      self.$el.on('click', function(e) {
        e.preventDefault();
        $('body, html').animate({
          scrollTop: 0
        }, self.options.delay);
        return false;
      });

      // Show/Hide Button on Window Scroll event.
      $(window).scroll(function() {

        if (!_isScrolling) {

          _isScrolling = true;

          if ($(window).scrollTop() > self.options.offset) {

            self.$el.stop(true, true).addClass('show');
            _isScrolling = false;

          }
          else {

            self.$el.stop(true, true).removeClass('show');
            _isScrolling = false;

          }

        }

      });

      return this;
    }
  }

  // Expose to scope.
  $.extend(theme, {
    PluginScrollToTop: PluginScrollToTop
  });

  // Jquery plugin.
  $.fn.clPluginScrollToTop = function(options) {
    return this.map(function() {
      var $this = $(this);
      new PluginScrollToTop($this, options);
    });
  };

}).apply(this, [ window.theme, jQuery ]);
