/**
 * @file
 * Timepicker Plugin.
 */

(function(theme, $) {

  theme = theme || {};

  var pluginName = 'timepicker';

  var Timepicker = function($element, options) {
    return this.init($element, options);
  };

  Timepicker.DEFAULTS = {};

  Timepicker.prototype = {
    init: function($element, options) {
      this.$element = $element;
      this.setDefaults();
      this.setData();
      this.setOptions(options);
      this.addTimepicker();

      return this;
    },

    setData: function() {
      this.$element.data(window.instPrefix + pluginName, this);

      return this;
    },

    setDefaults: function () {
      this.defaults = Timepicker.DEFAULTS;
            return this;
    },

    setOptions: function(options) {
      this.options = $.extend(true, {}, this.defaults, options);
      return this;
    },

    addTimepicker: function() {
      this.$element.timepicker(this.options);
      return this;
    }
  };

  // Expose to scope.
  $.extend(theme, {
    Timepicker: Timepicker
  });

  // Jquery plugin.
  $.fn.clTimepicker = function(options) {
    return this.map(function() {
      var $this = $(this);
      if (!$this.data(window.instPrefix + pluginName)) {
        new Timepicker($this, options);
      }
      else {
           $this.data(window.instPrefix + pluginName);
      }
    });
  };

}).apply(this, [ window.theme, jQuery ]);
