/**
 * @file
 * Data Tables - Config.
 */

(function($) {

  'use strict';

  // We overwrite initialize of all datatables here
  // because we want to use select2, give search input a bootstrap look
  // keep in mind if you overwrite this fnInitComplete somewhere,
  // you should run the code inside this function to keep functionality.
  //
  // there's no better way to do this at this time :(.
  if ($.isFunction($.fn[ 'dataTable' ])) {

    $.extend(true, $.fn.dataTable.defaults, {
      sDom: "<'row datatables-header form-inline'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>r><'table-responsive't><'row datatables-footer'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",
      oLanguage: {
        sLengthMenu: '_MENU_ records per page',
        sProcessing: '<i class="fa fa-spinner fa-spin"></i> Loading'
      },
      fnInitComplete: function(settings, json) {
        // Select 2.
        if ($.isFunction($.fn[ 'select2' ])) {
          $('.dataTables_length select', settings.nTableWrapper).select2({
            minimumResultsForSearch: -1
          });
        }

        // Search.
        var $input = $('.dataTables_filter input', settings.nTableWrapper)
          .attr({
            placeholder: 'Search...'
          });
          $('.dataTables_filter label', settings.nTableWrapper).contents().filter(function(){  return this.nodeType != 1;}).remove();
      }
    });

  }

}).apply(this, [ jQuery ]);
