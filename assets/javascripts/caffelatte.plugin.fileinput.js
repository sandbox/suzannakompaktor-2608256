/**
 * @file
 * File input Plugin.
 */

(function(theme, $) {

  theme = theme || {};

  var pluginName = 'fileinput';

  var FileInput = function($element, options) {
    return this.init($element, options);
  };

  FileInput.DEFAULTS = {
    browseIcon: '<i class="fa fa-fw fa-folder-open"></i>',
    removeIcon: '<i class="fa fa-fw fa-trash"></i>',
    cancelIcon: '<i class="fa fa-fw fa-ban"></i>',
    uploadIcon: '<i class="fa fa-fw fa-upload"></i>',
    msgValidationErrorIcon: '<i class="fa fa-fw fa-times-circle"></i> ',
    captionTemplate : '<div tabindex="-1" class="form-control file-caption {class}">\n' +
                '   <span class="fa fa-file-o text-muted kv-caption-icon"></span><div class="file-caption-name"></div>\n' +
                '</div>',
    progressClass: 'progress-bar progress-bar-info progress-bar-striped active',
    progressCompleteClass: 'progress-bar progress-bar-success',
    zoomIndicator: '<i class="fa fa-fw fa-search-plus"></i>',
    fileActionSettings: {
        removeIcon: '<i class="fa fa-fw fa-trash"></i>',
        uploadIcon: '<i class="fa fa-fw fa-upload"></i>',
        indicatorNew: '<i class="fa fa-fw fa-plus-circle text-warning"></i>',
        indicatorSuccess: '<i class="fa fa-fw fa-check text-success"></i>',
        indicatorError: '<i class="fa fa-fw fa-times-circle"></i>',
        indicatorLoading: '<i class="fa fa-fw fa-refresh text-muted"></i>',
     },
    layoutTemplates: {
      icon: '<span class="fa fa-fw fa-file-o kv-caption-icon"></span> '
     }
  };

  FileInput.prototype = {
    init: function($element, options) {
      this.$element = $element;
      this.setDefaults();
      this.setData();
      this.setOptions(options);
      this.addFileInput();

      return this;
    },

    setData: function() {
      this.$element.data(window.instPrefix + pluginName, this);

      return this;
    },

    setDefaults: function () {
      this.defaults = FileInput.DEFAULTS;
            return this;
          },

    setOptions: function(options) {
      this.options = $.extend(true, {}, this.defaults, options);

      return this;
    },

    addFileInput: function() {
      this.$element.fileinput(this.options);
      return this;
    }
  };

  // Expose to scope.
  $.extend(theme, {
    FileInput: FileInput
  });

  // Jquery plugin.
  $.fn.clFileInput = function(options) {
    return this.map(function() {
      var $this = $(this);
      if (!$this.data(window.instPrefix + pluginName)) {
        new FileInput($this, options);
      }
      else {
           $this.data(window.instPrefix + pluginName);
      }
    });
  };

}).apply(this, [ window.theme, jQuery ]);
