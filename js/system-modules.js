/**
 * @file
 * System module page behaviors.
 */

(function ($) {

  "use strict";

  /**
   * Create "show more"/"show less".
   */
  Drupal.behaviors.caffelatteSystemModuleShowMore = {
    attach: function (context, settings) {
      $('td.description', context).once('caffelatte-show-more', function() {
        var $this = $(this);

        $this.addClass('collapsed');
        var is_truncated = Drupal.truncateString($this.find('span.description'), 80);

        if (is_truncated || $this.find('.admin-requirements').length > 0) {
          $this.append($('<a href="#" class="view-more">' + Drupal.t('Show more') + '</a>'));
        }

        $this.on('click', '.view-more', function(event) {
          event.preventDefault();
          if ($this.hasClass('collapsed')) {
            $(this).text(Drupal.t('Show less'));
            $this.find('span.description .ellipsis').hide();
            $this.find('span.description .all').show();
            $this.find('.admin-requirements').removeClass('hidden');
            $this.removeClass('collapsed');
          }
          else {
            $(this).text(Drupal.t('Show more'));
            $this.find('span.description .ellipsis').show();
            $this.find('span.description .all').hide();
            $this.find('.admin-requirements').addClass('hidden');
            $this.addClass('collapsed');
          }
        });

      });
    }
  };

})(jQuery);
