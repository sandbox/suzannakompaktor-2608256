/**
 * @file
 * clientside_validation.js
 *
 * Custom behavior for Clientside Validation module
 * Add CaffeLatte error styles.
 */

(function ($) {

  "use strict";

  Drupal.behaviors.caffelatteClientsideValidation = {
    attach: function (context) {
      if (Drupal.settings.clientsideValidation.forms !== undefined) {
        $.each(Drupal.settings.clientsideValidation.forms, function (f) {
          var self = this;
          Drupal.settings.clientsideValidation.forms[f].general.errorClass = 'has-error';
          $('#' + f).validate({
            // Add custom highlight function.
            highlight: function(element) {

              $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

              // Sort the classes out for the tabs.
              var tab = Drupal.findVerticalTab(element);
              if (tab) {
                tab.addClass('has-error').removeClass('has-success');
              }
              tab = Drupal.findHorizontalTab(element);
              if (tab) {
                tab.addClass('has-error').removeClass('has-success');
              }
              var fieldset = $(element).parents('fieldset.collapsible');
              if (fieldset !== undefined) {
                fieldset.find('legend').addClass('has-error').removeClass('has-success');
              }

            },
            // Add custom unhighlight function.
            unhighlight: function(element) {
              var tab;

              $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');

              var fieldset = $(element).parents('fieldset.vertical-tabs-pane');
              if (fieldset.size() && fieldset.find('.has-error').not('label').size() === 0) {
                tab = Drupal.findVerticalTab(element);
                if (tab) {
                  tab.removeClass('has-error').addClass('has-success');
                }
              }

              // Same for horizontal tabs.
              fieldset = $(element).parents('fieldset.horizontal-tabs-pane');
              if (fieldset.size() && fieldset.find('.has-error').not('label').size() === 0) {
                tab = Drupal.findHorizontalTab(element);
                if (tab) {
                  tab.removeClass('has-error').addClass('has-success');
                }
              }

              fieldset = $(element).parents('fieldset.collapsible');
              if (fieldset !== undefined) {
                fieldset.find('legend').removeClass('has-error').addClass('has-success');
              }

            }
          });
        });
      }
    }
  };

})(jQuery);
