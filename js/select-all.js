/**
 * @file
 * Custom checkbox style on "select all".
 */

(function ($) {
  /**
   * Add custom checkbox wrapper to select all checkboxes if custom checkbox is allowed.
   */
  Drupal.behaviors.caffelatteSelectAll = {
    attach: function (context, settings) {
      $('th.select-all, td.simpletest-select-all', context).once('caffelatte-select-all', function () {
        if (settings.caffelatte.customCheckbox) {
          var $checkbox = $(this).find('input[type="checkbox"]');
          $checkbox.wrap('<div class="checkbox-custom checkbox-default"></div>');
          $checkbox.closest('div').append('<label></label>');
        }
      });
      }
  };

})(jQuery);
