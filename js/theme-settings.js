/**
 * @file
 * Theme settings.
 */

(function ($) {

  Drupal.behaviors.caffelatteSettingsAutocomplete = {
    attach: function (context, settings) {
      var clIcon = Drupal.settings.clIcon;
      var iconClass = '.' + clIcon;

      $('.form-item-caffelatte-autocomplete-icon, .form-item-caffelatte-autocomplete-icon-active').find('select').change(function(event) {
        var $icon = $(this).closest('div').find(iconClass);
        var icon_class_classes = $icon.attr('class').split(' ');
        icon_class_classes[1] = clIcon + '-' + $(this).find('option:selected').val();
        $icon.removeClass().addClass(icon_class_classes.join(' '));
      });
      /*$('.form-item-caffelatte-autocomplete-icon-anim').find('select').change(function(event) {
      $icon = $(this).closest('div').find(iconClass);
      var icon_class_classes = $icon.attr('class').split(' ');
      icon_class_classes[3] = clIcon + '-' + $(this).find('option:selected').val();
      $icon.removeClass().addClass(icon_class_classes.join(' '));
      });*/

    }
  };

  Drupal.behaviors.caffelatteSettingsAccordation = {
    attach: function (context, settings) {
      var clIcon = Drupal.settings.clIcon;
      var iconClass = '.' + clIcon;
      $('.form-item-caffelatte-accordation-icon-collapsed, .form-item-caffelatte-accordation-icon-expanded').find('select').change(function(event) {
        $icon = $(this).closest('div').find(iconClass);
        var icon_class_classes = $icon.attr('class').split(' ');
        icon_class_classes[1] = clIcon + '-' + $(this).find('option:selected').val();
        $icon.removeClass().addClass(icon_class_classes.join(' '));
      });
    }
  };

  Drupal.behaviors.caffelatteSettingsSpinner = {
    attach: function (context, settings) {
      var clIcon = Drupal.settings.clIcon;
      var iconClass = '.' + clIcon;
      var $increase_icon = $('input[name="caffelatte_spinbox_increase_icon"]');
      var $decrease_icon = $('input[name="caffelatte_spinbox_decrease_icon"]');
      var $demo = $('.form-item-spinbox-demo');
      var $demo_increase = $demo.find('.spinbox-up ' + iconClass);
      var $demo_decrease = $demo.find('.spinbox-down ' + iconClass);
      var increase_icon_default_style = 'angle-up';
      var decrease_icon_default_style = 'angle-down';
      var increase_icon_alter_style = 'plus';
      var decrease_icon_alter_style = 'minus';

      $('.form-item-caffelatte-spinbox-style').find('select').change(function(event) {
        if ($(this).val() == 'default') {
          $increase_icon.val(increase_icon_default_style);
          $decrease_icon.val(decrease_icon_default_style);

          var icon_class_classes_increase = $demo_increase.attr('class').split(' ');
          icon_class_classes_increase[1] = clIcon + '-' + increase_icon_default_style;
          $demo_increase.removeClass().addClass(icon_class_classes_increase.join(' '));

          var icon_class_classes_decrease = $demo_decrease.attr('class').split(' ');
          icon_class_classes_decrease[1] = clIcon + '-' + decrease_icon_default_style;
          $demo_decrease.removeClass().addClass(icon_class_classes_decrease.join(' '));
        }
        else {
          $increase_icon.val(increase_icon_alter_style);
          $decrease_icon.val(decrease_icon_alter_style);

          var icon_class_classes_increase = $demo_increase.attr('class').split(' ');
          icon_class_classes_increase[1] = clIcon + '-' + increase_icon_alter_style;
          $demo_increase.removeClass().addClass(icon_class_classes_increase.join(' '));

          var icon_class_classes_decrease = $demo_decrease.attr('class').split(' ');
          icon_class_classes_decrease[1] = clIcon + '-' + decrease_icon_alter_style;
          $demo_decrease.removeClass().addClass(icon_class_classes_decrease.join(' '));
        }
      });
    }
  };

})(jQuery);
