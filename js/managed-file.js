/**
 * @file
 * Init fileinput plugin for managed_file.
 *
 * @see caffelatte.plugin.fileinput.js
 */

(function ($) {

  Drupal.behaviors.caffelatteManagedFile = {
    attach: function (context, settings) {
      if ($.isFunction($.fn[ 'fileinput' ]) && $.isFunction($.fn[ 'clFileInput' ])) {

        $.each(Drupal.settings.caffelatte.file.elements, function(selector) {
          var $input = $(selector, context);
          var $wrapper = $input.closest('.form-managed-file');
          var options = $.extend(true,
            settings.caffelatte.fileinput,
            settings.caffelatte.file.elements[selector]
          );

          var html5data = (typeof $.html5data === 'function' ? $input.html5data('plugin-fileinput') : {});
          options = $.extend(true, options, $input.data('plugin-options'), html5data);

          $input.clFileInput(options);

          var $remove = $wrapper.find('.file-managed-file-remove-button');
          var $upload = $wrapper.find('.file-managed-file-upload-button');
          $upload.addClass('hidden');
          $remove.addClass('hidden');
          $input.on('filedeleted', function(event, key) {
            $remove.mousedown();
          });
          $input.on('filebatchselected', function(event, key) {
            $upload.mousedown();
          });
        });
      }
    }
  };

})(jQuery);
