/**
 * @file
 * Panels behaviors.
 */

(function($) {

  "use strict";

  /**
   * Attaches caffelattePanel behavior to all panel items.
   */
  Drupal.behaviors.caffelattePanel = {
    attach: function (context, settings) {
      $('.panel', context).once('caffelatte-panel', function() {
        var clIcon = Drupal.settings.clIcon;
        var iconClass = '.' + clIcon;
        var icons = {
          collapsed: clIcon + '-caret-right',
          expanded: clIcon + '-caret-down'
        };
        var actions = {
          $close: $(this).find('> .panel-heading .panel-actions .panel-actions-close'),
          $toggleCollapse: $(this).find('> .panel-heading .panel-actions .panel-actions-collapse')
        };

        new Drupal.clPanel($(this), actions, icons);

      });
    }
  };

  /**
 * Panel object.
 */
  Drupal.clPanel = function ($panel, actions, icons) {
    var panel = this;
    this.$panel = $panel;
    this.actions = $.extend(true, {}, this.actions, actions);
    this.icons = $.extend(true, {}, this.icons, icons);
    var $close = this.actions.$close;
    var $toggleCollapse = this.actions.$toggleCollapse;

    if ($panel.hasClass('collapsed')) {
      $toggleCollapse.removeClass(this.icons.expanded).addClass(this.icons.collapsed);
    }

    if ($toggleCollapse !== undefined) {
      $toggleCollapse.on('click', function(event) {
        event.preventDefault();
        panel.toggleCollapse($(this));
      });
    }

    if ($close !== undefined) {
      $close.on('click', function(event) {
        event.preventDefault();
        panel.close();
      });
    }

  };

  /**
   * Toggle Panel.
   */
  Drupal.clPanel.prototype.toggleCollapse = function (target) {
    var $target = $(target);
    this.$panel.find('> .panel-body, > .panel-footer').slideToggle();
    this.$panel.toggleClass('collapsed');
    if (this.$panel.hasClass('collapsed')) {
      $target.removeClass(this.icons.expanded).addClass(this.icons.collapsed);
    }
    else {
        $target.removeClass(this.icons.collapsed).addClass(this.icons.expanded);
    }
  }

  /**
   * Close Panel.
   */
  Drupal.clPanel.prototype.close = function () {
    this.$panel.hide();
  }

})(jQuery);
