/**
 * @file
 * Custom behavior for password field.
 */

(function ($) {

  Drupal.behaviors.caffelattePassword = {
    attach: function (context, settings) {
      var translate = settings.password;
      $('input.password-field', context).once('caffelatte-password', function () {

        var passwordInput = $(this);
        var innerWrapper = $(this).parent();
        // Display password suggestion as alert in warning style.
        var passwordSuggestions = $('.password-suggestions');
        passwordSuggestions.addClass('alert alert-warning');

        $('input.password-confirm').after('<div class="password-confirm">' + translate['confirmTitle'] + ' <span></span></div>').addClass('confirm-parent');
         var confirmInput = $('input.password-confirm');
         var confirmResult = $('div.password-confirm');
         var confirmChild = $('span', confirmResult);

        var passwordCheck = function () {

            // Evaluate the password strength.
            var result = Drupal.evaluatePasswordStrength(passwordInput.val(), settings.password);
            // Add color styles.
            var indicatorClass = 'indicator-danger';

            if (result.strength < 60) {
            indicatorClass = 'indicator-danger';
            }
            else if (result.strength < 70) {
                     indicatorClass = 'indicator-warning';
            }
            else if (result.strength < 80) {
                    indicatorClass = 'indicator-success';
            }
            else if (result.strength <= 100) {
                    indicatorClass = 'indicator-info';
            }

            $(innerWrapper).find('.indicator').removeClass().addClass('indicator').addClass(indicatorClass);
         };

          passwordInput.keyup(passwordCheck).focus(passwordCheck).blur(passwordCheck);
      });
    }
  };

})(jQuery);
