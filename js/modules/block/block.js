/**
 * @file
 * Custom Block behaviors.
 */

(function ($) {

  /**
   * Move a block in the blocks table from one region to another via select list.
   *
   * This behavior is dependent on the tableDrag behavior, since it uses the
   * objects initialized in that behavior to update the row.
   */
  Drupal.behaviors.caffelatteBlockDrag = {
    attach: function (context, settings) {
      // tableDrag is required and we should be on the blocks admin page.
      if (typeof Drupal.tableDrag == 'undefined' || typeof Drupal.tableDrag.blocks == 'undefined') {
        return;
      }

      var table = $('table#blocks');
      var tableDrag = Drupal.tableDrag.blocks;
      // Get the blocks tableDrag object.
      // Add a handler so when a row is dropped, update fields dropped into new regions.
      tableDrag.onDrop = function () {
        dragObject = this;
        // Use "region-message" row instead of "region" row because
        // "region-{region_name}-message" is less prone to regexp match errors.
        var regionRow = $(dragObject.rowObject.element).prevAll('tr.region-message').get(0);
        var regionName = regionRow.className.replace(/([^ ]+[ ]+)*region-([^ ]+)-message([ ]+[^ ]+)*/, '$2');
        var regionField = $('select.block-region-select', dragObject.rowObject.element);

        // Check whether the newly picked region is available for this block.
        if ($('option[value=' + regionName + ']', regionField).length == 0) {
          // If not, alert the user and keep the block in its old region setting.
          alert(Drupal.t('The block cannot be placed in this region.'));
          // Simulate that there was a selected element change, so the row is put
          // back to from where the user tried to drag it.
          regionField.change();
        }
        else if ($(dragObject.rowObject.element).prev('tr').is('.region-message')) {
          var weightField = $('select.block-weight', dragObject.rowObject.element);
          var oldRegionName = weightField[0].className.replace(/([^ ]+[ ]+)*block-weight-([^ ]+)([ ]+[^ ]+)*/, '$2');

          if (!regionField.is('.block-region-' + regionName)) {
            regionField.removeClass('block-region-' + oldRegionName).addClass('block-region-' + regionName);
            weightField.removeClass('block-weight-' + oldRegionName).addClass('block-weight-' + regionName);
            // Change dropdown value ( add change() ).
            regionField.val(regionName).change();
          }
        }
      };
    }
  };

})(jQuery);
