/**
 * @file
 * Autoupload managed_file files.
 */

(function ($) {

  Drupal.behaviors.autoUpload = {
    attach: function (context, settings) {
      $('.form-managed-file').on('change', '.form-file', function(e){
        e.preventDefault();
        var $this = $(this);
        $this.closest('.form-managed-file').find('.file-managed-file-upload-button').mousedown();
      });
      }
  };

})(jQuery);
