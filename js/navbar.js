/**
 * @file
 * Navbar behaviors.
 */

(function ($) {
  Drupal.behaviors.caffelatteNavbar = {
    attach: function (context, settings) {
      // Dropdown on hover.
      $('.navbar-nav .dropdown').hover(function(){
      	$('[data-toggle="dropdown"]', this).trigger('click');
      });
    }
  };

})(jQuery);
