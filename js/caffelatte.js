/**
 * @file
 * Caffelatte main scripts.
 */

(function ($) {

  "use strict";

  Drupal.caffelatte = Drupal.caffelatte || {};

  $.extend(Drupal.caffelatte, {
    navbarHeight: function () {
      var offsetheight = $(".navbar-fixed-top").height();
      // Add toolbar and admin-menu height.
      offsetheight += $("#toolbar").height();
      offsetheight += $("#admin-menu").height();
      return offsetheight;
    }
  });

/**
 * Bootstrap.
 */
  Drupal.behaviors.caffelatteBootstrap = {
    attach: function (context, settings) {
      // Init tooltip.
      if ($.isFunction($.fn['tooltip'])) {
        $('[data-toggle=tooltip],[rel=tooltip]').tooltip({ container: 'body' });
      }
      // Init popover.
      if ($.isFunction($.fn['popover'])) {
        $('[data-toggle=popover]').popover();

        $(window).on('scroll', function(){
          $('[data-toggle=popover]').popover('hide');
        });
      }
    }
  };


/**
 * Scroll to top button.
 */
  Drupal.behaviors.caffelatteScrollToTop = {
    attach: function (context, settings) {
      if ($.isFunction($.fn[ 'clPluginScrollToTop' ])) {
        $('a[rel="scroll-to-top"]', context).clPluginScrollToTop();
      }
    }
  };

  Drupal.findHorizontalTab = function(element) {
    element = $(element);

    // Check for the vertical tabs fieldset and the verticalTab data.
    var fieldset = element.parents('fieldset.horizontal-tabs-pane');
    if ((fieldset.size() > 0) && (typeof(fieldset.data('horizontalTab')) !== 'undefined')) {
      var tab = $(fieldset.data('horizontalTab').item[0]);
      if (tab.size()) {
        return tab;
      }
     }

    // Return null by default.
    return null;
  };

  Drupal.findVerticalTab = function(element) {
    element = $(element);

    // Check for the vertical tabs fieldset and the verticalTab data.
    var fieldset = element.parents('fieldset.vertical-tabs-pane');
    if ((fieldset.size() > 0) && (typeof(fieldset.data('verticalTab')) !== 'undefined')) {
      var tab = $(fieldset.data('verticalTab').item[0]);
      if (tab.size()) {
        return tab;
      }
    }

    // Return null by default.
    return null;
  };

  Drupal.truncateString = function(element, chars) {
    var t = element.text();
    if (t.length < chars) {
      return false; }
    element.html(t.slice(0,chars) + '<span class="ellipsis">... </span><span class="all" style="display:none;">' + t.slice(100,t.chars) + '</span>');
    return true;
  };

  Drupal.theme.dropdownIcon = function () {
    var clIcon = Drupal.settings.clIcon;
    return '<i class="' + clIcon + ' custom-caret"></i>';
  };

  Drupal.theme.bootstrapDropdownMenu = function (buttonText, buttonClasses, items, classes) {
    var classes = (classes.length > 0) ? ' ' + classes : '';
    var output = '';
        output += '<a data-toggle="dropdown" class="' + buttonClasses + '">' + buttonText + Drupal.theme.dropdownIcon() + '</a>';
        output += '<ul class="dropdown-menu' + classes + '">';
        for (var i = 0; i < items.length; i++) {
          output += '<li>' + items[i] + '</li>';
        }
        output += '</ul>'

        return output;
  };

})(jQuery);
