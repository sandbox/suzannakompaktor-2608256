/**
 * @file
 * Accordation behaviors.
 */

(function($) {

  "use strict";

  /**
   * Attaches caffelatteAccordation behavior to all accordation items.
   */
  Drupal.behaviors.caffelatteAccordation = {
    attach: function (context, settings) {
      $('.panel-accordation', context).once('caffelatte-accordation', function() {
        var $actionContainer = $(this).find('> .panel-heading .accordation-actions');

        var actions = {
          $container: $actionContainer,
          $collapsed: $actionContainer.find('.accordation-icon-collapsed'),
          $expanded: $actionContainer.find('.accordation-icon-expanded')
        };

        new Drupal.clAccordation($(this), actions);

      });
    }
  };

  /**
 * Accordation object.
 */
  Drupal.clAccordation = function ($panel, actions) {
    this.$panel = $panel;
    this.actions = actions;
    var accordation = this;

    accordation.actions.$container.each(function() {
      accordation.toggle(accordation.isExpanded());
    });

    accordation.$panel.on('show.bs.collapse', function() {
      accordation.toggle(true);
    });

    accordation.$panel.on('hide.bs.collapse', function() {
      accordation.toggle(false);
    });
  };

  /**
   * Toggle Accordation.
   */
  Drupal.clAccordation.prototype.toggle = function (isActive, actions) {
    var actions = actions || this.actions;

    if (isActive) {
      this.actions.$collapsed.hide();
      this.actions.$expanded.show();
    }
    else {
        this.actions.$collapsed.show();
        this.actions.$expanded.hide();
    }
  }

  /**
   * Is accordation item expanded.
   */
  Drupal.clAccordation.prototype.isExpanded = function () {
    return this.$panel.find('> .accordation-body').hasClass('in');
  }

})(jQuery);
