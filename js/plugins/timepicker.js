/**
 * @file
 * Init timepicker plugin.
 *
 * @see caffelatte.plugin.timepicker.js
 */

(function ($) {

  Drupal.behaviors.caffelatteTimepicker = {
    attach: function (context, settings) {
      if ($.isFunction($.fn[ 'timepicker' ]) && $.isFunction($.fn[ 'clTimepicker' ])) {
        $('[data-plugin-timepicker]', context).once('caffelatte-timepicker', function() {
          $(this).each(function() {
            var $this = $(this);
            var options = settings.caffelatte.timepicker;
            var html5data = (typeof $.html5data === 'function' ? $this.html5data('plugin-timepicker') : {});
            options = $.extend(true, options, $this.data('plugin-options'), html5data);

            $this.clTimepicker(options);
          });
        });
      }
    }
  };

})(jQuery);
