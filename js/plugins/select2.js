/**
 * @file
 * Init select2 plugin.
 *
 * @see caffelatte.plugin.select2.js
 */

(function ($) {

  Drupal.behaviors.caffelatteSelect2 = {
    attach: function (context, settings) {
      if ($.isFunction($.fn[ 'select2' ]) && $.isFunction($.fn[ 'clSelect2' ])) {
        $('.custom-select, [data-plugin-select2]', context)
        .filter(function() {
          if ($(this).hasClass('chosen-enable') || $(this).hasClass('chosen-processed')) {
            return false;
          }
          else {
            return true;
          }
        })
        .once('caffelatte-select2', function() {
          $(this).each(function() {
            var $this = $(this);
            var options = settings.caffelatte.select2;
            var html5data = (typeof $.html5data === 'function' ? $this.html5data('plugin-select2') : {});
            options = $.extend(true, options, $this.data('plugin-options'), html5data);

            $this.clSelect2(options);
          });
        });
      }
    }
  };

})(jQuery);
