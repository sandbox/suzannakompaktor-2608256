/**
 * @file
 * Init lightbox plugin.
 */

(function ($) {

  Drupal.behaviors.caffelatteLightbox = {
    attach: function (context, settings) {
      if ($.isFunction($.fn[ 'magnificPopup' ])) {
        /* Modal Dismiss */
        $(document).on('click mouseup', '.mfp .modal-dismiss, .mfp-hide .modal-dismiss', function (e) {
          e.preventDefault();
          $.magnificPopup.close();
        });

        $('[data-plugin-lightbox], .lightbox:not(.manual)', context).once('caffelatte-lightbox', function() {
          $(this).each(function() {
            var $this = $(this);
            var options = settings.caffelatte.lightbox;
            var html5data = (typeof $.html5data === 'function' ? $this.html5data('plugin-lightbox') : {});
            options = $.extend(true, options, $this.data('plugin-options'), html5data);

            $this.clLightbox(options);

          });
        });

        if (jQuery.type(Drupal.file) !== "undefined") {
          $('div.form-managed-file .file a, .file-widget .file a', context).unbind('click', Drupal.file.openInNewWindow);
        }
      }
    }
  };

})(jQuery);
