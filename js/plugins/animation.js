/**
 * @file
 * Init animation plugin.
 *
 * @see caffelatte.plugin.animation.js
 */

(function ($) {

  Drupal.behaviors.caffelatteAnimation = {
    attach: function (context, settings) {
      if ($.isFunction($.fn[ 'appear' ]) && $.isFunction($.fn[ 'clAnimation' ])) {
        $('[data-plugin-animation], [data-appear-animation]', context).once('caffelatte-animation', function() {
          $(this).each(function() {
            var $this = $(this);
            var options = settings.caffelatte.animate;
            var html5data = (typeof $.html5data === 'function' ? $this.html5data('plugin-animation') : {});
            options = $.extend(true, options, $this.data('plugin-options'), html5data);

            $this.clAnimation(options);
          });
        });
      }
    }
  };

})(jQuery);
