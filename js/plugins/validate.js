/**
 * @file
 * Validate.
 */

(function ($) {

  Drupal.behaviors.caffelatteValidate = {
    attach: function (context, settings) {
      if ($.isFunction($.fn[ 'validate' ])) {
        $('form.client-validation', context).once('caffelatte-validate', function() {
          Drupal.caffelatteValidation($(this));
        });
      }
    }
  };

  Drupal.caffelatteValidation = function(f) {
    f.validate({
      invalidHandler: function(event, validator) {
        var $errorContainer = $(this).find('.form-error-container');
        // 'this' refers to the form.
        var errors = validator.numberOfInvalids();
        if (errors) {
          var message = $('<ul/>').addClass('list-unstyled');
          $.each(validator.errorList, function(key, value) {
            var $el = $(value.element);
            var $label = $el.closest('.form-group').find('.control-label');
            $label.find('span').remove();
            var label = $label.text() || '';
            var item = $('<li/>').html('<strong>' + label + ': </strong>' + value.message);
            message.append(item);
          });
          $errorContainer.addClass('alert alert-danger').find('.content').html(message);

          var x = $(validator.errorList[0].element).offset().top - 60;
          x -= $('.navbar-fixed-top').height();
          $('html, body').addClass('cv-scrolling').animate(
          {scrollTop: x},
          {
            duration: 800,
            complete: function () {
              $('html, body').removeClass('cv-scrolling')
            }
            }
          );

          $(validator.errorList[0].element).focus();
        }
      },
      highlight: function(element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');

        // Sort the classes out for the tabs.
        var tab = Drupal.findVerticalTab(element);
        if (tab) {
          tab.addClass('has-error').removeClass('has-success');
        }
        tab = Drupal.findHorizontalTab(element);
        if (tab) {
          tab.addClass('has-error').removeClass('has-success');
        }
        var fieldset = $(element).parents('fieldset.collapsible');
        if (fieldset !== undefined) {
          fieldset.find('legend').addClass('has-error').removeClass('has-success');
        }
      },
      success: function(element) {
        var tab;

        $(element).closest('.form-group').removeClass('has-error').removeClass('has-success');

        var fieldset = $(element).parents('fieldset.vertical-tabs-pane');
        if (fieldset.size() && fieldset.find('.has-error').not('label').size() === 0) {
          tab = Drupal.findVerticalTab(element);
          if (tab) {
            tab.removeClass('has-error').addClass('has-success');
          }
        }

        // Same for horizontal tabs.
        fieldset = $(element).parents('fieldset.horizontal-tabs-pane');
        if (fieldset.size() && fieldset.find('.has-error').not('label').size() === 0) {
          tab = Drupal.findHorizontalTab(element);
          if (tab) {
            tab.removeClass('has-error').addClass('has-success');
          }
        }

        fieldset = $(element).parents('fieldset.collapsible');
        if (fieldset !== undefined) {
          fieldset.find('legend').removeClass('has-error').addClass('has-success');
        }
      }
    });
  };

})(jQuery);
