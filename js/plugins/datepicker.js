/**
 * @file
 * Init datepicker plugin.
 *
 * @see caffelatte.plugin.datepicker.js
 */

(function ($) {

  Drupal.behaviors.caffelatteDatepicker = {
    attach: function (context, settings) {
      if ($.isFunction($.fn[ 'datepicker' ]) && $.isFunction($.fn[ 'clDatepicker' ])) {
        $('[data-plugin-datepicker]', context).once('caffelatte-datepicker', function() {
          $(this).each(function() {
            var $this = $(this);
            var options = settings.caffelatte.datepicker;
            var html5data = (typeof $.html5data === 'function' ? $this.html5data('plugin-datepicker') : {});
            options = $.extend(true, options, $this.data('plugin-options'), html5data);

            $this.clDatepicker(options);
          });
        });
      }
    }
  };

  /**
   * Set date on datefield.
   */
  Drupal.behaviors.caffelatteDatepickerSetDate = {
    attach: function (context, settings) {
      if ($.isFunction($.fn[ 'datepicker' ]) && $.isFunction($.fn[ 'clDatepicker' ])) {
        $('.form-type-date', context).once('caffelatte-datepicker-setdate', function() {
          $(this).each(function() {
            var $this = $(this);
            var $year = $('.form-date-year');
            var $month = $('.form-date-month');
            var $day = $('.form-date-day');

            var datepicker = $this.find('[data-plugin-datepicker]');
            datepicker.on('change blur', function(event) {
              event.preventDefault();

              var date = datepicker.datepicker('getDate');
              var year = date.getFullYear();
              var month = date.getMonth() + 1;
              var day = date.getDate();

              $year.find('option:selected').removeAttr('selected');
              $month.find('option:selected').removeAttr('selected');
              $day.find('option:selected').removeAttr('selected');

              $year.find('option[value="' + year + '"]').attr('selected', 'selected');
              $month.find('option[value="' + month + '"]').attr('selected', 'selected');
              $day.find('option[value="' + day + '"]').attr('selected', 'selected');

            });

          });
        });
      }
    }
  };

})(jQuery);
