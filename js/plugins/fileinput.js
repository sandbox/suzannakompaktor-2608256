/**
 * @file
 * Init fileinput plugin.
 *
 * @see caffelatte.plugin.fileinput.js
 */

(function ($) {

  Drupal.behaviors.caffelatteFileinput = {
    attach: function (context, settings) {
      if ($.isFunction($.fn[ 'fileinput' ]) && $.isFunction($.fn[ 'clFileInput' ])) {
        $('[data-plugin-fileinput]', context).once('caffelatte-fileinput', function() {
          $(this).each(function() {
            var $this = $(this);
            var options = settings.caffelatte.fileinput;
            var html5data = (typeof $.html5data === 'function' ? $this.html5data('plugin-fileinput') : {});
            options = $.extend(true, options, $this.data('plugin-options'), html5data);

            $this.clFileInput(options);
          });
        });
      }
    }
  };

})(jQuery);
