/**
 * @file
 * Init gallery plugin.
 */

(function ($) {

  Drupal.behaviors.caffelatteGallery = {
    attach: function (context, settings) {
      if ($.isFunction($.fn[ 'magnificPopup' ])) {
        $(Drupal.settings.caffelatte.lightbox.gallerySelector, context).once('caffelatte-lightbox-gallery', function() {
          $(this).magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: Drupal.t('Loading image @curr...', {'@curr': '#%curr%'}),
            mainClass: 'mfp-img-mobile',
            gallery: {
              enabled: true,
              navigateByImgClick: true,
              preload: [0,1]
              // Will preload 0 - before current, and 1 after the current image.
            },
            image: {
              tError: '<a href="%url%">' + Drupal.t('The image @curr.', {'@curr': '#%curr%'}) + '</a> ' + Drupal.t('could not be loaded.')
            }
          });
        });

      }
    }
  };

  Drupal.behaviors.caffelatteGalleryYoutube = {
    attach: function (context, settings) {
      if ($.isFunction($.fn[ 'magnificPopup' ])) {
        $(Drupal.settings.caffelatte.lightbox.popupSelector, context).once('caffelatte-lightbox-popup', function() {
          $(this).magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,

            fixedContentPos: false
          });
        });
      }
    }
  };

})(jQuery);
