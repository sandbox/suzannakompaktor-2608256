/**
 * @file
 * Init maskedinput plugin.
 *
 * @see caffelatte.plugin.maskedinput.js
 */

(function ($) {

  Drupal.behaviors.caffelatteMaskedinput = {
    attach: function (context, settings) {
      if ($.isFunction($.fn[ 'mask' ]) && $.isFunction($.fn[ 'clMaskedInput' ])) {
        $('[data-plugin-masked-input]', context).once('caffelatte-masked-input', function() {
          $(this).each(function() {
            var $this = $(this);
            var options = settings.caffelatte.maskedinput;
            var html5data = (typeof $.html5data === 'function' ? $this.html5data('plugin-masked-input') : {});
            options = $.extend(true, options, $this.data('plugin-options'), html5data);

            $this.clMaskedInput(options);
          });
        });
      }
    }
  };

})(jQuery);
