/**
 * @file
 * Init colorpicker plugin.
 *
 * @see caffelatte.plugin.colorpicker.js
 */

(function ($) {

  Drupal.behaviors.caffelatteColorpicker = {
    attach: function (context, settings) {
      if ($.isFunction($.fn[ 'colorpicker' ]) && $.isFunction($.fn[ 'clColorpicker' ])) {
        $('[data-plugin-colorpicker]', context).once('caffelatte-colorpicker', function() {
          $(this).each(function() {
            var $this = $(this);
            var options = settings.caffelatte.colorpicker;
            var html5data = (typeof $.html5data === 'function' ? $this.html5data('plugin-colorpicker') : {});
            options = $.extend(true, options, $this.data('plugin-options'), html5data);
            $this.clColorpicker(options);
          });
        });
      }
    }
  };

})(jQuery);
