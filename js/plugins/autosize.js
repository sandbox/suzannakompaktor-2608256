/**
 * @file
 * Init autosize plugin.
 *
 * @see caffelatte.plugin.autosize.js
 */

(function ($) {

  Drupal.behaviors.caffelatteAutosize = {
    attach: function (context, settings) {
      if ($.isFunction($.fn[ 'autosize' ]) && $.isFunction($.fn[ 'clAutoSize' ])) {
        $('[data-plugin-autosize]', context).once('caffelatte-autosize', function() {
          $(this).each(function() {
            var $this = $(this);
            $this.clAutoSize();
          });
        });
      }
    }
  };

})(jQuery);
