/**
 * @file
 * Init slider plugin.
 *
 * @see caffelatte.plugin.slider.js
 */

(function ($) {

  Drupal.behaviors.caffelatteSlider = {
    attach: function (context, settings) {
      if ($.isFunction($.fn[ 'slider' ]) && $.isFunction($.fn[ 'clSlider' ])) {
        $('[data-plugin-slider]', context).once('caffelatte-slider', function() {
          $(this).each(function() {
            var $this = $(this);
            var options = settings.caffelatte.slider;
            var html5data = (typeof $.html5data === 'function' ? $this.html5data('plugin-slider') : {});
            options = $.extend(true, options, $this.data('plugin-options'), html5data);

            $this.clSlider(options);
          });
        });
      }
    }
  };

})(jQuery);
