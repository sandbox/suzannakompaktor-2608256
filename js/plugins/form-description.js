/**
 * @file
 * Init form description plugin.
 *
 * @see caffelatte.form.description.js
 */

(function ($) {

  Drupal.behaviors.caffelatteFormDescription = {
    attach: function (context, settings) {
      if ($.isFunction($.fn[ 'popover' ]) && $.isFunction($.fn[ 'formDescription' ])) {
        $('[data-form-description]', context).once('caffelatte-form-description', function() {
            var $this = $(this);
            var options = settings.caffelatte.formDescription;
            var html5data = (typeof $.html5data === 'function' ? $this.html5data('form-description') : {});
            options = $.extend(true, options, $this.data('plugin-options'), html5data);

            $this.formDescription(options);
        });
      }
    }
  };

})(jQuery);
