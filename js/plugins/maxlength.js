/**
 * @file
 * Init maxlength plugin.
 *
 * @see caffelatte.plugin.maxlength.js
 */

(function ($) {

  Drupal.behaviors.caffelatteMaxlength = {
    attach: function (context, settings) {
      if ($.isFunction($.fn[ 'maxlength' ]) && $.isFunction($.fn[ 'clMaxLength' ])) {
        $('[data-plugin-maxlength]', context).once('caffelatte-maxlength', function() {
          $(this).each(function() {
            var $this = $(this);
            var options = settings.caffelatte.maxlength;
            var html5data = (typeof $.html5data === 'function' ? $this.html5data('plugin-maxlength') : {});
            options = $.extend(true, options, $this.data('plugin-options'), html5data);

            $this.clMaxLength(options);
          });
        });
      }
    }
  };

})(jQuery);
