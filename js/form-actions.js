/**
 * @file
 * Action button behaviors.
 */

(function ($) {

  /**
   * Attaches caffelatteFormAction behavior to all form-actions container.
   */
  Drupal.behaviors.caffelatteFormActions = {
    attach: function (context, settings) {
      $('.form-actions', context).once('caffelatte-form-actions', function() {
        var clFormActions = new Drupal.clFormActions($(this));

        $(window).on('resize', function(){
          clFormActions.toggleButton();
        });

      });
    }
  };

  /**
 * Action buttons object.
 */
  Drupal.clFormActions = function ($formActions) {
    this.inFlow = true;
    this.$formActions = $formActions;
    this.buttonTextClass = 'button-text';
    this.buttonAllowHideClass = 'hidden-on-overflow';

    this.toggleButton();
  };

  /**
   * Hide button text if there's not enough space.
   */
  Drupal.clFormActions.prototype.toggleButton = function () {
    var hasOverflow = this.hasOverflow();
    var _self = this;

    this.$formActions.find('button').each(function() {
      if (_self.isAllowedToHide($(this))) {
        _self.toggleButtonText($(this), hasOverflow);
       }
    });
  };

  /**
   * Hide button text if there's not enough space.
   */
  Drupal.clFormActions.prototype.toggleButtonText = function (button, hasOverflow) {
    var $button = $(button);
    var $buttonText = $button.find('> .' + this.buttonTextClass);

    if (hasOverflow) {
      $button.attr('title', $buttonText.text());
      $buttonText.addClass('hidden');
    }
    else {
      $button.removeAttr('title');
      $buttonText.removeClass('hidden');
    }
  };

  /**
   * Check if buttons are too long for the container.
   */
  Drupal.clFormActions.prototype.hasOverflow = function () {
    this.setInFlow();

    if (this.inFlow) {
      var hasOverflow = false;
      var elementsWidth = 0;

      this.$formActions.children().each(function(){
        elementsWidth += $(this).outerWidth();
      });

      if (elementsWidth + 15 > this.$formActions.width()) {
        hasOverflow = true;
      }

      return hasOverflow;
    };

    return false;
  };

  /**
   * Check if button can be hidden.
   */
  Drupal.clFormActions.prototype.isAllowedToHide = function ($button) {
    return $button.find('.' + this.buttonTextClass).hasClass(this.buttonAllowHideClass);
  };

  /**
   * Check if form actions is in the flow.
   */
  Drupal.clFormActions.prototype.setInFlow = function () {
    if (this.$formActions.css('display') !== 'block') {
      this.inFlow = false;
    }

    if (this.$formActions.css('float') === 'left' || this.$formActions.css('float') === 'right') {
      this.inFlow = false;
    }

    if (this.$formActions.css('overflow') !== 'visible'){
        this.inFlow = false; 
    }
  };

})(jQuery);
