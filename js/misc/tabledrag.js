/**
 * @file
 * Custom behaviors for tabledrag.
 */

(function ($) {

  /**
 * Ovewrride Drupal.theme.tableDragChangedWarning.
 *
 * Display as alert in warning style.
 */
  Drupal.theme.tableDragChangedWarning = function () {
    return '<div class="alert alert-warning tabledrag-changed-warning messages">' + Drupal.theme('tableDragChangedMarker') + ' ' + Drupal.t('Changes made in this table will not be saved until the form is submitted.') + '</div>';
  };

  Drupal.behaviors.caffelatteTableDrag = {
    attach: function (context, settings) {
      var clIcon = Drupal.settings.clIcon;
      var iconClass = '.' + clIcon;
      var $icon = $('<i></i>').addClass(clIcon).addClass(clIcon + '-fw');
      // Add handle icon instead of image.
      $('a.tabledrag-handle .handle', context).once('caffelatte-tabledrag-handle', function () {
        $(this).html($('<i></i>').addClass(clIcon).addClass(clIcon + '-fw').addClass(clIcon + '-arrows'));
      });
      // Add icons to show/hide links.
      $('.tabledrag-toggle-weight', context).once('caffelatte-tabledrag-toggle-weight', function () {

        var $toggle = $(this);
        var $table = $toggle.closest('.tabledrag-toggle-weight-wrapper').nextAll('.tabledrag-processed');

        $toggle.prepend($icon);
        if ($.cookie('Drupal.tableDrag.showWeight') == 1) {
          $toggle.find(iconClass).addClass(clIcon + '-eye-slash');
          $table.removeClass('tabledrag-toggle-weight-hidden');
        }
        else {
          $toggle.find(iconClass).addClass(clIcon + '-eye');
          $table.addClass('tabledrag-toggle-weight-hidden');
        }
        $toggle.click(function () {
          $(this).prepend($icon);
          if ($.cookie('Drupal.tableDrag.showWeight') == 1) {
            $(this).find(iconClass).addClass(clIcon + '-eye-slash').removeClass(clIcon + '-eye');
            $table.removeClass('tabledrag-toggle-weight-hidden');
          }
          else {
            $(this).find(iconClass).addClass(clIcon + '-eye').removeClass(clIcon + '-eye-slash');
            $table.addClass('tabledrag-toggle-weight-hidden');
          }
        });
      });
    }
  };

})(jQuery);
