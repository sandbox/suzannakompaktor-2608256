/**
 * @file
 * Custom collapsible fieldset behavior.
 */

(function ($) {

  Drupal.behaviors.collapse = {
    attach: function (context, settings) {
      $('fieldset.collapsible', context).once('collapse', function() {
        var fieldset = $(this).get(0);
        var clCollapsibleFieldset = new Drupal.clCollapsibleFieldset(fieldset);
      });
    }
  };

  /**
 * CollapsibleFieldset object.
 */
  Drupal.clCollapsibleFieldset = function (fieldset) {
    var clIcon = Drupal.settings.clIcon;
    this.fieldset = fieldset;
    this.$fieldset = $(fieldset);
    this.$legend = this.$fieldset.find('> legend .fieldset-legend');
    this.fieldsetWrapper = '> .fieldset-wrapper';
    this.icons = {
      collapsed: clIcon + '-caret-right',
      expanded: clIcon + '-caret-down'
    };

    var _self = this;

    var $link = $('<a class="fieldset-title" href="#"></a>')
      .prepend(this.$legend.contents())
      .appendTo(this.$legend);

    $link.click(function () {
      // Don't animate multiple times.
      if (!_self.fieldset.animating) {
        _self.fieldset.animating = true;
        _self.toggle(fieldset);
      }
      return false;
    });
  };

  /**
   * Hide button text if there's not enough space.
   */
  Drupal.clCollapsibleFieldset.prototype.toggle = function () {
    var fieldset = this.fieldset;
    var $fieldset = this.$fieldset;
    var icons = this.icons;
    var clIcon = Drupal.settings.clIcon;
    var iconClass = '.' + clIcon;

    if ($fieldset.is('.collapsed')) {
      var $content = $(this.fieldsetWrapper, fieldset).hide();
      $fieldset
        .removeClass('collapsed')
        .trigger({ type: 'collapsed', value: false })
        .find('> legend ' + iconClass).removeClass(icons.collapsed).addClass(icons.expanded);
      $content.slideDown({
        duration: 'fast',
        easing: 'linear',
        complete: function () {
          fieldset.animating = false;
        }
      });
    }
    else {
      $fieldset.trigger({ type: 'collapsed', value: true });
      $(this.fieldsetWrapper, fieldset).slideUp('fast', function () {
        $fieldset
          .addClass('collapsed')
          .find('> legend ' + iconClass).removeClass(icons.expanded).addClass(icons.collapsed);
        fieldset.animating = false;
      });
    }
  };

})(jQuery);
