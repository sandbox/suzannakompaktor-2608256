/**
 * @file
 * Custom behaviors for tabs.
 */

(function ($) {

  Drupal.behaviors.tabs = {
    attach: function (context, settings) {
      $('.tabs', context).once('caffelatte-tabs', function() {
        var clTabs = new Drupal.clTabs($(this));
      });
    }
  };

/**
 * clTabs object.
 */
  Drupal.clTabs = function (tabs, container, options) {
    this.init(tabs, container, options);
  };

  Drupal.clTabs.prototype = {
    tabs: {},
    container: {},
    hasSecondary: false,
    options: {},
    original: '',
    width: 0
  };

  Drupal.clTabs.prototype.init = function (tabs, container, options) {

    this.tabs = {
      $element: tabs
    }
    this.tabs.$primary = tabs.find('ul.nav-tabs');
    if (tabs.find('ul.nav-tabs-secondary')) {
      this.tabs.$secondary = tabs.find('ul.nav-tabs-secondary');
      this.hasSecondary = true;
    };

    // Set container if different from tabs element.
    this.container.$element = container || tabs;
    var containerHeight = this.options.containerHeight || this.container.$element.innerHeight();
    this.container.height = containerHeight;

    // Set base width of li items.
    var width = 0;
    $.each(this.tabs.$primary.find('li'), function(i){
      width += $(this).outerWidth();
    });
    this.width = width;

    // Store the content of original item.
    this.original = this.tabs.$primary.html();

    this.build();
    this.events();
  }

  /**
   * Collapse to dropdown.
   */
  Drupal.clTabs.prototype.collapse = function () {
    var clIcon = Drupal.settings.clIcon;
    var iconClass = '.' + clIcon;

    if (!this.tabs.$primary.hasClass('dropdown')) {
      this.tabs.$primary.addClass('dropdown').removeClass('nav-tabs');
      this.tabs.$element.addClass('tabs-collapsed');

      var buttonText = this.tabs.$primary.find('li.active a').html();
      var buttonClasses = this.tabs.$primary.find('li.active a').attr('class');
      var items = [];
      this.tabs.$primary.find('li.active').remove();
      $.each(this.tabs.$primary.find('li'), function(i){
        items[i] = $(this).html();
      });
      var dropdown = Drupal.theme.bootstrapDropdownMenu(
      buttonText,
      buttonClasses,
      items,
      ''
      );
      this.tabs.$primary.html('<li>' + dropdown + '</li>');
    }
  };

  /**
   * Expand to original.
   */
  Drupal.clTabs.prototype.expand = function () {
    if (this.tabs.$primary.hasClass('dropdown')) {
      this.tabs.$primary.html(this.original);
      this.tabs.$primary.removeClass('dropdown').addClass('nav-tabs');
      this.tabs.$element.removeClass('tabs-collapsed');
    }
  };

  /**
   * Build tabs.
   */
  Drupal.clTabs.prototype.build = function () {
    if ($(window).width() <= 480) {
      this.collapse();
    }
    else {
      if (this.hasOverflow()) {
        this.collapse();
      }
      else {
         this.expand();
      }
    }
    
    this.setContainerHeight();
  };

  /**
   * Events.
   */
  Drupal.clTabs.prototype.events = function () {
    var _self = this;
    
    $(window).on('resize', function(){
      _self.build();
    });
  };

  /**
   * Set the correct height of container if secondary tabs exists and it's out of DOM.
   */
  Drupal.clTabs.prototype.setContainerHeight = function () {
    if (this.hasSecondary) {
      if (this.tabs.$secondary.css('position') == 'absolute') {
        var height = this.container.height + this.tabs.$secondary.outerHeight();
        this.container.$element.css('height', height);
      };
    };
  };

  /**
   * Check if content of tab items are longer than container.
   */
  Drupal.clTabs.prototype.hasOverflow = function () {
    var _self = this;
    var contentWidth = this.width;

    if (this.container.$element.width() < contentWidth) {
      return true;
    }

    return false;
  };

})(jQuery);
