<?php
/**
 * @file
 * ui-accordation.vars.php
 */

/**
 * Implements hook_preprocess_ui_accordation().
 */
function caffelatte_preprocess_ui_accordation(&$variables) {
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/bootstrap-accordation.js', array('scope' => 'footer'));

  $variables['styles'] = isset($variables['styles']) ? (array) $variables['styles'] : array();
  $variables['attributes'] = isset($variables['attributes']) ? (array) $variables['attributes'] : array();

  $icon_collapsed = isset($variables['icon_collapsed']) ? $variables['icon_collapsed'] : theme_get_setting('caffelatte_accordation_icon_collapsed');
  $icon_expanded = isset($variables['icon_expanded']) ? $variables['icon_expanded'] : theme_get_setting('caffelatte_accordation_icon_expanded');

  $variables['accordation_icon_collapsed'] = theme('icon', array(
    'icon' => $icon_collapsed,
    'attributes' => array(
      'class' => array('accordation-icon-collapsed'),
    ),
  ));

  $variables['accordation_icon_expanded'] = theme('icon', array(
    'icon' => $icon_expanded,
    'attributes' => array(
      'class' => array('accordation-icon-expanded'),
    ),
  ));

  if (empty($variables['attributes']['id'])) {
    $variables['accordation_id'] = 'accordation-' . md5($variables['elements'][0]['title']);
  }
  else {
    $variables['accordation_id'] = check_plain($variables['attributes']['id']);
    unset($variables['attributes']['id']);
  }

  foreach ($variables['elements'] as $key => $element) {
    if (!isset($element['collapsed'])) {
      $variables['elements'][$key]['collapsed'] = TRUE;
    }

    if (!isset($element['id'])) {
      $id = 'accordationItem' . $key;
      $variables['elements'][$key]['id'] = $id;
    }
    $variables['elements'][$key]['accordation_toggle'] = '<a class="accordation-toggle collapsed" data-toggle="collapse" data-parent="#' . $variables['accordation_id'] . '" href="#' . $id . '">';
    $variables['elements'][$key]['accordation_toggle'] .= check_plain($element['title']);
    $variables['elements'][$key]['accordation_toggle'] .= '</a>';
  }
}

/**
 * Implements hook_process_ui_accordation().
 */
function caffelatte_process_ui_accordation(&$variables) {
  $variables['styles'] = ' ' . _caffelatte_create_styles('panel', $variables['styles']);
  if (isset($variables['attributes']['class'])) {
    $variables['styles'] .= ' ' . implode($variables['attributes']['class']);
    unset($variables['attributes']['class']);
  }
  $variables['attributes'] = drupal_attributes($variables['attributes']);
}
