<?php
/**
 * @file
 * ui-modal.tpl.php
 *
 * @see template_preprocess_ui_modal()
 */

?>
<div id="<?php print $id ?>" class="<?php print $anim ?>modal-block<?php print $styles ?> mfp-hide" role="dialog" aria-hidden="true" tabindex="-1">
  <section class="panel">
    <?php if($heading): ?>
    <header class="panel-heading">
      <h2 class="panel-title"><?php print $heading ?></h2>
    </header>
    <?php endif;?>
    <div class="panel-body">
      <div class="modal-wrapper">
        <?php if($icon): ?>
        <div class="modal-icon"><?php print $icon ?></div>
        <?php endif;?>
        <div class="modal-text">
          <?php print $content ?>
        </div>
      </div>
    </div>
    <?php if($footer): ?>
    <footer class="panel-footer">
      <div class="row">
        <div class="col-md-12 text-right">
          <?php print $footer ?>
        </div>
      </div>
    </footer>
    <?php endif;?>
  </section>
</div>
