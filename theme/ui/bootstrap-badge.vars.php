<?php
/**
 * @file
 * bootstrap-badge.vars.php
 */

/**
 * Implements hook_preprocess_bootstrap_badge().
 */
function caffelatte_preprocess_bootstrap_badge(&$variables) {
  $styles = _caffelatte_create_styles('badge', $variables['styles']);
  $variables['styles'] = $styles;
}
