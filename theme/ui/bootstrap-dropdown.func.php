<?php
/**
 * @file
 * bootstrap-dropdown.func.php
 *
 * Variables:
 * - $direction: 'up' or 'down' (dropup or dropdown).
 * - $label: Label (main item text) of dropdown.
 * - $links: Links to display as dropdown.
 * - $attributes: HTML attributes.
 */

/**
 * Implements theme_bootstrap_dropdown().
 */
function caffelatte_bootstrap_dropdown($variables) {
  $sub_links = '';

  if (isset($variables['direction'])) {
    $variables['attributes']['class'][] = ($variables['direction'] == 'up') ? 'dropup' : 'dropdown';
  }
  else {
    $variables['attributes']['class'][] = 'dropdown';
  }

  // Start markup.
  $output = '<div' . drupal_attributes($variables['attributes']) . '>';

  $output .= '<a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true href="#">';

  // It is a link, create one.
  if (is_string($variables['label']) && !empty($variables['label'])) {
    $output .= $variables['label'];
  }

  if (is_array($variables['links'])) {
    $sub_links = theme('links', array(
      'links' => $variables['links'],
      'attributes' => array(
        'class' => array('dropdown-menu'),
      ),
    ));
  }
  // Finish markup.
  $output .= theme('icon', array('icon' => 'custom', 'attributes' => array('class' => array('custom-caret')))) . '</a>' . $sub_links . '</div>';
  return $output;
}
