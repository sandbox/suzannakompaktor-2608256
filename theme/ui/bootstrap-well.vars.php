<?php
/**
 * @file
 * bootstrap-well.vars.php
 */

/**
 * Implements hook_preprocess_bootstrap_well().
 */
function caffelatte_preprocess_bootstrap_well(&$variables) {
  $styles = _caffelatte_create_styles('well', $variables['styles']);
  $variables['styles'] = $styles;
  if (!empty($variables['attributes'])) {
    if (!empty($variables['attributes']['class'])) {
      $variables['classes'] = ' ' . implode(' ', $variables['attributes']['class']);
      unset($variables['attributes']['class']);
    }
    $variables['attributes'] = drupal_attributes($variables['attributes']);
  }
}
