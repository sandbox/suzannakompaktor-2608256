<?php
/**
 * @file
 * bootstrap-input-group.vars.php
 */

/**
 * Implements hook_preprocess_bootstrap_input_group().
 */
function caffelatte_preprocess_bootstrap_input_group(&$variables) {
  if (!empty($variables['styles'])) {
    $variables['styles'] = ' ' . implode(' ', $variables['styles']);
  }
  if (!empty($variables['prefix'])) {
    if (!empty($variables['prefix_styles'])) {
      $variables['prefix_styles'] = ' ' . implode(' ', $variables['prefix_styles']);
    }
    if (empty($variables['prefix_type'])) {
      $variables['prefix_type'] = 'addon';
    }
  }
  if (!empty($variables['suffix'])) {
    if (!empty($variables['suffix_styles'])) {
      $variables['suffix_styles'] = ' ' . implode(' ', $variables['suffix_styles']);
    }
    if (empty($variables['suffix_type'])) {
      $variables['suffix_type'] = 'addon';
    }
  }

  if (isset($variables['attributes'])) {
    if (is_array($variables['attributes'])) {
      if (isset($variables['attributes']['class'])) {
        if (!empty($variables['attributes']['class'])) {
          $variables['styles'] .= ' ' . implode(' ', $variables['attributes']['class']);
        }
        unset($variables['attributes']['class']);
      }
      $variables['attributes'] = drupal_attributes($variables['attributes']);
    }
  }
}
