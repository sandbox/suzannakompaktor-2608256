<?php
/**
 * @file
 * bootstrap-label.tpl.php
 *
 * @see template_preprocess_bootstrap_label()
 */

?>
<span class="label <?php print $styles ?>"><?php print $content?></span>
