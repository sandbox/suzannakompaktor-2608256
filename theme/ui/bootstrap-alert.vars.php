<?php
/**
 * @file
 * bootstrap-alert.vars.php
 */

/**
 * Implements hook_preprocess_bootstrap_alert().
 */
function caffelatte_preprocess_bootstrap_alert(&$variables) {
  $styles = _caffelatte_create_styles('alert', $variables['styles']);
  $variables['styles'] = $styles;
  if (!empty($variables['attributes'])) {
    if (!empty($variables['attributes']['class'])) {
      $variables['classes'] = ' ' . implode(' ', $variables['attributes']['class']);
      unset($variables['attributes']['class']);
    }
    $variables['attributes'] = drupal_attributes($variables['attributes']);
  }
}
