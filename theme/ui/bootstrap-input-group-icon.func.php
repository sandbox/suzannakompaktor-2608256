<?php
/**
 * @file
 * bootstrap-input-group-icon.func.php
 */

/**
 * Implements bootstrap_input_group_icon().
 */
function caffelatte_bootstrap_input_group_icon(&$variables) {
  $variables['styles'][] = 'input-group-icon';
  return theme('bootstrap_input_group', $variables);
}
