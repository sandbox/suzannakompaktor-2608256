<?php
/**
 * @file
 * Bootstrap-panel.tpl.php.
 *
 * @see template_preprocess_bootstrap_panel()
 */

?>
<?php if(!empty($actions)): ?>
<div class="panel-actions">
  <?php print $actions; ?>
</div>
<?php endif; ?>

<?php if($title): ?>
<h2 class="panel-title"><?php print $title; ?></h2>
<?php endif; ?>
<?php if($subtitle): ?>
<p class="panel-subtitle"><?php print $subtitle; ?></p>
<?php endif; ?>
