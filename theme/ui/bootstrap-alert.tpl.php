<?php
/**
 * @file
 * bootstrap-alert.tpl.php
 *
 * @see template_preprocess_bootstrap_alert()
 */

?>
<div class="alert <?php print $styles ?><?php print $classes ?>" role="alert"<?php print $attributes ?>>
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <?php print $content?>
</div>
