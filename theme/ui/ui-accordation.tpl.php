<?php
/**
 * @file
 * ui-accordation.tpl.php
 *
 * @see template_preprocess_ui_accordation()
 */

?>
<div class="panel-group" id="<?php print $accordation_id ?>"<?php print $attributes ?>>
  <?php foreach($elements as $id => $item): ?>
  <div class="panel panel-accordation<?php print $styles ?>">
    <div class="panel-heading">
      <div class="panel-actions accordation-actions">
        <a class="accordation-toggle">
          <?php print $accordation_icon_collapsed ?>
          <?php print $accordation_icon_expanded ?>
        </a>
      </div>
      <h4 class="panel-title">
        <?php print $item['accordation_toggle']; ?>
      </h4>
    </div>
    <div id="<?php print $item['id'] ?>" class="accordation-body collapse<?php print !$item['collapsed'] ? ' in' : ''; ?>">
      <div class="panel-body">
        <?php print render($item['content']) ?>
      </div>
    </div>
  </div>
  <?php endforeach;?>
</div>
