<?php
/**
 * @file
 * bootstrap-panel.vars.php
 */

/**
 * Implements hook_preprocess_bootstrap_panel().
 */
function caffelatte_preprocess_bootstrap_panel(&$variables) {
  $variables['styles'] = isset($variables['styles']) ? (array) $variables['styles'] : array();
  $variables['attributes'] = isset($variables['attributes']) ? (array) $variables['attributes'] : array();

  if ($variables['is_transparent']) {
    $variables['styles'] = array('panel-transparent');
  }

  if (isset($variables['attributes']['class'])) {
    $variables['styles'] = array_merge($variables['attributes']['class'], $variables['styles']);
    unset($variables['attributes']['class']);
  }
}

/**
 * Implements hook_process_bootstrap_panel().
 */
function caffelatte_process_bootstrap_panel(&$variables) {
  $variables['styles'] = !empty($variables['styles']) ? ' ' . implode(' ', $variables['styles']) : '';
  $variables['attributes'] = drupal_attributes($variables['attributes']);
}
