<?php
/**
 * @file
 * ui-modal.vars.php
 */

/**
 * Implements hook_preprocess_ui_modal().
 */
function caffelatte_preprocess_ui_modal(&$variables) {
  if (empty($variables['id'])) {
    $variables['id'] = drupal_html_id(strip_tags($variables['heading']));
  }

  $variables['heading'] = $variables['html_heading'] ? $variables['heading'] : check_plain($variables['heading']);
}

/**
 * Implements hook_process_ui_modal().
 */
function caffelatte_process_ui_modal(&$variables) {
  $variables['content'] = render($variables['content']);
  $variables['footer'] = render($variables['footer']);
}
