<?php
/**
 * @file
 * Display Bootstrap button dropdown.
 *
 * Variables:
 * - $direction: 'up' or 'down' (dropup or dropdown).
 * - $label: Label (main item text) of dropdown.
 * - $links: Links to display as dropdown.
 * - $styles: Button styles (primary, info, sm, lg, etc).
 * - $attributes: HTML attributes.
 */

/**
 * Implements theme_bootstrap_dropdown_btn().
 */
function caffelatte_bootstrap_dropdown_btn($variables) {
  $sub_links = '';
  $styles = '';

  if (isset($variables['direction'])) {
    $variables['attributes']['class'][] = ($variables['direction'] == 'up') ? 'dropup' : 'dropdown';
  }
  else {
    $variables['attributes']['class'][] = 'dropdown';
  }

  $variables['attributes']['class'][] = 'btn-group';
  // Style class.
  if (isset($variables['styles'])) {
    $styles .= ' ' . _caffelatte_create_styles('btn', $variables['styles']);
  }

  // Start markup.
  $output = '<div' . drupal_attributes($variables['attributes']) . '>';

  $output .= '<a class="btn' . $styles . ' dropdown-toggle" data-toggle="dropdown" aria-expanded="true" href="#">';

  // It is a link, create one.
  if (is_string($variables['label']) && !empty($variables['label'])) {
    $output .= check_plain($variables['label']);
  }

  if (is_array($variables['links'])) {
    $sub_links = theme('links', array(
      'links' => $variables['links'],
      'attributes' => array(
        'class' => array('dropdown-menu'),
      ),
    ));
  }

  // Finish markup.
  $output .= theme('icon', array('icon' => 'custom', 'attributes' => array('class' => array('custom-caret')))) . '</a>' . $sub_links . '</div>';
  return $output;
}
