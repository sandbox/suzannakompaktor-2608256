<?php
/**
 * @file
 * bootstrap-panel-heading.vars.php
 */

/**
 * Implements hook_preprocess_bootstrap_panel_heading().
 */
function caffelatte_preprocess_bootstrap_panel_heading(&$variables) {
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/bootstrap-panel.js', array('scope' => 'footer'));

  $variables['actions'] = isset($variables['actions']) ? (array) $variables['actions'] : array();

  $variables['actions'][] = '<a href="#" class="' . _caffelatte_get_icon('custom') . ' custom-caret panel-actions-collapse"></a>';
  $variables['actions'][] = '<a href="#" class="' . _caffelatte_get_icon('times') . ' panel-actions-close"></a>';
}

/**
 * Implements hook_process_bootstrap_panel_heading().
 */
function caffelatte_process_bootstrap_panel_heading(&$variables) {
  $variables['actions'] = implode('', $variables['actions']);
}
