<?php
/**
 * @file
 * bootstrap-label.vars.php
 */

/**
 * Implements hook_preprocess_bootstrap_label().
 */
function caffelatte_preprocess_bootstrap_label(&$variables) {
  $styles = _caffelatte_create_styles('label', $variables['styles']);
  $variables['styles'] = $styles;
}
