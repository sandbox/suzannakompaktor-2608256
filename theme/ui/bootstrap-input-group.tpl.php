<?php
/**
 * @file
 * bootstrap-input-group.tpl.php
 *
 * Markup for Bootstrap input groups.
 */

?>

<div class="input-group<?php print $styles ?>"<?php print $attributes ?>>
  <?php if(!empty($prefix)): ?>
    <span class="input-group-<?php print $prefix_type ?><?php print $prefix_styles ?>"><?php print $prefix ?></span>
  <?php endif; ?>
  <?php print $input ?>
  <?php if(!empty($suffix)): ?>
    <span class="input-group-<?php print $suffix_type ?><?php print $suffix_styles ?>"><?php print $suffix ?></span>
  <?php endif; ?>
</div>
