<?php
/**
 * @file
 * go-up.tpl.php
 */

?>

<a href="#" rel="scroll-to-top" class="scroll-to-top">
	<?php print theme('icon', array('icon' => 'chevron-up')); ?> 
</a> 
