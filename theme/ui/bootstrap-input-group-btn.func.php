<?php
/**
 * @file
 * bootstrap-input-group-btn.func.php
 */

/**
 * Implements bootstrap_input_group_btn().
 */
function caffelatte_bootstrap_input_group_btn(&$variables) {
  $variables['prefix_type'] = 'btn';
  $variables['suffix_type'] = 'btn';

  $variables['prefix'] = $variables['prefix'];
  $variables['suffix'] = $variables['suffix'];

  return theme('bootstrap_input_group', $variables);
}
