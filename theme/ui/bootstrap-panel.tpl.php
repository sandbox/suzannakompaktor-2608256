<?php
/**
 * @file
 * bootstrap-panel.tpl.php
 *
 * @see template_preprocess_bootstrap_panel()
 */

?>
<section class="panel<?php print $styles; ?>"<?php print $attributes; ?>>

  <?php if(!empty($heading)): ?>
  <header class="panel-heading<?php print $is_heading_transparent ? ' panel-heading-transparent' : ''; ?>">
    <?php print $heading; ?>
  </header>
  <?php endif; ?>
  <?php print render($contextual_links); ?>

  <div class="panel-body">
    <?php print $content; ?>
  </div>
  <?php if(!empty($footer)): ?>
  <div class="panel-footer">
    <?php print $footer; ?>
  </div>
  <?php endif; ?>
</section>
