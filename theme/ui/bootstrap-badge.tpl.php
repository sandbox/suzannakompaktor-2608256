<?php
/**
 * @file
 * bootstrap-badge.tpl.php
 *
 * @see template_preprocess_bootstrap_badge()
 */

?>
<span class="badge <?php print $styles ?>"><?php print $content?></span>
