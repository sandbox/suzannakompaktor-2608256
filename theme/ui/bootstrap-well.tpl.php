<?php
/**
 * @file
 * bootstrap-well.tpl.php
 *
 * @see template_preprocess_bootstrap_well()
 */

?>
<div class="well <?php print $styles ?><?php print $classes ?>"<?php print $attributes ?>>
  <?php print $content?>
</div>
