<?php
/**
 * @file
 * block.vars.php
 */

/**
 * Implements hook_preprocess_block().
 */
function caffelatte_preprocess_block(&$variables) {
  $block = $variables['block'];
  $elements = $variables['elements'];
  $display = theme_get_setting('caffelatte_block_display-' . $block->module . '_' . $block->delta);

  if ($display == 'panel' || $display == 'widget') {
    $variables['theme_hook_suggestions'][] = 'block__panel';

    $panel_variables = array(
      'styles' => theme_get_setting('caffelatte_block_styles-' . $block->module . '_' . $block->delta),
    );
    $content = $variables['content'];
    if ($display == 'panel') {
      $panel_variables['heading'] = theme('bootstrap_panel_heading', array(
        'title' => $block->subject,
      ));
    }
    elseif ($display == 'widget') {
      $title = '<h2 class="widget-title h3 mt-unset">' . $block->subject . '</h2>';
      $content = $title . $content;
      $panel_variables['styles'][] = 'widget';
    }
    $panel_variables['content'] = $content;
    if (isset($variables['title_suffix']['contextual_links'])) {
      $panel_variables['contextual_links'] = $variables['title_suffix']['contextual_links'];
      $panel_variables['attributes']['class'][] = 'contextual-links-region';
    }

    $output = theme('bootstrap_panel', $panel_variables);
    $variables['output'] = $output;
  }
}

/**
 * Implements hook_process_block().
 */
function caffelatte_process_block(&$variables) {
  // Drupal 7 should use a $title variable instead of $block->subject.
  $variables['title'] = $variables['block']->subject;
}
