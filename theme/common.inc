<?php
/**
 * @file
 * common.inc
 *
 * Contains common functionality for the entire base theme.
 */

/**
 * Auto-rebuild the theme registry during theme development.
 */
if (theme_get_setting('caffelatte_rebuild_registry') && !defined('MAINTENANCE_MODE')) {
  // Rebuild .info data.
  system_rebuild_theme_data();
  // Rebuild theme registry.
  drupal_theme_rebuild();
}

/**
 * Return information from the .info file of a theme (and possible base themes).
 *
 * @param string $theme_key
 *   The machine name of the theme.
 * @param string $key
 *   The key name of the item to return from the .info file. This value can
 *   include "][" to automatically attempt to traverse any arrays.
 * @param bool $base_themes
 *   Recursively search base themes, defaults to TRUE.
 *
 * @return string|array|false
 *   A string or array depending on the type of value and if a base theme also
 *   contains the same $key, FALSE if no $key is found.
 */
function caffelatte_get_theme_info($theme_key = NULL, $key = NULL, $base_themes = TRUE) {
  // If no $theme_key is given, use the current theme if we can determine it.
  if (!isset($theme_key)) {
    $theme_key = !empty($GLOBALS['theme_key']) ? $GLOBALS['theme_key'] : FALSE;
  }
  if ($theme_key) {
    $themes = list_themes();
    if (!empty($themes[$theme_key])) {
      $theme = $themes[$theme_key];
      // If a key name was specified, return just that array.
      if ($key) {
        $value = FALSE;
        // Recursively add base theme values.
        if ($base_themes && isset($theme->base_themes)) {
          foreach (array_keys($theme->base_themes) as $base_theme) {
            $value = caffelatte_get_theme_info($base_theme, $key);
          }
        }
        if (!empty($themes[$theme_key])) {
          $info = $themes[$theme_key]->info;
          // Allow array traversal.
          $keys = explode('][', $key);
          foreach ($keys as $parent) {
            if (isset($info[$parent])) {
              $info = $info[$parent];
            }
            else {
              $info = FALSE;
            }
          }
          if (is_array($value)) {
            if (!empty($info)) {
              if (!is_array($info)) {
                $info = array($info);
              }
              $value = drupal_array_merge_deep($value, $info);
            }
          }
          else {
            if (!empty($info)) {
              if (empty($value)) {
                $value = $info;
              }
              else {
                if (!is_array($value)) {
                  $value = array($value);
                }
                if (!is_array($info)) {
                  $info = array($info);
                }
                $value = drupal_array_merge_deep($value, $info);
              }
            }
          }
        }
        return $value;
      }
      // If no info $key was specified, just return the entire info array.
      return $theme->info;
    }
  }
  return FALSE;
}

/**
 * Helper function for including theme files.
 *
 * @param string $theme
 *   Name of the theme to use for base path.
 * @param string $path
 *   Path relative to $theme.
 */
function caffelatte_include($theme, $path) {
  static $themes = array();
  if (!isset($themes[$theme])) {
    $themes[$theme] = drupal_get_path('theme', $theme);
  }
  if ($themes[$theme] && ($file = DRUPAL_ROOT . '/' . $themes[$theme] . '/' . $path) && file_exists($file)) {
    include_once $file;
  }
}

/**
 * Helper function to create styles for component.
 *
 * @param string $type
 *   Component type (eg: btn).
 * @param string/array $styles
 *   The styles (color or size types).
 *
 * @return string
 *   Styles to add to class attribute.
 */
function _caffelatte_create_styles($type, $styles = '', $default = 'default') {
  $result = array();
  $color_styles = array(
    'primary',
    'info',
    'success',
    'warning',
    'danger',
  );

  if (!empty($styles)) {
    if (is_array($styles)) {
      $has_color_style = FALSE;

      foreach ($styles as $style) {
        if (!empty($style)) {
          $result[] = !empty($type) ? $type . '-' . $style : $style;
        }
        if (in_array($style, $color_styles)) {
          $has_color_style = TRUE;
        }
      }

      // Add default if there's no style.
      if (!$has_color_style) {
        $default_style = !empty($type) ? $type . '-' . $default : $default;
        array_unshift($result, $default_style);
      }
    }
    elseif (is_string($styles)) {
      if (!in_array($styles, $color_styles)) {
        $result[] = !empty($type) ? $type . '-' . $default : $default;
      }
      $result[] = !empty($type) ? $type . '-' . $styles : $styles;
    }
  }
  else {
    $result[] = $type . '-' . $default;
  }

  return implode(' ', $result);
}

/**
 * Get icon style with icon prefix. (Dafault prefix is "icon").
 *
 * @param string $icon
 *   Name of icon (eg.: "cog").
 *
 * @return string
 *   Icon string (eg.: "icon icon-cog").
 */
function _caffelatte_get_icon($icon) {
  $icon_prefix = theme_get_setting('caffelatte_icon');
  return $icon_prefix . ' ' . $icon_prefix . '-' . $icon;
}

/**
 * Get icon for files by mime type.
 *
 * @param string $mime
 *   MIME type.
 *
 * @return string
 *   Icon string.
 */
function _caffelatte_get_file_icon($mime) {
  $mime = explode('/', check_plain($mime));

  // Set default.
  $icon = 'file-o';
  switch ($mime[0]) {
    case 'image':
      $icon = 'file-image-o';
      break;

    case 'audio':
      $icon = 'file-sound-o';
      break;

    case 'video':
      $icon = 'file-video-o';
    case 'text':
      $icon = 'file-text-o';
      break;

    case 'application':
      $icon = 'file';
      break;

    default:
      $icon = 'file-o';
      break;
  }

  switch ($mime[1]) {
    case 'pdf':
      $icon = 'file-pdf-o';
      break;
  }

  return $icon;
}

/**
 * Colorize buttons based on the text value.
 *
 * @param string $text
 *   Button text to search against.
 *
 * @return string
 *   The specific button class to use or FALSE if not matched.
 */
function _caffelatte_colorize_button_by_text($text) {
  // Text values containing these specific strings, which are matched first.
  $specific_strings = array(
    'primary' => array(
      t('Download feature'),
      t('Save and add'),
      t('Log in'),
      t('Create new account'),
      t('E-mail new password'),
    ),
    'success' => array(),
    'info' => array(
      t('Add another item'),
      t('Update style'),
      t('Add effect'),
      t('Add and configure'),
    ),
  );
  // Text values containing these generic strings, which are matches last.
  $generic_strings = array(
    'primary' => array(
      t('Save'),
      t('Confirm'),
      t('Submit'),
      t('Search'),
    ),
    'success' => array(),
    'warning' => array(
      t('Export'),
      t('Import'),
      t('Restore'),
      t('Rebuild'),
    ),
    'info' => array(
      t('Apply'),
      t('Add'),
      t('Create'),
      t('Write'),
    ),
    'danger' => array(
      t('Delete'),
      t('Remove'),
    ),
  );
  // Specific matching first.
  foreach ($specific_strings as $style => $strings) {
    foreach ($strings as $string) {
      if (strpos(drupal_strtolower($text), drupal_strtolower($string)) !== FALSE) {
        return $style;
      }
    }
  }
  // Generic matching last.
  foreach ($generic_strings as $style => $strings) {
    foreach ($strings as $string) {
      if (strpos(drupal_strtolower($text), drupal_strtolower($string)) !== FALSE) {
        return $style;
      }
    }
  }
  return '';
}

/**
 * Iconize buttons based on the text value.
 *
 * @param string $text
 *   Button text to search against.
 *
 * @return string
 *   The icon markup or FALSE if not matched.
 */
function _caffelatte_iconize_button_by_text($text) {
  // Text values containing these specific strings, which are matched first.
  $specific_strings = array();
  // Text values containing these generic strings, which are matches last.
  $generic_strings = array(
    'check' => array(
      t('Save'),
    ),
    'cog' => array(
      t('Manage'),
      t('Configure'),
    ),
    'download' => array(
      t('Download'),
    ),
    'arrow-circle-o-right ' => array(
      t('Export'),
    ),
    'arrow-circle-o-left' => array(
      t('Import'),
    ),
    'pencil' => array(
      t('Edit'),
    ),
    'plus-circle' => array(
      t('Add'),
      t('Write'),
    ),
    'trash-o' => array(
      t('Delete'),
      t('Remove'),
    ),
    'upload' => array(
      t('Upload'),
    ),
    'eye' => array(
      t('Preview'),
    ),
  );
  // Specific matching first.
  foreach ($specific_strings as $icon => $strings) {
    foreach ($strings as $string) {
      if (strpos(drupal_strtolower($text), drupal_strtolower($string)) !== FALSE) {
        return $icon;
      }
    }
  }
  // Generic matching last.
  foreach ($generic_strings as $icon => $strings) {
    foreach ($strings as $string) {
      if (strpos(drupal_strtolower($text), drupal_strtolower($string)) !== FALSE) {
        return $icon;
      }
    }
  }
  return FALSE;
}

/**
 * Return icon by status.
 *
 * @param string $status
 *   Status (info/success/warning/danger).
 *
 * @return string
 *   Themed icon for status.
 */
function _caffelatte_get_status_icon($status) {
  if (!empty($status)) {
    switch ($status) {
      case 'info':
        return theme('icon_fw', array('icon' => 'info-circle', 'attributes' => array('class' => array('text-info'))));

      break;
      case 'success':
        return theme('icon_fw', array('icon' => 'check', 'attributes' => array('class' => array('text-success'))));

      break;
      case 'warning':
        return theme('icon_fw', array('icon' => 'warning', 'attributes' => array('class' => array('text-warning'))));

      break;
      case 'danger':
        return theme('icon_fw', array('icon' => 'times-circle', 'attributes' => array('class' => array('text-danger'))));

      break;
      default:
        break;
    }
  }
}


/**
 * Allowed form element types for input group.
 */
function _caffelatte_input_group_allowed_types() {
  return array(
    'password',
    'password_confirm',
    'select',
    'textfield',
    'file',
    'date',
    // Elements module.
    'emailfield',
    'numberfield',
    'rangefield',
    'searchfield',
    'telfield',
    'urlfield',
    // Caffelatte.
    'colorfield',
    'datefield',
  );
}

/**
 * Convert js date format to PHP dateformat.
 *
 * @param string $dateformat
 *   Bootstrap datepicker date format.
 *
 * @return string
 *   PHP date format.
 */
function _caffelatte_dateformat_js_to_php($dateformat) {
  $symbols_js = array(
    '#mm#',
    '#m#',
    '#dd#',
    '#d#',
    '#yyyy#',
    '#yy#',
    '#DD#',
    '#D#',
    '#MM#',
    '#M#',
  );
  $symbols_php = array(
    'I',
    'n',
    'J',
    'j',
    'Y',
    'y',
    'L',
    'l',
    'F',
    'M',
  );
  $replace = preg_replace($symbols_js, $symbols_php, $dateformat, 1);

  $fake_php = array('#I#i', '#J#i', '#L#i');
  $real_php = array('m', 'd', 'D');
  $replace = preg_replace($fake_php, $real_php, $replace, 1);
  return $replace;
}
