<?php
/**
 * @file
 * icon-fw.func.php
 */

/**
 * Implements caffelatte_icon_fw().
 */
function caffelatte_icon_fw($variables) {
  if (!is_array($variables['styles'])) {
    $variables['styles'] = array();
  }
  array_unshift($variables['styles'], 'fw');
  return theme('icon', $variables);
}
