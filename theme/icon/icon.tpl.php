<?php
/**
 * @file
 * icon.tpl.php
 *
 * Markup for icons.
 *
 * Variables
 * - $attributes: HTML attributes with Font Awesome classes.
 */

?>
<i<?php print $attributes ?> aria-hidden="true"></i>
