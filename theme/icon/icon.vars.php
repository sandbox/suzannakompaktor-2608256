<?php
/**
 * @file
 * icon.vars.php
 */

/**
 * Implements hook_preprocess_icon().
 *
 * Requires FontAwesome.
 *
 * @see template_preprocess_icon()
 */
function caffelatte_preprocess_icon(&$variables) {
  $icon_suffix = theme_get_setting('caffelatte_icon');
  // Init class array.
  if (!array_key_exists('class', $variables['attributes'])) {
    $variables['attributes']['class'] = array();
  }
  // Add icon class if there's no.
  if (!in_array($icon_suffix, $variables['attributes']['class'])) {
    $variables['attributes']['class'][] = $icon_suffix;
  }

  $variables['attributes']['class'][] = $icon_suffix . "-" . $variables['icon'];

  if (isset($variables['styles']) && is_array($variables['styles'])) {
    foreach ($variables['styles'] as $style) {
      $variables['attributes']['class'][] = $icon_suffix . "-" . $style;
    }
  }
}

/**
 * Implements hook_process_icon().
 */
function caffelatte_process_icon(&$variables) {
  $variables['attributes'] = drupal_attributes($variables['attributes']);
}
