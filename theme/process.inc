<?php
/**
 * @file
 * process.inc
 *
 * Contains various implementations for #process callbacks on elements.
 */

/**
 * Process form.
 *
 * Add default '#client_validation' (bool)
 *
 * @see caffelatte_preprocess_form()
 * @see caffelatte_load_plugin_validate()
 */
function _caffelatte_process_form(&$element, &$form_state) {

  if (theme_get_setting('caffelatte_allow_jquery_validate') && !isset($element['#client_validation'])) {
    $element['#client_validation'] = TRUE;
  }

  $element['#client_validation'] = isset($element['#client_validation']) ? $element['#client_validation'] : FALSE;

  return $element;
}

/**
 * Process input elements.
 *
 * Add form-control class.
 */
function _caffelatte_process_input(&$element, &$form_state) {
  // Only add the "form-control" class for specific element input types.
  $types = array(
    // Core.
    'password',
    'password_confirm',
    'select',
    'textarea',
    'textfield',
    'file',
    'date',
    // Elements module.
    'emailfield',
    'numberfield',
    'rangefield',
    'searchfield',
    'telfield',
    'urlfield',
    // Caffelatte.
    'colorfield',
    'datefield',
  );
  if (!empty($element['#type']) && in_array($element['#type'], $types)) {
    $element['#attributes']['class'][] = 'form-control';
  }
  return $element;
}

/**
 * Process textarea elements.
 *
 * Add default '#autosize' (bool)
 *
 * @see caffelatte_preprocess_textarea()
 * @see caffelatte_load_plugin_autosize()
 * Add default '#bootstrap_maxlength' (bool)
 * @see caffelatte_preprocess_textarea()
 * @see caffelatte_load_plugin_maxlength()
 */
function _caffelatte_process_textarea(&$element, &$form_state) {

  if (theme_get_setting('caffelatte_allow_autosize') && !isset($element['#autosize'])) {
    $element['#autosize'] = TRUE;
  }

  if (theme_get_setting('caffelatte_textarea_allow_maxlength') && !isset($element['#bootstrap_maxlength'])) {
    $element['#bootstrap_maxlength'] = TRUE;
  }

  $element['#autosize'] = isset($element['#autosize']) ? $element['#autosize'] : FALSE;
  $element['#bootstrap_maxlength'] = isset($element['#bootstrap_maxlength']) ? $element['#bootstrap_maxlength'] : FALSE;

  return $element;
}

/**
 * Process textfield elements.
 *
 * Add default '#input_group' (addon, icon or btn)
 *
 * @see caffelatte_form_element()
 * @see caffelatte_preprocess_input_group()
 * @see input-group.tpl.php
 * Add default '#bootstrap_maxlength' (bool)
 * @see caffelatte_preprocess_textfield()
 * @see caffelatte_load_plugin_maxlength()
 */
function _caffelatte_process_textfield(&$element, &$form_state) {
  $element['#input_group'] = 'addon';

  if (theme_get_setting('caffelatte_textfield_allow_maxlength') && !isset($element['#bootstrap_maxlength'])) {
    $element['#bootstrap_maxlength'] = TRUE;
  }

  $element['#bootstrap_maxlength'] = isset($element['#bootstrap_maxlength']) ? $element['#bootstrap_maxlength'] : FALSE;

  return $element;
}

/**
 * Process select elements.
 *
 * Add default '#input_group' (addon, icon or btn)
 *
 * @see caffelatte_form_element()
 * @see caffelatte_preprocess_input_group()
 * @see input-group.tpl.php
 * Add default '#bootstrap_tagsinput' (bool)
 * @see caffelatte_preprocess_select()
 * @see caffelatte_load_plugin_tagsinput()
 */
function _caffelatte_process_select(&$element, &$form_state) {
  $element['#input_group'] = 'addon';

  $element['#bootstrap_tagsinput'] = isset($element['#bootstrap_tagsinput']) ? $element['#bootstrap_tagsinput'] : FALSE;

  return $element;
}

/**
 * Process checkbox and radio elements.
 *
 * Add default '#styles'
 *
 * @see _caffelatte_create_styles()
 */
function _caffelatte_process_checkbox_radio(&$element, &$form_state) {
  $element['#styles'] = isset($element['#styles']) ? $element['#styles'] : '';

  return $element;
}

/**
 * Process date elements.
 *
 * Add default '#datepicker' (bool)
 *
 * @see caffelatte_datefield()
 * @see caffelatte_load_plugin_datepicker()
 */
function _caffelatte_process_datefield(&$element, &$form_state) {
  $element['#input_group'] = 'addon';
  $element['#theme'] = 'datefield';

  if (theme_get_setting('caffelatte_allow_datepicker') && !isset($element['#datepicker'])) {
    $element['#datepicker'] = TRUE;
  }

  $element['#datepicker'] = isset($element['#datepicker']) ? $element['#datepicker'] : FALSE;

  if ($element['#datepicker']) {
    $element['year']['#select2'] = FALSE;
    $element['month']['#select2'] = FALSE;
    $element['day']['#select2'] = FALSE;
  }

  $element['year']['#attributes']['class'] = array('form-date-year');
  $element['month']['#attributes']['class'] = array('form-date-month');
  $element['day']['#attributes']['class'] = array('form-date-day');

  return $element;
}

/**
 * Process file elements.
 *
 * Add default '#input_group' (addon, icon or btn)
 *
 * @see caffelatte_form_element()
 * @see caffelatte_preprocess_input_group()
 * @see input-group.tpl.php
 * Add default '#file_input_type' (file or managed_file)
 * @see caffelatte_preprocess_file()
 */
function _caffelatte_process_file(&$element) {
  $element['#input_group'] = 'addon';

  if (count($element['#parents']) > 1) {
    $element['#file_input_type'] = 'managed_file';
  }
  else {
    $element['#file_input_type'] = 'file';
  }

  return $element;
}

/**
 * Process password elements.
 */
function _caffelatte_process_password(&$element, &$form_state) {
  $element['#input_group'] = 'addon';
  // Maxlength to false.
  $element['#bootstrap_maxlength'] = FALSE;

  return $element;
}

/**
 * Process date elements.
 *
 * Add default '#input_group' (addon, icon or btn)
 *
 * @see caffelatte_form_element()
 * @see caffelatte_preprocess_input_group()
 * Add Colorpicker plugin
 * @see caffelatte_load_plugin_colorpicker
 */
function _caffelatte_process_colorfield(&$element) {
  $element['#input_group'] = 'addon';

  // Add icon display.
  if (theme_get_setting('caffelatte_colorpicker_icon')) {
    // Set direction.
    $direction = theme_get_setting('caffelatte_colorpicker_icon_display');
    $element['#field_' . $direction] = '<i></i>';
    // Add colorpicker plugin.
    $element['#input_group_attributes']['data-plugin-colorpicker'] = TRUE;

    if ($direction == 'prefix') {
      $element['#input_group_attributes']['data-plugin-colorpicker-align'] = "left";
    }
  }
  return $element;
}
