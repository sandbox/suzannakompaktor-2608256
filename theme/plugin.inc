<?php
/**
 * @file
 * plugin.inc
 *
 * Load plugins.
 */

/**
 * Form Description.
 */
function caffelatte_load_plugin_form_description($options = array()) {
  $icon_attributes = array('aria-label' => t('Help'), 'class' => array('icon'));
  if (theme_get_setting('caffelatte_descriptions_style') == 'muted') {
    $icon_attributes['class'][] = 'text-muted';
  }
  if (!isset($options['icon'])) {
    $options['icon'] = theme('icon', array('icon' => 'question-circle', 'attributes' => $icon_attributes));
  }
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/javascripts/caffelatte.form-description.js');
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/plugins/form-description.js');
  drupal_add_js(array(
  'caffelatte' => array(
    'formDescription' => $options,
  ),
  ), 'setting');
}

/**
 * Lightbox.
 *
 * @see http://dimsemenov.com/plugins/magnific-popup/
 */
function caffelatte_load_plugin_lightbox($options = array()) {
  $options = array_merge(
    array(
      'tClose' => t('Close (Esc)'),
      'tLoading' => t('Loading...'),
      'gallery' => array(
        'tPrev' => t('Previous (Left arrow key)'),
        'tNext' => t('Next (Right arrow key)'),
        'tCounter' => t('%curr% of %total%'),
      ),
      'image' => array(
        'tError' => t('<a href="%url%">The image</a> could not be loaded.'),
      ),
      'ajax' => array(
        'tError' => t('<a href="%url%">The content</a> could not be loaded.'),
      ),
    ),
    $options
  );

  $from = theme_get_setting('caffelatte_lightbox_load_from');
  if ($from == 'external') {
    _caffelatte_load_external_plugin('lightbox');
  }
  else {
    drupal_add_css(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/magnific-popup/magnific-popup.css');
    drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/magnific-popup/magnific-popup.min.js', array('scope' => 'footer'));
  }

  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/javascripts/caffelatte.plugin.lightbox.js', array('scope' => 'footer', 'weight' => 10));
  drupal_add_js(array(
  'caffelatte' => array(
    'lightbox' => $options,
  ),
  ), 'setting');
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/plugins/lightbox.js', array('scope' => 'footer', 'weight' => 20));
}

/**
 * Lightbox popups.
 */
function caffelatte_load_plugin_lightbox_popups($options = array()) {
  caffelatte_load_plugin_lightbox();

  $options['gallerySelector'] = theme_get_setting('caffelatte_lightbox_popups_gallery_selector');
  $options['popupSelector'] = theme_get_setting('caffelatte_lightbox_popups_popup_selector');

  drupal_add_js(array(
  'caffelatte' => array(
    'lightbox' => $options,
  ),
  ), 'setting');
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/plugins/lightbox-gallery.js', array('scope' => 'footer', 'weight' => 20));
}


/**
 * Autocomplete.
 */
function caffelatte_load_plugin_autocomplete($options = array()) {
  $options += array(
    'icon' => theme_get_setting('caffelatte_autocomplete_icon'),
    'iconActive' => theme_get_setting('caffelatte_autocomplete_icon_active'),
    'iconAnim' => theme_get_setting('caffelatte_autocomplete_icon_anim'),
  );
  drupal_add_js(array(
  'caffelatte' => array(
    'autocomplete' => $options,
  ),
  ), 'setting');
}

/**
 * JQuery Appear.
 *
 * @see https://github.com/morr/jquery.appear
 */
function caffelatte_load_plugin_appear() {
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/jquery-appear/jquery.appear.js', array('scope' => 'footer'));
}

/**
 * Animate CSS.
 *
 * @see https://daneden.github.io/animate.css/
 */
function caffelatte_load_plugin_animate($options = array()) {
  caffelatte_load_plugin_appear();
  drupal_add_css(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/animate/animate.css');
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/javascripts/caffelatte.plugin.animation.js', array('scope' => 'footer', 'weight' => 10));
  drupal_add_js(array(
  'caffelatte' => array(
    'animation' => $options,
  ),
  ), 'setting');
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/plugins/animation.js', array('scope' => 'footer', 'weight' => 20));
}

/**
 * JQuery Autosize.
 *
 * @see http://www.jacklmoore.com/autosize/
 */
function caffelatte_load_plugin_autosize($options = array()) {

  $from = theme_get_setting('caffelatte_autosize_load_from');
  // Set to module if jQuery Autosize module enabled.
  if (module_exists('jquery_autosize')) {
    $from = 'module';
  }

  if ($from == 'theme') {
    drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/jquery-autosize/jquery.autosize.min.js', array('scope' => 'footer'));
  }
  elseif ($from == 'external') {
    _caffelatte_load_external_plugin('autosize');
  }

  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/javascripts/caffelatte.plugin.autosize.js', array('scope' => 'footer', 'weight' => 10));
  drupal_add_js(array(
  'caffelatte' => array(
    'autosize' => $options,
  ),
  ), 'setting');
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/plugins/autosize.js', array('scope' => 'footer', 'weight' => 20));
}

/**
 * Colorpicker.
 *
 * @see http://mjolnic.github.io/bootstrap-colorpicker/
 */
function caffelatte_load_plugin_colorpicker($options = array()) {
  $options += array(
    'format' => theme_get_setting('caffelatte_colorpicker_format'),
    'horizontal' => theme_get_setting('caffelatte_colorpicker_horizontal'),
  );
  drupal_add_css(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css');
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js', array('scope' => 'footer'));
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/javascripts/caffelatte.plugin.colorpicker.js', array('scope' => 'footer', 'weight' => 10));
  drupal_add_js(array(
  'caffelatte' => array(
    'colorpicker' => $options,
  ),
  ), 'setting');
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/plugins/colorpicker.js', array('scope' => 'footer', 'weight' => 20));
}

/**
 * Datepicker.
 *
 * @see https://github.com/eternicode/bootstrap-datepicker/
 */
function caffelatte_load_plugin_datepicker($options = array()) {
  global $language;

  $options += array(
    'format' => theme_get_setting('caffelatte_datepicker_format'),
    'pluginSkin' => theme_get_setting('caffelatte_datepicker_plugin_skin'),
  );
  drupal_add_css(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/bootstrap-datepicker/css/datepicker3.css');
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js', array('scope' => 'footer'));

  if (file_exists(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/bootstrap-datepicker/js/locales/bootstrap-datepicker.' . $language->language . '.js')) {
    drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/bootstrap-datepicker/js/locales/bootstrap-datepicker.' . $language->language . '.js', array('scope' => 'footer', 'weight' => 10));
    $options['language'] = $language->language;
  }

  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/javascripts/caffelatte.plugin.datepicker.js', array('scope' => 'footer', 'weight' => 10));
  drupal_add_js(array(
  'caffelatte' => array(
    'datepicker' => $options,
  ),
  ), 'setting');
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/plugins/datepicker.js', array('scope' => 'footer', 'weight' => 20));
}

/**
 * Timepicker.
 *
 * @see https://github.com/jdewit/bootstrap-timepicker/
 */
function caffelatte_load_plugin_timepicker($options = array()) {
  $options += array(
    'showMeridian' => theme_get_setting('caffelatte_timepicker_show_meridian'),
    'minuteStep' => theme_get_setting('caffelatte_timepicker_minute_step'),
  );
  drupal_add_css(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css');
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js', array('scope' => 'footer'));
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/javascripts/caffelatte.plugin.timepicker.js', array('scope' => 'footer', 'weight' => 10));
  drupal_add_js(array(
  'caffelatte' => array(
    'timepicker' => $options,
  ),
  ), 'setting');
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/plugins/timepicker.js', array('scope' => 'footer', 'weight' => 20));
}

/**
 * Fileinput.
 *
 * @see http://plugins.krajee.com/file-input
 */
function caffelatte_load_plugin_fileinput($options = array()) {
  global $language;

  $icon_class = theme_get_setting('caffelatte_icon');

  $options += array(
    'language' => $language->language,
    'browseIcon' => theme('icon_fw', array('icon' => 'folder-open')),
    'removeClass' => 'btn btn-danger',
    'removeIcon' => theme('icon_fw', array('icon' => 'trash')),
    'uploadIcon' => theme('icon_fw', array('icon' => 'upload')),
    'cancelIcon' => theme('icon_fw', array('icon' => 'ban')),
    'msgValidationErrorIcon' => theme('icon_fw', array('icon' => 'times-circle')),
    'captionTemplate' => '<div tabindex="-1" class="form-control file-caption {class}">' .
    '   <span class="' . $icon_class . ' ' . $icon_class . '-file-o text-muted kv-caption-icon"></span><div class="file-caption-name"></div>' .
    '</div>',
    'previewFileIcon' => theme('icon_fw', array('icon' => 'file-o')),
    'zoomIndicator' => theme('icon_fw', array('icon' => 'fa-search-plus')),
    'fileActionSettings' => array(
      'removeIcon' => theme('icon_fw', array('icon' => 'trash')),
      'uploadIcon' => theme('icon_fw', array('icon' => 'upload')),
      'indicatorNew' => theme('icon_fw', array('icon' => 'plus-circle', 'attributes' => array('class' => array('text-warning')))),
      'indicatorSuccess' => theme('icon_fw', array('icon' => 'check', 'attributes' => array('class' => array('text-success')))),
      'indicatorError' => theme('icon_fw', array('icon' => 'times-circle')),
      'indicatorLoading' => theme('icon_fw', array('icon' => 'refresh')),
    ),
    'layoutTemplates' => array(
      'icon' => '<span class="' . $icon_class . ' ' . $icon_class . '-file-o kv-caption-icon"></span> ',
    ),
  );

  drupal_add_css(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/bootstrap-fileinput/css/fileinput.min.css');
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/bootstrap-fileinput/js/fileinput.min.js', array('scope' => 'footer'));
  if (file_exists(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/bootstrap-fileinput/js/fileinput_locale_' . $language->language . '.js')) {
    drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/bootstrap-fileinput/js/fileinput_locale_' . $language->language . '.js', array('scope' => 'footer', 'weight' => 10));
  }

  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/javascripts/caffelatte.plugin.fileinput.js', array('scope' => 'footer', 'weight' => 10));
  drupal_add_js(array(
  'caffelatte' => array(
    'fileinput' => $options,
  ),
  ), 'setting');
}

/**
 * Bootstrap Maxlength.
 *
 * @see http://mimo84.github.io/bootstrap-maxlength/
 */
function caffelatte_load_plugin_maxlength($options = array()) {
  $separator = theme_get_setting('caffelatte_maxlength_separator');
  $pre_text = theme_get_setting('caffelatte_maxlength_pre_text');
  $post_text = theme_get_setting('caffelatte_maxlength_post_text');
  $options += array(
    'warningClass' => theme_get_setting('caffelatte_maxlength_warning_class'),
    'separator' => t($separator),
    'preText' => t($pre_text),
    'postText' => t($post_text),
  );
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/bootstrap-maxlength/bootstrap-maxlength.min.js', array('scope' => 'footer'));
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/javascripts/caffelatte.plugin.maxlength.js', array('scope' => 'footer', 'weight' => 10));
  drupal_add_js(array(
  'caffelatte' => array(
    'maxlength' => $options,
  ),
  ), 'setting');
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/plugins/maxlength.js', array('scope' => 'footer', 'weight' => 20));
}

/**
 * Select2.
 *
 * @see https://select2.github.io/
 */
function caffelatte_load_plugin_select2($options = array()) {
  global $language;

  drupal_add_css(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/select2/select2.css');
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/select2/select2.min.js', array('scope' => 'footer'));

  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/javascripts/caffelatte.plugin.select2.js', array('scope' => 'footer', 'weight' => 10));
  drupal_add_js(array(
  'caffelatte' => array(
    'select2' => $options,
  ),
  ), 'setting');
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/plugins/select2.js', array('scope' => 'footer', 'weight' => 20));
}

/**
 * Slider.
 */
function caffelatte_load_plugin_slider($options = array()) {
  drupal_add_library('system', 'ui.slider');
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/jquery.ui.touch-punch/jquery.ui.touch-punch.min.js', array('scope' => 'footer'));
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/javascripts/caffelatte.plugin.slider.js', array('scope' => 'footer', 'weight' => 10));
  drupal_add_js(array(
  'caffelatte' => array(
    'slider' => $options,
  ),
  ), 'setting');
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/plugins/slider.js', array('scope' => 'footer', 'weight' => 20));
}

/**
 * Spinner.
 */
function caffelatte_load_plugin_spinner() {
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/fuelux/js/spinbox.js', array('scope' => 'footer'));
}

/**
 * Tagsinput.
 */
function caffelatte_load_plugin_tagsinput() {
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js', array('scope' => 'footer'));
}

/**
 * Validate.
 *
 * @see http://jqueryvalidation.org/
 */
function caffelatte_load_plugin_validate() {
  global $language;

  $from = theme_get_setting('caffelatte_jquery_validate_load_from');
  // Set to module if Clientside Validation enabled.
  if (module_exists('clientside_validation')) {
    $from = 'module';
  }

  if ($from == 'theme') {
    drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/jquery-validation/jquery.validate.min.js', array('scope' => 'footer'));
    /*
     * @todo: handle pt-BR, zh-CN, etc
     */
    if (file_exists(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/jquery-validation/localization/messages_' . $language->language . '.min.js')) {
      drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/jquery-validation/localization/messages_' . $language->language . '.min.js', array('scope' => 'footer'));
    }
  }
  elseif ($from == 'external') {
    _caffelatte_load_external_plugin('jquery_validate');
  }

  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/plugins/validate.js', array('scope' => 'footer', 'weight' => 20));
}

/**
 * Helper function to load plugins from external location or libraries dir.
 */
function _caffelatte_load_external_plugin($plugin_name) {
  global $language;
  $lib_path = 'sites/all/libraries';

  $plugin_js = theme_get_setting('caffelatte_' . $plugin_name . '_external_js');

  if (isset($plugin_js)) {
    $options = array();
    if (preg_match('/^%l/', $plugin_js)) {
      $plugin_js = preg_replace('/^%l/', $lib_path, $plugin_js);
    }
    else {
      $options = array('type' => 'external');
    }
    drupal_add_js($plugin_js, $options);
  }

  $plugin_css = theme_get_setting('caffelatte_' . $plugin_name . '_external_css');
  if (isset($plugin_css)) {
    $options = array();
    if (preg_match('/^%l/', $plugin_css)) {
      $plugin_css = preg_replace('/^%l/', $lib_path, $plugin_css);
    }
    else {
      $options = array('type' => 'external');
    }
    drupal_add_css(theme_get_setting('caffelatte_' . $plugin_name . '_external_css'), $options);
  }

  $plugin_lang = theme_get_setting('caffelatte_' . $plugin_name . '_external_lang');
  if (isset($plugin_lang)) {
    $options = array();
    if (preg_match('/^%l/', $plugin_lang)) {
      $plugin_lang = preg_replace('/^%l/', $lib_path, $plugin_lang);
    }
    else {
      $options = array('type' => 'external');
    }
    $file = str_replace('##', $language->language, $plugin_lang);
    if (file_exists($file)) {
      drupal_add_js($file, $options);
    }
  }
}
