<?php
/**
 * @file
 * menu-local-action.func.php
 */

/**
 * Overrides theme_menu_local_action().
 */
function caffelatte_menu_local_action($variables) {
  $link = $variables['element']['#link'];

  $options = isset($link['localized_options']) ? $link['localized_options'] : array();

  // If the title is not HTML, sanitize it.
  if (empty($options['html'])) {
    $link['title'] = check_plain($link['title']);
  }

  // Format the action link.
  $output = '<li>';
  if (isset($link['href'])) {
    $link['title'] = theme('icon', array('icon' => 'plus-circle')) . $link['title'];
    // Turn link into label.
    if (!isset($options['attributes']['class'])) {
      $options['attributes']['class'] = array();
    }
    $string = is_string($options['attributes']['class']);
    if ($string) {
      $options['attributes']['class'] = explode(' ', $options['attributes']['class']);
    }
    $options['attributes']['class'][] = 'label';
    $options['attributes']['class'][] = 'label-' . theme_get_setting('caffelatte_local_action_color_style');
    if ($string) {
      $options['attributes']['class'] = implode(' ', $options['attributes']['class']);
    }
    // Force HTML so we can add the icon rendering element.
    $options['html'] = TRUE;
    $output .= l($link['title'], $link['href'], $options);
  }
  else {
    $output .= $link['title'];
  }
  $output .= "</li>\n";

  return $output;
}
