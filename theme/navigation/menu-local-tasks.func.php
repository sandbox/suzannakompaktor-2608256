<?php
/**
 * @file
 * menu-local-tasks.func.php
 */

/**
 * Overrides theme_menu_local_tasks().
 */
function caffelatte_menu_local_tasks(&$variables) {
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/tabs.js');
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Tab Navigation') . '</h2>' . "\n";
    $classes = '';
    if (!empty($variables['secondary'])) {
      $classes .= ' has-secondary';
    }
    $variables['primary']['#prefix'] = '<ul class="nav-tabs' . $classes . '">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }

  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<ul class="nav-tabs-secondary">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}
