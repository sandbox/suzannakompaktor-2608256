<?php
/**
 * @file
 * menu-tree.func.php
 */

/**
 * Overrides theme_menu_tree().
 */
function caffelatte_menu_tree(&$variables) {
  return '<ul class="menu nav">' . $variables['tree'] . '</ul>';
}

/**
 * Bootstrap theme wrapper function for the primary menu links.
 */
function caffelatte_menu_tree__primary(&$variables) {
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/navbar.js');
  return '<ul class="menu nav navbar-nav">' . $variables['tree'] . '</ul>';
}

/**
 * Bootstrap theme wrapper function for the secondary menu links.
 */
function caffelatte_menu_tree__secondary(&$variables) {
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/navbar.js');
  return '<ul class="menu nav navbar-nav secondary">' . $variables['tree'] . '</ul>';
}
