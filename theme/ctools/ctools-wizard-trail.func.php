<?php

/**
 * @file
 * Override the wizard tool.
 */

/**
 * Themable display of the 'breadcrumb' trail to show the order of the forms.
 */
function caffelatte_ctools_wizard_trail($vars) {
  $output = '';
  if (!empty($vars['trail'])) {
    $output .= '<div class="ctools-wizard wizard-tabs"><ul class="wizard-steps">';
    foreach ($vars['trail'] as $key => $value) {
      $class = '';
      if (strpos($value, 'wizard-trail-current')) {
        $class = ' class="active"';
      }
      $output .= '<li' . $class . '><a class="text-center">' . $value . '</a></li>';
    }
    $output .= '</ul></div>';
  }
  return $output;
}
