<?php
/**
 * @file
 * alter.inc
 *
 * Contains various implementations of hook_*_alter().
 */

/**
 * Implements hook_css_alter().
 */
function caffelatte_css_alter(&$css) {
  $theme_path = drupal_get_path('theme', 'caffelatte');

  // Exclude specified CSS files from theme.
  $excludes = caffelatte_get_theme_info(NULL, 'exclude][css');
  if (!empty($excludes)) {
    $css = array_diff_key($css, drupal_map_assoc($excludes));
  }
}

/**
 * Implements hook_js_alter().
 */
function caffelatte_js_alter(&$js) {
  $theme_path = drupal_get_path('theme', 'caffelatte');

  $excludes = caffelatte_get_theme_info(NULL, 'exclude][js');

  // Default jquery version to 1.7.
  $jquery_version = array('1', '7');
  // Check is jquery_update module exists.
  if (module_exists('jquery_update')) {
    // Get jquery_update admin version.
    $jquery_update_jquery_admin_version = variable_get('jquery_update_jquery_admin_version', '');
    if (!empty($jquery_update_jquery_admin_version)) {
      $jquery_version = explode(".", $jquery_update_jquery_admin_version);
    }
    else {
      $jquery_update_jquery_version = variable_get('jquery_update_jquery_version', '');
      if (!empty($jquery_update_jquery_version)) {
        $jquery_version = explode(".", $jquery_update_jquery_version);
      }
    }
  }

  // Check if jquery_update module doesnt exists or jqery version is under 1.10 and add theme's jquery if necessary.
  if ($jquery_version[1] < 10) {
    $path = $theme_path . '/assets/vendor/jquery/jquery.min.js';
    // Copy the current jQuery file settings and change.
    $key = !module_exists('jquery_update') ? 'misc/jquery.js' : drupal_get_path('module', 'jquery_update') . '/replace/jquery/' . implode('.', $jquery_version) . '/jquery.min.js';
    $js[$path] = $js[$key];

    // Update necessary settings.
    $js[$path]['version'] = 1.10;
    $js[$path]['data'] = $path;

    // Finally remove the original jQuery.
    unset($js[$key]);
  }

  // Add or replace JavaScript files when matching paths are detected.
  // Replacement files must begin with '_', like '_node.js'.
  $files = file_scan_directory($theme_path . '/js', '/\.js$/');
  foreach ($files as $file) {
    $path = str_replace($theme_path . '/js/', '', $file->uri);
    // Detect if this is a replacement file.
    $replace = FALSE;
    if (preg_match('/^[_]/', $file->filename)) {
      $replace = TRUE;
      $path = dirname($path) . '/' . preg_replace('/^[_]/', '', $file->filename);
    }
    $matches = array();
    if (preg_match('/^modules\/([^\/]*)/', $path, $matches)) {
      if (!module_exists($matches[1])) {
        continue;
      }
      else {
        $path = str_replace('modules/' . $matches[1], drupal_get_path('module', $matches[1]), $path);
      }
    }
    // Path should always exist to either add or replace JavaScript file.
    if (!empty($js[$path])) {
      // Replace file.
      if ($replace) {
        $js[$file->uri] = $js[$path];
        $js[$file->uri]['data'] = $file->uri;
        unset($js[$path]);
      }
      // Add file.
      else {
        $js[$file->uri] = drupal_js_defaults($file->uri);
        $js[$file->uri]['group'] = JS_THEME;
      }
    }
  }

  // Always add bootstrap.js last.
  $bootstrap = $theme_path . '/assets/vendor/bootstrap/js/bootstrap.min.js';
  $js[$bootstrap] = drupal_js_defaults($bootstrap);
  $js[$bootstrap]['group'] = JS_THEME;
  $js[$bootstrap]['scope'] = 'footer';

  // Exclude specified CSS files from theme.
  if (!empty($excludes)) {
    $js = array_diff_key($js, drupal_map_assoc($excludes));
  }

  // Add stickyheader offset.
  if (isset($js['misc/tableheader.js'])) {
    drupal_add_js(array('tableHeaderOffset' => 'Drupal.caffelatte.navbarHeight'), 'setting');
  }
}

/**
 * Implements hook_element_info_alter().
 */
function caffelatte_element_info_alter(&$elements) {
  foreach ($elements as &$element) {
    // Process input elements.
    if (!empty($element['#input'])) {
      $element['#process'][] = '_caffelatte_process_input';
    }
    switch ($element['#type']) {
      case 'form':
        $element['#process'][] = '_caffelatte_process_form';
        break;

      case 'textarea':
        $element['#process'][] = '_caffelatte_process_textarea';
        break;

      case 'textfield':
      case 'machine_name':
        $element['#process'][] = '_caffelatte_process_textfield';
        if ($element['#theme'] == 'colorfield') {
          $element['#process'][] = '_caffelatte_process_colorfield';
        }
        break;

      case 'select':
        $element['#process'][] = '_caffelatte_process_select';
        break;

      case 'checkbox':
      case 'radio':
        $element['#process'][] = '_caffelatte_process_checkbox_radio';
        break;

      case 'date':
        $element['#process'][] = '_caffelatte_process_datefield';
        break;

      case 'file':
        $element['#process'][] = '_caffelatte_process_file';
        break;

      case 'password':
      case 'password_confirm':
        $element['#process'][] = '_caffelatte_process_password';
        break;

      case 'colorfield':
        $element['#process'][] = '_caffelatte_process_colorfield';
        break;

      case 'contextual_links':
        $element['#attached']['css'] = drupal_add_css(drupal_get_path('theme', 'caffelatte') . '/css/contextual.css', array('weight' => 20));
        break;
    }
  }
}

/**
 * Implements hook_page_alter().
 */
function caffelatte_page_alter(&$page) {
  // Add path to theme to Drupal.settings.
  drupal_add_js('jQuery.extend(Drupal.settings, { "pathToTheme": "' . path_to_theme() . '"});', 'inline');
  // Add icon prefix to settings.
  drupal_add_js(array('clIcon' => theme_get_setting('caffelatte_icon')), 'setting');

  // Modernizr.
  drupal_add_js('//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array('type' => 'external'));
  // Google Font.
  drupal_add_css('//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&subset=latin', array('type' => 'external'));
  // html5data.
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/vendor/jquery-html5data/jquery.html5data.min.js');

  if (theme_get_setting('caffelatte_allow_lightbox_popups')) {
    caffelatte_load_plugin_lightbox_popups();
  }

  // Go top.
  if (theme_get_setting('caffelatte_display_go_top')) {
    drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/assets/javascripts/caffelatte.plugin.scrolltotop.js');
    $page['page_bottom']['go_top'] = array('#markup' => theme('go_up'));
  }
}

/**
 * Implements hook_form_alter().
 */
function caffelatte_form_alter(array &$form, array &$form_state = array(), $form_id = NULL) {
  // Add button style to links.
  if (isset($form['actions'])) {
    foreach (element_children($form['actions']) as $key) {
      if ($form['actions'][$key]['#type'] == 'link') {
        $form['actions'][$key]['#attributes']['class'][] = 'btn';
        $form['actions'][$key]['#attributes']['class'][] = 'btn-default';
        $form['actions'][$key]['#attributes']['class'][] = 'ml-xs';
      }
    }
  }

  if ($form_id) {
    switch ($form_id) {
      case 'system_theme_settings':
        // Include the settings form here.
        caffelatte_include('caffelatte', 'theme/settings.inc');
        _caffelatte_settings_form($form, $form_state);
        break;

      case 'search_form':
        $form['#client_validation'] = FALSE;
        $form['basic']['keys']['#bootstrap_maxlength'] = FALSE;
        $form['basic']['keys']['#title_display'] = 'none';
        $form['basic']['keys']['#size'] = 18;
        $form['basic']['keys']['#theme'] = 'searchfield';
        $form['basic']['keys']['#attributes']['placeholder'] = t('Search...');
        $form['basic']['keys']['#theme_wrappers'] = array();
        $form['basic']['submit']['#icon'] = 'search';
        $form['basic']['submit']['#styles'] = 'default';
        $form['basic']['submit']['#value_attributes'] = array('class' => array('hidden'));
        $form['basic']['#theme_wrappers'] = array('input_group', 'search_form_wrapper');
        break;

      case 'search_block_form':
        $form['#client_validation'] = FALSE;
        $form['#theme_wrappers'][] = 'input_group';
        $form['search_block_form']['#bootstrap_maxlength'] = FALSE;
        break;

      case 'node_admin_content':
        $form['filter']['filters']['#collapsible'] = TRUE;
        break;

      case 'views_ui_edit_form':
        $form['#attached']['css'][drupal_get_path('theme', 'caffelatte') . '/css/views-admin.caffelatte.css'] = array();
        break;
    }

  }
}

/**
 * Include #process callbacks for elements.
 */
caffelatte_include('caffelatte', 'theme/process.inc');
