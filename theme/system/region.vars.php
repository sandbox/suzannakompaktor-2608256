<?php
/**
 * @file
 * region.vars.php
 */

/**
 * Implements hook_preprocess_region().
 */
function caffelatte_preprocess_region(&$variables) {

  switch ($variables['region']) {
    case 'help':
      $variables['content'] = theme('bootstrap_alert', array(
        'content' => theme('icon', array('icon' => 'info-circle', 'styles' => array('lg'))) . $variables['content'],
        'styles' => 'default',
      ));
      break;
  }
}
