<?php
/**
 * @file
 * html.vars.php
 *
 * @see html.tpl.php
 */

/**
 * Implements hook_preprocess_html().
 */
function caffelatte_preprocess_html(&$variables) {
  switch (theme_get_setting('caffelatte_navbar_position')) {
    case 'fixed-top':
      $variables['classes_array'][] = 'navbar-is-fixed-top';
      break;

    case 'fixed-bottom':
      $variables['classes_array'][] = 'navbar-is-fixed-bottom';
      break;

    case 'static-top':
      $variables['classes_array'][] = 'navbar-is-static-top';
      break;
  }

  if (!empty($variables['page']['highlighted'])) {
    $variables['classes_array'][] = 'page-has-highlighted';
  }

  $q = current_path();
  $path = explode('/', $q);
  if (in_array('manage', $path)) {
    $variables['classes_array'][] = 'page-manage';
  }
  if (in_array('add', $path)) {
    $variables['classes_array'][] = 'page-add';
  }

  $variables['classes'] = implode(" ", $variables['classes_array']);

}
