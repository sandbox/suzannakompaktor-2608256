<?php
/**
 * @file
 * breadcrumb.tpl.php
 *
 * Variables
 * - $breadcrumb: Array with list items.
 * - $type: List type.
 * - $styles: Caffelatte styles.
 */

?>
<?php if (!empty($breadcrumb)): ?>
  <h2 class="hidden" aria-hidden="false"><?php print t('You are here') ?></h2>
  <<?php print $type ?> class="breadcrumb<?php print $styles ?>">
    <?php foreach($breadcrumb as $ba): ?>
      <li><?php print $ba ?></li>
    <?php endforeach; ?>
  </<?php print $type ?> >
<?php endif; ?>
