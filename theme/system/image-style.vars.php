<?php
/**
 * @file
 * image-style.vars.php
 */

/**
 * Implements hook_preprocess_image_style().
 */
function caffelatte_preprocess_image_style(&$variables) {
  $variables['attributes']['class'][] = 'img-thumbnail';
}
