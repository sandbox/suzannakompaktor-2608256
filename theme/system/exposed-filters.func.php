<?php
/**
 * @file
 * exposed-filters.func.php
 */

/**
 * Overrides theme_exposed_filters().
 */
function caffelatte_exposed_filters($variables) {
  $form = $variables['form'];
  $output = '';

  foreach (element_children($form['status']['filters']) as $key) {
    $form['status']['filters'][$key]['#group_attributes']['class'] = array('col-xs-12');
    $form['status']['filters'][$key]['#label_attributes']['class'] = array('col-sm-3', 'col-md-2');
    $form['status']['filters'][$key]['#item_attributes']['class'] = array('col-sm-9', 'col-md-10');
  }

  $form['status']['actions']['#prefix'] = '<div class="col-xs-12">';
  $form['status']['actions']['#suffix'] = '</div>';

  if (isset($form['current'])) {
    $items = array();
    foreach (element_children($form['current']) as $key) {
      $items[] = drupal_render($form['current'][$key]);
    }
    $output .= theme('item_list', array(
      'items' => $items,
      'attributes' => array('class' => array('clearfix', 'current-filters')),
    ));
  }

  $output .= drupal_render_children($form);

  return '<div class="form-horizontal"><div class="col-xs-12">' . $output . '</div></div>';
}
