<?php
/**
 * @file
 * image-formatter.func.php
 */

/**
 * Overrides theme_image_formatter().
 */
function caffelatte_image_formatter($variables) {
  $item = $variables['item'];
  $image = array(
    'path' => $item['uri'],
  );

  if (array_key_exists('alt', $item)) {
    $image['alt'] = $item['alt'];
  }

  if (isset($item['attributes'])) {
    $image['attributes'] = $item['attributes'];
  }

  if (isset($variables['path']['path'])) {
    $image['attributes']['class'][] = 'img-responsive';
  }

  if (isset($item['width']) && isset($item['height'])) {
    $image['width'] = $item['width'];
    $image['height'] = $item['height'];
  }

  // Do not output an empty 'title' attribute.
  if (isset($item['title']) && drupal_strlen($item['title']) > 0) {
    $image['title'] = $item['title'];
  }

  if ($variables['image_style']) {
    $image['style_name'] = $variables['image_style'];
    $output = theme('image_style', $image);
  }
  else {
    $output = theme('image', $image);
  }

  // The link path and link options are both optional, but for the options to be
  // processed, the link path must at least be an empty string.
  if (isset($variables['path']['path'])) {
    $path = $variables['path']['path'];
    $options = isset($variables['path']['options']) ? $variables['path']['options'] : array();
    $options['attributes']['class'] = array('img-thumbnail');
    if (preg_match('/\.(jpe?g|png|gif)$/i', $path)) {
      $icon = theme('icon', array('icon' => 'search'));
      // Add lightbox if allowed.
      if (theme_get_setting('caffelatte_allow_lightbox_image')) {
        caffelatte_load_plugin_lightbox();
        $options['attributes']['data-plugin-lightbox'] = TRUE;
        $options['attributes']['data-plugin-lightbox-type'] = 'image';
      }
    }
    else {
      $icon = theme('icon', array('icon' => 'link'));
    }
    $icon = '<span class="zoom">' . $icon . '</<span>';
    $output = $output . $icon;
    // When displaying an image inside a link, the html option must be TRUE.
    $options['html'] = TRUE;

    $output = '<div class="thumbnail-gallery">' . l($output, $path, $options) . '</div>';
  }

  return $output;
}
