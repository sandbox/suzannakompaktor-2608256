<?php
/**
 * @file
 * page.vars.php
 */

/**
 * Implements hook_preprocess_page().
 *
 * @see page.tpl.php
 */
function caffelatte_preprocess_page(&$variables) {

  // Add information about the number of sidebars.
  if (!empty($variables['page']['sidebar_first']) && !empty($variables['page']['sidebar_second'])) {
    $variables['content_column_class'] = ' class="col-sm-6"';
  }
  elseif (!empty($variables['page']['sidebar_first']) || !empty($variables['page']['sidebar_second'])) {
    $variables['content_column_class'] = ' class="col-sm-9"';
  }
  else {
    $variables['content_column_class'] = ' class="col-sm-12"';
  }

  // Primary nav.
  $variables['primary_nav'] = FALSE;
  if ($variables['main_menu']) {
    // Primary nav.
    $item = menu_get_item();
    $tree = menu_tree_all_data(variable_get('menu_main_links_source', 'main-menu'), $item, 3);
    $variables['primary_nav']  = menu_tree_output($tree);
    // Provide default theme wrapper function.
    $variables['primary_nav']['#theme_wrappers'] = array('menu_tree__primary');
  }

  // Secondary nav.
  $variables['secondary_nav'] = FALSE;
  if ($variables['secondary_menu']) {
    // Build links.
    $variables['secondary_nav'] = menu_tree(variable_get('menu_secondary_links_source', 'user-menu'));
    // Provide default theme wrapper function.
    $variables['secondary_nav']['#theme_wrappers'] = array('menu_tree__secondary');
  }

  $variables['navbar_classes_array'] = array('navbar');

  if (theme_get_setting('caffelatte_navbar_position') !== '') {
    $variables['navbar_classes_array'][] = 'navbar-' . theme_get_setting('caffelatte_navbar_position');
  }
  else {
    $variables['navbar_classes_array'][] = 'container';
  }

  $variables['navbar_classes_array'][] = theme_get_setting('caffelatte_navbar_inverse') ? 'navbar-inverse' : 'navbar-default';

  $variables['footer_classes_array'] = array('footer');
  if (theme_get_setting('caffelatte_footer_inverse')) {
    $variables['footer_classes_array'][] = 'footer-inverse';
  }
  if (theme_get_setting('caffelatte_navbar_position') === '') {
    $variables['footer_classes_array'][] = 'container';
  }

  if ($variables['is_front']) {
    unset($variables['page']['content']['system_main']['default_message']);
  }

  $variables['main_highlight_classes_array'] = theme_get_setting('caffelatte_main_highlight_classes');
  if (theme_get_setting('caffelatte_navbar_position') === '') {
    $variables['main_highlight_classes_array'][] = 'container';
  }

  if (module_exists('search') && theme_get_setting('caffelatte_navbar_search')) {
    $search_box = drupal_get_form('search_form');
    $variables['search_box'] = $search_box;
  }
}

/**
 * Implements hook_process_page().
 *
 * @see page.tpl.php
 */
function caffelatte_process_page(&$variables) {
  $variables['navbar_classes'] = implode(' ', $variables['navbar_classes_array']);
  $variables['footer_classes'] = implode(' ', $variables['footer_classes_array']);
  if (!empty($variables['main_highlight_classes_array'])) {
    $variables['main_highlight_classes'] = ' ' . implode(' ', $variables['main_highlight_classes_array']);
  }
  if (isset($variables['search_box'])) {
    $variables['search_box'] = drupal_render($variables['search_box']);
  }
}
