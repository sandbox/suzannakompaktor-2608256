<?php
/**
 * @file
 * file-link.func.php
 */

/**
 * Overrides theme_file_link().
 */
function caffelatte_file_link($variables) {
  $file = $variables['file'];
  $has_icon = isset($variables['#has_icon']) ? $variables['#has_icon'] : TRUE;
  $icon = '';
  $icon_directory = $variables['icon_directory'];
  $url = file_create_url($file->uri);

  $options['attributes'] = isset($variables['#attributes']) ? $variables['#attributes'] : array();

  if ($has_icon) {

    // Human-readable names, for use as text-alternatives to icons.
    $mime_name = array(
      'application/msword' => t('Microsoft Office document icon'),
      'application/vnd.ms-excel' => t('Office spreadsheet icon'),
      'application/vnd.ms-powerpoint' => t('Office presentation icon'),
      'application/pdf' => t('PDF icon'),
      'video/quicktime' => t('Movie icon'),
      'audio/mpeg' => t('Audio icon'),
      'audio/wav' => t('Audio icon'),
      'image/jpeg' => t('Image icon'),
      'image/png' => t('Image icon'),
      'image/gif' => t('Image icon'),
      'application/zip' => t('Package icon'),
      'text/html' => t('HTML icon'),
      'text/plain' => t('Plain text icon'),
      'application/octet-stream' => t('Binary Data'),
    );

    $mimetype = file_get_mimetype($file->uri);

    $icon = theme('file_icon', array(
      'file' => $file,
      'alt' => !empty($mime_name[$mimetype]) ? $mime_name[$mimetype] : t('File'),
    ));
  }

  // Set options as per anchor format described at
  // http://microformats.org/wiki/file-format-examples
  $options['attributes']['type'] = $file->filemime . '; length=' . $file->filesize;

  // Use the description as the link text if available.
  if (empty($file->description)) {
    $filename_parts = explode('.', $file->filename);
    $extension = array_pop($filename_parts);
    $filename = implode('.', $filename_parts) . '<small>.' . $extension . '</small>';
    $link_text = $filename;
    $options['html'] = TRUE;
  }
  else {
    $link_text = $file->description;
    $options['attributes']['title'] = check_plain($file->filename);
  }

  return '<span class="file">' . $icon . ' ' . l($link_text, $url, $options) . '</span>';
}
