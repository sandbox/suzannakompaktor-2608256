<?php
/**
 * @file
 * overlay.vars.php
 */

/**
 * Implements hook_preprocess_overlay().
 */
function caffelatte_preprocess_overlay(&$variables) {
  $variables['content'] = drupal_render($variables['page']['content']);
}
