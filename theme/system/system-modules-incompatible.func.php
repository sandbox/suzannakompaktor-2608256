<?php
/**
 * @file
 * System-modules-incompatible.func.php.func.php.
 */

/**
 * Overrides theme_system_modules_incompatible().
 */
function caffelatte_system_modules_incompatible($variables) {
  return '<div class="well well-danger incompatible">' . $variables['message'] . '</div>';
}
