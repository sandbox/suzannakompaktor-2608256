<?php
/**
 * @file
 * links.func.php
 */

/**
 * Implements theme_links().
 */
function caffelatte_links__locale_block(&$variables) {
  global $language;

  $vars = array();
  $links = $variables['links'];
  $default = $links[$language->language];

  $vars['label'] = theme('icon', array('icon' => 'globe', 'attributes' => array('class' => array('text-muted')))) . ' ' . $default['title'];
  unset($links[$language->language]);

  $vars['links'] = $links;

  $vars['attributes'] = array('class' => array('language-dropdown'));

  return theme('bootstrap_dropdown', $vars);

}
