<?php
/**
 * @file
 * System-themes-page.func.php.func.php.
 */

/**
 * Overrides theme_system_themes_page().
 */
function caffelatte_system_themes_page($variables) {
  $theme_groups = $variables['theme_groups'];

  $output = '<div id="system-themes-page" class="clearfix">';

  foreach ($variables['theme_group_titles'] as $state => $title) {
    if (!count($theme_groups[$state])) {
      // Skip this group of themes if no theme is there.
      continue;
    }
    // Start new theme group.
    $output .= '<div class="system-themes-list system-themes-list-' . $state . ' clearfix"><h2>' . $title . '</h2>';

    foreach ($theme_groups[$state] as $theme) {
      // Theme the screenshot.
      $classes = array('screenshot', 'img-thumbnail');
      if ($state == 'enabled') {
        $classes[] = 'pull-left';
        $classes[] = 'mr-md';
      }
      if (!$theme->is_default) {
        $classes[] = 'mb-md';
      }
      $screenshot = $theme->screenshot ? theme('image', array_merge($theme->screenshot, array('attributes' => array('class' => $classes)))) : '<div class="no-screenshot ' . implode(' ', $classes) . '">' . t('no screenshot') . '</div>';

      // Localize the theme description.
      $description = t($theme->info['description']);

      // Style theme info.
      $notes = count($theme->notes) ? ' (' . implode(', ', $theme->notes) . ')' : '';

      $theme->classes[] = 'caffelatte-theme-selector';
      $theme->classes[] = 'clearfix';
      if ($state == 'disabled') {
        $theme->classes[] = 'col-sm-6';
        $theme->classes[] = 'col-md-4';
      }
      if ($theme->is_default) {
        $theme->classes[] = 'well';
      }
      $output .= '<div class="' . implode(' ', $theme->classes) . '">' . $screenshot . '<div class="theme-info">
      <h3 class="mt-unset' . ($state == 'disabled' ? ' h4' : '') . '">' . $theme->info['name'] . ' ' . (isset($theme->info['version']) ? $theme->info['version'] : '') . $notes . '</h3>
      <div class="theme-description"><p>' . $description . '</p></div>';

      // Make sure to provide feedback on compatibility.
      if (!empty($theme->incompatible_core)) {
        $output .= '<div class="well well-danger incompatible">' . t('This version is not compatible with Drupal !core_version and should be replaced.', array('!core_version' => DRUPAL_CORE_COMPATIBILITY)) . '</div>';
      }
      elseif (!empty($theme->incompatible_php)) {
        if (substr_count($theme->info['php'], '.') < 2) {
          $theme->info['php'] .= '.*';
        }
        $output .= '<div class="well well-danger incompatible">' . t('This theme requires PHP version @php_required and is incompatible with PHP version !php_version.', array('@php_required' => $theme->info['php'], '!php_version' => phpversion())) . '</div>';
      }
      else {
        $output .= theme('links', array(
          'links' => $theme->operations,
          'attributes' => array(
            'class' => array('operations', 'clearfix', 'list-separeted'),
          ),
        ));
      }
      $output .= '</div></div>';
    }
    $output .= '</div>';
  }
  $output .= '</div>';

  $output .= '<hr />';

  return $output;
}
