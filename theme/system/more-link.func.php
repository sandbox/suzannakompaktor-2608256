<?php
/**
 * @file
 * more-link.func.php
 */

/**
 * Overrides theme_more_link().
 */
function caffelatte_more_link($variables) {
  return '<hr /><div class="view-more">' . l(t('More'), $variables['url'], array('attributes' => array('title' => $variables['title']))) . '</div>';
}
