<?php
/**
 * @file
 * node.vars.php
 */

/**
 * Implements hook_preprocess_node().
 */
function caffelatte_preprocess_node(&$variables) {
  $variables['content']['links']['#attributes']['class'][] = 'view-more';
}
