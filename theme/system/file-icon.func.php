<?php
/**
 * @file
 * file-icon.func.php
 */

/**
 * Overrides theme_file_icon().
 */
function caffelatte_file_icon($variables) {
  $file = $variables['file'];
  $attributes = isset($variables['attributes']) ? $variables['attributes'] : array();
  $styles = isset($variables['styles']) ? $variables['styles'] : array();

  $icon = _caffelatte_get_file_icon($file->filemime);

  if (isset($attributes['class'])) {
    $attributes['class'][] = 'text-muted';
  }
  else {
    $attributes['class'] = array('text-muted');
  }

  return theme('icon_fw', array(
    'icon' => $icon,
    'attributes' => $attributes,
    'styles' => $styles,
  ));
}
