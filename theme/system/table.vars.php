<?php
/**
 * @file
 * table.vars.php
 */

/**
 * Implements hook_preprocess_table().
 *
 * @see template_preprocess_table()
 */
function caffelatte_preprocess_table(&$variables) {
  drupal_add_js(array(
  'caffelatte' => array(
    'customCheckbox' => theme_get_setting('caffelatte_custom_checkbox'),
  ),
  ), 'setting');
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/select-all.js', array('scope' => 'footer', 'weight' => 20));
  $variables['attributes']['class'][] = 'table';
}
