<?php
/**
 * @file
 * container.vars.php
 */

/**
 * Implements hook_preprocess_container().
 */
function caffelatte_preprocess_container(&$variables) {
  $element = $variables['element'];

  // Add form-actions js for actions buttons and add custom classes.
  if (isset($element['#type'])) {
    $is_action = $element['#type'] == 'actions' ||
        (isset($element['#array_parents']) &&
          array_search('actions', $element['#array_parents']) === 0
        );
        if ($is_action) {
          drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/form-actions.js', array('scope' => 'footer', 'weight' => 20));
          $element['#attributes']['class'][] = 'text-right';
        }
  }

  // Ensure #attributes is set.
  $element += array('#attributes' => array());

  // Special handling for form elements.
  if (isset($element['#array_parents'])) {
    // Assign an html ID.
    if (!isset($element['#attributes']['id'])) {
      $element['#attributes']['id'] = $element['#id'];
    }
    // Add the 'form-wrapper' class.
    $element['#attributes']['class'][] = 'form-wrapper';
    // Add 'form-group' Bootstrap class.
    $element['#attributes']['class'][] = 'form-group';
  }

  $variables['attributes'] = drupal_attributes($element['#attributes']);
  $variables['content'] = $element['#children'];
}
