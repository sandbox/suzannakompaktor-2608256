<?php
/**
 * @file
 * tablesort-indicator.func.php
 */

/**
 * Overrides theme_tablesort_indicator().
 */
function caffelatte_tablesort_indicator($variables) {
  if ($variables['style'] == "asc") {
    return theme('icon_fw', array(
      'icon' => 'sort-asc',
    ));
  }
  else {
    return theme('icon_fw', array(
      'icon' => 'sort-desc',
    ));
  }
}
