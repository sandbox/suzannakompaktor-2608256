<?php
/**
 * @file
 * breadcrumb.vars.php
 */

/**
 * Implements hook_preprocess_breadcrumb().
 */
function caffelatte_preprocess_breadcrumb(&$variables) {
  $breadcrumb = &$variables['breadcrumb'];

  if (!theme_get_setting('caffelatte_breadcrumb')) {
    $breadcrumb = '';
  }
  else {
    // Optionally get rid of the homepage link.
    $show_breadcrumb_home = theme_get_setting('caffelatte_breadcrumb_home');
    if (!$show_breadcrumb_home) {
      array_shift($breadcrumb);
    }

    $variables['styles'] = '';
    $variables['type'] = 'ul';
  }
}
