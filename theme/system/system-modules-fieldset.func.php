<?php
/**
 * @file
 * system-modules-fieldset.func.php
 */

/**
 * Overrides theme_system_modules_fieldset().
 */
function caffelatte_system_modules_fieldset($variables) {
  // Show more/Show less on description script.
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/system-modules.js');

  $form = $variables['form'];

  // Individual table headers.
  $rows = array();
  // Iterate through all the modules, which are
  // children of this fieldset.
  foreach (element_children($form) as $key) {
    // Stick it into $module for easier accessing.
    $module = $form[$key];
    $row = array();
    unset($module['enable']['#title']);
    // Display as switchfield if type is checkbox.
    if (!array_key_exists('#type', $module['enable'])) {
      // Elelemnt has error, display a danger icon.
      $module['enable']['#markup'] = _caffelatte_get_status_icon('danger');
    }
    elseif ($module['enable']['#type'] == 'checkbox') {
      $module['enable']['#theme'] = 'switchfield';
      $module['enable']['#wrapper_attributes'] = array('class' => array('switch-sm'));
      $module['enable']['#styles'] = array('warning-on-change');
      if (isset($module['enable']['#disabled'])) {
        if ($module['enable']['#disabled']) {
          $module['enable']['#styles'][] = 'disabled';
        }
      }
    }

    $row[] = array('class' => array('checkbox'), 'data' => drupal_render($module['enable']));
    $label = '<label';
    if (isset($module['enable']['#id'])) {
      $label .= ' for="' . $module['enable']['#id'] . '"';
    }
    $row[] = $label . '><strong>' . drupal_render($module['name']) . '</strong></label>';
    $row[] = drupal_render($module['version']);

    $description = '';
    // Put error message on the bottom instead of in description text.
    if (strrpos($module['description']['#markup'], '<div')) {
      $markup = $module['description']['#markup'];
      $desciption_message = substr($markup, strrpos($markup, '<div'));
      $module['description']['#markup'] = substr($markup, 0, strrpos($markup, '<div'));
      $description .= $desciption_message;
    }
    // Add the description, along with any modules it requires.
    $description .= '<span class="description">' . drupal_render($module['description']) . '</span>';
    if ($module['#requires']) {
      $description .= '<div class="admin-requirements hidden">' . t('Requires: !module-list', array('!module-list' => implode(', ', $module['#requires']))) . '</div>';
    }
    if ($module['#required_by']) {
      $description .= '<div class="admin-requirements hidden">' . t('Required by: !module-list', array('!module-list' => implode(', ', $module['#required_by']))) . '</div>';
    }

    $row[] = array('data' => $description, 'class' => array('description'));
    $links = array();
    // Display links (such as help or permissions) in their own columns.
    foreach (array('help', 'permissions', 'configure') as $link_type) {
      switch ($link_type) {
        case 'help':
              $i = 'question-circle';
          break;

        case 'permissions':
              $i = 'key';
          break;

        case 'configure':
              $i = 'cogs';
          break;

        default:
              $i = '';
          break;
      }
      $icon = theme('icon_fw', array('icon' => $i));

      if (array_key_exists('#title', $module['links'][$link_type])) {
        $module['links'][$link_type]['#title'] = $icon . '<span class="hidden-xs">' . $module['links'][$link_type]['#title'] . '</span>';
        $module['links'][$link_type]['#options']['html'] = TRUE;
        $module['links'][$link_type]['#options']['attributes']['class'] = array('alt-link');
      }

      $link = drupal_render($module['links'][$link_type]);
      if (!empty($link)) {
        $links[] = array('data' => $link, 'class' => array($link_type));
      }
    }
    $row[] = theme('item_list', array('items' => $links, 'attributes' => array('class' => array('list-separeted'))));
    $rows[] = $row;
  }

  return theme('table', array('header' => $form['#header'], 'rows' => $rows));
}
