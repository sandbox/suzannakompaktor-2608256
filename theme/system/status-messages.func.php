<?php
/**
 * @file
 * status-messages.func.php
 */

/**
 * Overrides theme_status_messages().
 */
function caffelatte_status_messages($variables) {
  $display = $variables['display'];
  $status_heading = array(
    'status' => t('Status message'),
    'error' => t('Error message'),
    'warning' => t('Warning message'),
    'info' => t('Informative message'),
  );
  $icons = array(
    'status' => 'check',
    'error' => 'times-circle',
    'warning' => 'exclamation-triangle',
    'info' => 'info-circle',
  );
  $status_class = array(
    'status' => 'success',
    'error' => 'danger',
    'warning' => 'warning',
        // Not supported, but in theory a module could send any type of message.
        // @see drupal_set_message()
        // @see theme_status_messages()
    'info' => 'info',
  );

  $output = '';

  foreach (drupal_get_messages($display) as $type => $messages) {
    $content = '';
    $styles = array();
    if (isset($status_class[$type])) {
      $styles[] = $status_class[$type];
    }
    else {
      $styles[] = 'default';
    }
    $attributes['class'] = array('messages');

    if (!empty($status_heading[$type])) {
      $content .= '<h4 class="element-invisible">' . $status_heading[$type] . "</h4>\n";
    }
    $content .= theme('icon', array(
      'icon' => $icons[$type],
      'styles' => array('lg'),
    ));

    if (count($messages) > 1) {
      $content .= " <ul>\n";
      foreach ($messages as $message) {
        $content .= '  <li>' . $message . "</li>\n";
      }
      $content .= " </ul>\n";
    }
    else {
      $content .= $messages[0];
    }

    $output .= theme('bootstrap_alert', array(
      'content' => $content,
      'styles' => $styles,
      'attributes' => $attributes,
    ));
  }

  return $output;
}
