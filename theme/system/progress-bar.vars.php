<?php
/**
 * @file
 * progress-bar.vars.php
 */

/**
 * Implements hook_preprocess_progress_bar().
 */
function caffelatte_preprocess_progress_bar(&$variables) {
  if (!isset($variables['progress_styles'])) {
    $variables['progress_styles'] = ' progress-striped active';
  }

  if (empty($variables['styles'])) {
    $variables['styles'] = 'progress-bar-info';
  }
}
