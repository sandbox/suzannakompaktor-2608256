<?php

/**
 * @file
 * Display overlay as Caffelatte modal.
 *
 * Available variables:
 * - $title: the (sanitized) title of the page.
 * - $page: The rendered page content.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_overlay()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<?php print render($disable_overlay); ?>

<div id="overlay" class="modal-block modal-full">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title"><?php print $title; ?></h2>
			<div class="panel-actions">
				<a class="close overlay-close" href="#">
					<span aria-hidden="true">×</span>
					<span class="sr-only"><?php print t('Close overlay'); ?></span>
				</a>
			</div>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					<?php if ($tabs): ?>
					<div class="tabs">
					<h2 class="element-invisible"><?php print t('Primary tabs'); ?></h2>
					<ul class="nav-tabs"><?php print render($tabs); ?></ul>
					</div>
					<?php endif; ?>
					<?php print $content; ?>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<a class="btn btn-default overlay-close" href="#"><?php print t('Close overlay'); ?></a>
				</div>
			</div>
		</footer>
	</section>
</div>
