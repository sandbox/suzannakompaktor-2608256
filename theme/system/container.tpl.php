<?php
/**
 * @file
 * container.tpl.php
 *
 * Variables
 * - $content: The content os container.
 * - $attributes: HTML attributes.
 */

?>
<div<?php print $attributes ?>><?php print $content ?></div>
