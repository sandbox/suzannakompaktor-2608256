<?php
/**
 * @file
 * field.vars.php
 */

/**
 * Implements hook_preprocess_field().
 *
 * @see template_preprocess_field()
 */
function caffelatte_preprocess_field(&$variables) {
  switch ($variables['element']['#field_name']) {
    case 'field_address':
          $variables['label'] = theme('icon_fw', array('icon' => 'map-marker')) . $variables['label'];
      break;

    case 'field_email':
          $variables['label'] = theme('icon_fw', array('icon' => 'envelope')) . $variables['label'];
      break;

    case 'field_web':
          $variables['label'] = theme('icon_fw', array('icon' => 'globe')) . $variables['label'];
      break;

    case 'field_phone':
          $variables['label'] = theme('icon_fw', array('icon' => 'phone')) . $variables['label'];
      break;
  }
}
