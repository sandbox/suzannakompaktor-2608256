  <?php
/**
 * @file
 * views-view-table.vars.php
 */

/**
 * Implements hook_preprocess_views_view_table().
 */
function caffelatte_preprocess_views_view_table(&$vars) {
  $vars['classes_array'][] = 'table';
}
