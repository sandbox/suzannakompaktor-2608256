<?php
/**
 * @file
 * fieldset.vars.php
 */

/**
 * Implements hook_preprocess_fieldset().
 */
function caffelatte_preprocess_fieldset(&$variables) {

  $element = $variables['element'];
  element_set_attributes($element, array('id'));
  _form_set_class($element, array('form-wrapper', 'mb-md', 'clearfix'));

  if (array_key_exists('#title', $element)) {
    $variables['legend_classes'] = isset($variables['#legend_classes']) ? $variables['#legend_classes'] : (theme_get_setting('caffelatte_fieldset_legend_classes') ? explode(' ', theme_get_setting('caffelatte_fieldset_legend_classes')) : array('col-xs-12'));
    $variables['fieldset_wrapper_classes'] = isset($variables['#fieldset_wrapper_classes']) ? $variables['#fieldset_wrapper_classes'] : (theme_get_setting('caffelatte_fieldset_wrapper_classes') ? explode(' ', theme_get_setting('caffelatte_fieldset_wrapper_classes')) : array('col-xs-12'));
  }
  else {
    $variables['fieldset_wrapper_classes'][] = 'col-xs-12';
    $variables['legend_classes'][] = 'col-xs-12';
  }

  $variables['attributes'] = drupal_attributes($element['#attributes']);
  $variables['legend_classes'] = ' ' . implode(' ', $variables['legend_classes']);
  $variables['fieldset_wrapper_classes'] = ' ' . implode(' ', $variables['fieldset_wrapper_classes']);
  $variables['is_collapsible'] = isset($element['#collapsible']) ? $element['#collapsible'] : FALSE;
  $variables['title'] = array_key_exists('#title', $element) ? $element['#title'] : '';
  if (isset($element['#description'])) {
    $variables['description'] = $element['#description'];
  }
  $variables['content'] = $element['#children'];
  if (isset($element['#value'])) {
    $variables['content'] .= $element['#value'];
  }
}
