<?php
/**
 * @file
 * select.vars.php
 */

/**
 * Implements hook_preprocess_select().
 */
function caffelatte_preprocess_select(&$variables) {
  $element = $variables['element'];
  if (!isset($element['#bootstrap_tagsinput'])) {
    $element['#bootstrap_tagsinput'] = FALSE;
  }

  if (isset($variables['element']['#required']) && $variables['element']['#required']) {
    element_set_attributes($variables['element'], array('required'));
  }

  if ($element['#multiple']) {
    if (theme_get_setting('caffelatte_allow_select2_multiselect')) {
      $element['#select2'] = TRUE;
    }
  }
  elseif (!isset($element['#select2']) && (theme_get_setting('caffelatte_allow_select2_over') && count($element['#options']) >= theme_get_setting('caffelatte_allow_select2_over'))) {
    $element['#select2'] = TRUE;
  }
  if (!isset($element['#select2'])) {
    $element['#select2'] = FALSE;
  }

  if ($element['#select2']) {
    caffelatte_load_plugin_select2();
    $variables['element']['#attributes']['class'][] = 'custom-select';
  }
  elseif ($element['#bootstrap_tagsinput']) {
    caffelatte_load_plugin_tagsinput();
    $variables['element']['#attributes']['data-role'] = 'tagsinput';
    $variables['element']['#attributes']['data-tag-class'] = 'label label-info';
  }
}
