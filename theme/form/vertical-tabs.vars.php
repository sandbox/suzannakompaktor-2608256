<?php
/**
 * @file
 * vertical-tabs.vars.php
 */

/**
 * Implements hook_preprocess_vertical_tabs().
 */
function caffelatte_preprocess_vertical_tabs(&$variables) {
  drupal_add_css(drupal_get_path('theme', 'caffelatte') . '/css/vertical-tabs.css');
}
