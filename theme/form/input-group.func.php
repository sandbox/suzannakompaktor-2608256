<?php
/**
 * @file
 * input-group.func.php
 */

/**
 * Implements theme_input_group().
 */
function caffelatte_input_group($variables) {
  $output = '';
  $input = $variables['input'];
  $prefix = $variables['prefix'];
  $suffix = $variables['suffix'];

  switch ($variables['type']) {
    case 'btn':
      $output .= theme('bootstrap_input_group_btn', array(
        'prefix' => $prefix,
        'suffix' => $suffix,
        'input' => $input,
      ));
      break;

    case 'icon':
      $output .= theme('bootstrap_input_group_icon', array(
        'prefix' => $prefix,
        'suffix' => $suffix,
        'input' => $input,
      ));
      break;

    default:
      $output .= theme('bootstrap_input_group', array(
        'prefix' => $prefix,
        'suffix' => $suffix,
        'input' => $input,
      ));
      break;
  }

  $output .= $variables['content'];

  return $output;
}
