<?php
/**
 * @file
 * textfield.func.php
 */

/**
 * Overrides theme_textfield().
 */
function caffelatte_textfield($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'text';
  element_set_attributes($element, array(
    'id',
    'name',
    'value',
    'size',
    'maxlength',
  ));
  _form_set_class($element, array('form-text'));

  if (isset($element['#bootstrap_maxlength'])) {
    if ($element['#bootstrap_maxlength'] && isset($element['#maxlength'])) {
      caffelatte_load_plugin_maxlength();
      $element['#attributes']['data-plugin-maxlength'] = TRUE;
    }
  }

  $extra = '';
  $output = '';
  if ($element['#autocomplete_path'] && drupal_valid_path($element['#autocomplete_path'])) {
    caffelatte_load_plugin_autocomplete();
    $element['#attributes']['class'][] = 'form-autocomplete';

    $attributes = array();
    $attributes['type'] = 'hidden';
    $attributes['id'] = $element['#attributes']['id'] . '-autocomplete';
    $attributes['value'] = url($element['#autocomplete_path'], array('absolute' => TRUE));
    $attributes['disabled'] = 'disabled';
    $attributes['class'][] = 'autocomplete';

    $extra = theme('input_tag', array('element' => array('#attributes' => $attributes)));
    $input = theme('input_tag', array('element' => $element));

    $output = theme('bootstrap_input_group_icon', array(
      'input' => $input,
      'suffix' => theme('icon_fw', array('icon' => theme_get_setting('caffelatte_autocomplete_icon'))),
    ));

    $output .= $extra;
    return '<div class="autocomplete-group">' . $output . '</div>';
  }

  if (array_key_exists('#parents', $element) && array_search('email', $element['#parents'], TRUE)) {
    $output = theme('emailfield', array('element' => $element));
  }
  else {
    $element['#attributes']['type'] = 'text';
    $output = theme('input_tag', array('element' => $element));
  }

  return $output . $extra;
}
