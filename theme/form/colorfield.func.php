<?php
/**
 * @file
 * colorfield.func.php
 */

/**
 * Overrides theme_colorfield().
 */
function caffelatte_colorfield($variables) {
  $element = $variables['element'];

  if (isset($element['#default_value'])) {
    $value = $element['#default_value'];
  }
  else {
    switch ($element['#color_format']) {
      case 'hex':
        $value = '#ffffff';
        break;

      case 'rgb':
        $value = rgb(255, 255, 255);
        break;

      case 'rgba':
        $value = rgba(255, 255, 255, 1);
        break;

      default:
        $value = '#ffffff';
        break;
    }
  }

  $element['#default_value'] = $value;

  return theme('input_tag', array('element' => $element));
}
