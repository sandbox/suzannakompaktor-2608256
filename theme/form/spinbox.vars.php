<?php
/**
 * @file
 * spinbox.vars.php
 */

/**
 * Implements hook_preprocess_spinbox().
 */
function caffelatte_preprocess_spinbox(&$variables) {
  if (isset($variables['attributes']['class'])) {
    $variables['classes'] = $variables['attributes']['class'];
    unset($variables['attributes']['class']);
  }
}

/**
 * Implements hook_process_spinbox().
 */
function caffelatte_process_spinbox(&$variables) {
  $variables['attributes'] = drupal_attributes($variables['attributes']);

  if (isset($variables['classes'])) {
    if (!empty($variables['classes'])) {
      $variables['classes'] = ' ' . implode(' ', $variables['classes']);
    }
  }
}
