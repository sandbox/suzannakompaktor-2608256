<?php
/**
 * @file
 * telfield.func.php
 */

/**
 * Overrides theme_telfield().
 */
function caffelatte_telfield($variables) {
  $element = $variables['element'];
  return theme('input_tag', array('element' => $element));
}
