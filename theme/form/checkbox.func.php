<?php
/**
 * @file
 * checkbox.func.php
 */

/**
 * Overrides theme_checkbox().
 */
function caffelatte_checkbox($variables) {
  if (theme_get_setting('caffelatte_custom_checkbox')) {
    return theme('custom_checkbox', $variables);
  }
  return theme_checkbox($variables);
}
