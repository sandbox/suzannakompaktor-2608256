<?php
/**
 * @file
 * urlfield.func.php
 */

/**
 * Overrides theme_urlfield().
 */
function caffelatte_urlfield($variables) {
  $element = $variables['element'];
  return theme('input_tag', array('element' => $element));
}
