<?php
/**
 * @file
 * radio.func.php
 */

/**
 * Overrides theme_radio().
 */
function caffelatte_radio($variables) {
  if (theme_get_setting('caffelatte_custom_radio')) {
    return theme('custom_radio', $variables);
  }
  return theme_radio($variables);
}
