<?php
/**
 * @file
 * telfield.vars.php
 */

/**
 * Implements hook_preprocess_telfield().
 *
 * @see template_preprocess_telfield()
 */
function caffelatte_preprocess_telfield(&$variables) {
  $variables['element']['#attributes']['type'] = 'tel';

  element_set_attributes($variables['element'], array(
      'id',
      'name',
      'value',
      'size',
      'maxlength',
  ));
  _form_set_class($variables['element'], array('form-tel'));
}
