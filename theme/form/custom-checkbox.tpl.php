<?php
/**
 * @file
 * custom-checkbox.tpl.php
 *
 * Variables
 * - $styles: Bootstrap styles (checkbox-default, checkbox-primary, checkbox-info, checkbox-success, checkbox-warning, checkbox-danger, checkbox-inverse checkbox-inline).
 * - $label
 * - $attributes: HTML attributes for input.
 */

?>

<div class="checkbox-custom <?php print $styles ?>">
  <input<?php print $attributes ?> />
  <?php print $label ?>
</div>
