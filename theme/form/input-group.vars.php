<?php
/**
 * @file
 * input-group.vars.php
 */

/**
 * Implements hook_preprocess_input_group().
 */
function caffelatte_preprocess_input_group(&$variables) {
  $element = &$variables['element'];
  $type = isset($element['#input_group_type']) ? $element['#input_group_type'] : 'addon';
  $suffix = '';
  $prefix = '';

  if (isset($element['actions'])) {
    $type = 'btn';
    if ($element['actions']['#weight'] > 0) {
      $suffix = drupal_render_children($element['actions']);
    }
    else {
      $prefix = drupal_render_children($element['actions']);
    }
    unset($element['actions']);
  }

  if (isset($element['submit'])) {
    $type = 'btn';
    $element['submit']['#printed'] = FALSE;
    if ($element['submit']['#weight'] > 0) {
      $suffix = drupal_render($element['submit']);
    }
    else {
      $prefix = drupal_render($element['submit']);
    }
  }

  $variables['prefix'] = $prefix;
  $variables['suffix'] = $suffix;
  $variables['input'] = '';
  $allowed_types = _caffelatte_input_group_allowed_types();
  foreach (element_children($element) as $key) {
    if (in_array($element[$key]['#type'], $allowed_types)) {
      $element[$key]['#printed'] = FALSE;
      $element[$key]['#theme_wrappers'] = array();
      $variables['input'] .= drupal_render($element[$key]);
    }
  }

  $variables['type'] = $type;

  $variables['content'] = drupal_render_children($element);
}
