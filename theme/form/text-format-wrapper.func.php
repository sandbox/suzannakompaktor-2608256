<?php
/**
 * @file
 * text-format-wrapper.func.php
 */

/**
 * Overrides theme_text_format_wrapper().
 */
function caffelatte_text_format_wrapper($variables) {
  $element = $variables['element'];
  $output = '<div class="text-format-wrapper">';
  $output .= $element['#children'];
  if (!empty($element['#description'])) {
    $output .= '<div class="description">' . $element['#description'] . '</div>';
  }
  $output .= "</div>\n";

  return $output;
}
