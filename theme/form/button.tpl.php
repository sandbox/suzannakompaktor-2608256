<?php
/**
 * @file
 * button.tpl.php
 *
 * Variables
 * - $styles: Bootstrap styles (btn-default, btn-primary, btninfo, btn-success, btn-warning, btn-danger, btn-lg, btn-sm, btn-xs, btn-block).
 * - $classes: Additional classes
 * - $icon.
 * - $value
 * - $value_attributes: attributes for value wrapper item.
 */

?>
<button class="btn <?php print $styles; ?><?php print $classes; ?>"<?php print $attributes ?>>
  <?php print $icon ?><span<?php print $value_attributes ?>><?php print $value ?></span>
</button>
