<?php
/**
 * @file
 * file-managed-file.func.php
 */

/**
 * Implements theme_file_managed_file().
 */
function caffelatte_file_managed_file($variables) {
  global $base_url;

  $render_buttons = TRUE;
  $element = $variables['element'];

  $attributes = array();

  if (!empty($element['#attributes']['class'])) {
    $attributes['class'] = (array) $element['#attributes']['class'];
  }
  $attributes['class'][] = 'form-managed-file';

  if (theme_get_setting('caffelatte_allow_fileinput_managed_file')) {
    $render_buttons = FALSE;

    caffelatte_load_plugin_fileinput();

    $allowed_extensions = isset($element['#upload_validators']['file_validate_extensions']) ? explode(" ", $element['#upload_validators']['file_validate_extensions'][0]) : array(
      'jpg',
      'jpeg',
      'gif',
      'png',
      'txt',
      'doc',
      'xls',
      'pdf',
      'ppt',
      'pps',
      'odt',
      'ods',
      'odp',
    );

    $ajax_path = $element['upload_button']['#ajax']['path'];
    $ajax_path_parts = explode('/', $ajax_path);
    $element_options = array(
      'maxFileCount' => 1,
      'autoReplace' => TRUE,
      'allowedFileExtensions' => $allowed_extensions,
      'uploadUrl' => $base_url . '/' . $ajax_path,
      'uploadExtraData' => array(
        'form_build_id' => array_pop($ajax_path_parts),
      ),
    );

    if ($element['fid']['#value'] > 0) {
      $file = file_load($element['fid']['#value']);
      $preview = theme('fileinput_file_preview', array("file" => $file));

      $element_options += array(
        'initialPreview' => array($preview),
        'initialCaption' => $file->filename,
        'initialPreviewConfig' => array(
          array(
            'caption' => $file->filename,
            'key' => $file->fid,
            'url' => $base_url . '/' . $element['remove_button']['#ajax']['path'],
          ),
        ),
      );
    }

    drupal_add_js(array(
        'caffelatte' => array(
          'file' => array('elements' => array('#' . $element['#id'] => $element_options)),
        ),
      ), 'setting');

    drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/managed-file.js', array('scope' => 'footer', 'weight' => 21));

    $element['upload']['#access'] = TRUE;
    $element['upload_button']['#access'] = TRUE;
    if (isset($element['filename'])) {
      $element['filename']['#access'] = FALSE;
      $element['remove_button']['#access'] = TRUE;
    }
  }

  $element['upload_button']['#attributes']['class'][] = "file-managed-file-upload-button";
  if (isset($element['remove_button'])) {
    $element['remove_button']['#attributes']['class'][] = "file-managed-file-remove-button";
  }

  $element['upload_button']['#ajax']['progress'] = array('type' => 'none');

  $output = '';
  $output .= '<div' . drupal_attributes($attributes) . '>';
  if ($render_buttons) {
    $output .= theme('bootstrap_input_group_btn', array(
      'suffix' => drupal_render($element['upload_button']),
      'input' => drupal_render($element['upload']),
    ));
  }
  $output .= drupal_render_children($element);
  $output .= '</div>';
  return $output;
}
