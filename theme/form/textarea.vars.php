<?php
/**
 * @file
 * textarea.vars.php
 */

/**
 * Implements hook_preprocess_textarea().
 *
 * @see template_preprocess_textarea()
 */
function caffelatte_preprocess_textarea(&$variables) {
  $element = $variables['element'];

  if ($element['#autosize']) {
    caffelatte_load_plugin_autosize();
    $element['#attributes']['data-plugin-autosize'] = TRUE;
  }

  if ($element['#bootstrap_maxlength'] && isset($element['#maxlength'])) {
    caffelatte_load_plugin_maxlength();
    $element['#attributes']['data-plugin-maxlength'] = TRUE;
  }

  element_set_attributes($element, array('id', 'name', 'cols', 'rows'));
  if (isset($element['#required']) && $element['#required']) {
    element_set_attributes($variables['element'], array('required'));
  }
  _form_set_class($element, array('form-textarea', 'form-control'));

  $wrapper_attributes = array(
    'class' => array('form-textarea-wrapper'),
  );

  // Add resizable behavior.
  if (!empty($element['#resizable'])) {
    drupal_add_library('system', 'drupal.textarea');
    $wrapper_attributes['class'][] = 'resizable';
  }

  $variables['wrapper_attributes'] = drupal_attributes($wrapper_attributes);
  $variables['attributes'] = drupal_attributes($element['#attributes']);
  $variables['value'] = check_plain($element['#value']);
}
