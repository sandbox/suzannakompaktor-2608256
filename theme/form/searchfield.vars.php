<?php
/**
 * @file
 * searchfield.vars.php
 */

/**
 * Implements hook_preprocess_searchfield().
 *
 * @see template_preprocess_searchfield()
 */
function caffelatte_preprocess_searchfield(&$variables) {
  $variables['element']['#attributes']['type'] = 'search';

  element_set_attributes($variables['element'], array(
      'id',
      'name',
      'value',
      'size',
      'maxlength',
  ));
  _form_set_class($variables['element'], array('form-search'));
}
