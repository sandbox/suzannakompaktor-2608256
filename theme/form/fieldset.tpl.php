<?php
/**
 * @file
 * fieldset.tpl.php
 *
 * Variables
 * - $title: title of fieldset.
 * - $description: description of fieldset
 * - $content: content of fieldset.
 * - (bool) $is_collapsible: fieldset is collapsible or not.
 * - $attributes: HTML attributes for <fieldset>.
 */

?>

<fieldset<?php print $attributes ?>>
  <?php if (!empty($title) || !empty($description)): ?>
  <legend class="legend<?php print $legend_classes ?>">
    <div class="row mr-unset">
    <?php if (!empty($title)): ?>
      <strong class="fieldset-legend">
        <?php if($is_collapsible): ?>
          <?php print theme('icon', array('icon' => 'custom-icon', 'attributes' => array('class' => array('custom-caret')))); ?>
        <?php endif; ?>
        <?php print $title ?>
      </strong>
    <?php endif; ?>
    <?php if (isset($description)): ?>
      <span class="summary"><?php print $description ?></span>
    <?php endif; ?>
    </div>
  </legend>
  <?php endif; ?>
  
  <div class="fieldset-wrapper<?php print $fieldset_wrapper_classes ?>">
    <div class="row">
    <?php print $content ?>
    </div>
  </div>
</fieldset>
