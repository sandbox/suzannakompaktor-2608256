<?php
/**
 * @file
 * timefield.vars.php
 */

/**
 * Implements hook_preprocess_timefield().
 *
 * @see template_preprocess_timefield()
 */
function caffelatte_preprocess_timefield(&$variables) {
  caffelatte_load_plugin_timepicker();

  $variables['element']['#attributes']['data-plugin-timepicker'] = TRUE;
  $variables['element']['#attributes']['type'] = 'text';

  element_set_attributes($variables['element'], array(
      'id',
      'name',
      'value',
      'size',
      'maxlength',
  ));
  _form_set_class($variables['element'], array('form-color'));

  if (isset($variables['element']['#required']) && $variables['element']['#required']) {
    element_set_attributes($variables['element'], array('required'));
  }
  $variables['element']['#attributes']['class'][] = 'form-control';
}
