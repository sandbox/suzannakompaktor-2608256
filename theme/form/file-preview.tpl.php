<?php
/**
 * @file
 * file-preview.tpl.php
 */

?>
<div class="col-xs-12">
	<div class="row">
		<div class="<?php print ($additional) ? 'col-sm-6' : 'col-xs-12' ?>">
			<div class="file-preview-thumbnails">
				<div class="file-preview-frame">
					<?php print $preview; ?>
					<div class="file-thumbnail-footer">
						<div class="file-footer-caption"><?php print $caption; ?></div>
						<?php if($buttons): ?>
						<div class="file-actions">
							<div class="file-footer-buttons"><?php print $buttons; ?></div>
						</div>
					  <?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<?php if($additional): ?>
			<div class="col-sm-6"><?php print $additional; ?></div>
		<?php endif; ?>
	</div>
</div>
