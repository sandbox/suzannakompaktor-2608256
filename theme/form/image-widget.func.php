<?php
/**
 * @file
 * image-widget.func.php
 */

/**
 * Implements theme_image_widget().
 */
function caffelatte_image_widget($variables) {
  global $base_url;

  $render_buttons = TRUE;
  $element = $variables['element'];

  $element['upload_button']['#attributes']['class'][] = "file-managed-file-upload-button";
  if (isset($element['remove_button'])) {
    $element['remove_button']['#attributes']['class'][] = "file-managed-file-remove-button";
  }

  $attributes = array();

  if (!empty($element['#attributes']['class'])) {
    $attributes['class'] = (array) $element['#attributes']['class'];
  }
  $attributes['class'][] = 'image-widget';
  $attributes['class'][] = 'form-managed-file';

  $element['upload_button']['#ajax']['progress'] = array('type' => 'none');

  $output = '';
  $output .= '<div' . drupal_attributes($attributes) . '>';
  // Add preview.
  if ($element['fid']['#value'] != 0) {
    $element['filename']['#markup'] = theme('file_link', array(
      'file' => $element['#file'],
      '#has_icon' => FALSE,
      '#attributes' => array('data-plugin-lightbox' => TRUE, 'data-plugin-lightbox-type' => 'image'),
    ));
    $vars = array(
      'preview' => drupal_render($element['preview']),
      'caption' => drupal_render($element['filename']),
    );
    if (isset($element['title']) || isset($element['alt'])) {
      $vars['additional'] = '<div class="row">';
      if (isset($element['alt'])) {
        $vars['additional'] .= '<div class="col-xl-6">' . drupal_render($element['alt']) . '</div>';
      }
      if (isset($element['title'])) {
        $vars['additional'] .= '<div class="col-xl-6">' . drupal_render($element['title']) . '</div>';
      }
      $vars['additional'] .= '</div>';
    }

    $output .= theme('file_preview', $vars);
  }
  else {
    if (theme_get_setting('caffelatte_allow_fileinput_image_widget')) {
      $render_buttons = FALSE;

      caffelatte_load_plugin_fileinput();

      $allowed_extensions = isset($element['#upload_validators']['file_validate_extensions']) ? explode(" ", $element['#upload_validators']['file_validate_extensions'][0]) : array(
        'jpg',
        'jpeg',
        'gif',
        'png',
        'txt',
        'doc',
        'xls',
        'pdf',
        'ppt',
        'pps',
        'odt',
        'ods',
        'odp',
      );

      $ajax_path = $element['upload_button']['#ajax']['path'];
      $ajax_path_parts = explode('/', $ajax_path);
      $element_options = array(
        'maxFileCount' => 1,
        'autoReplace' => TRUE,
        'allowedFileExtensions' => $allowed_extensions,
        'uploadUrl' => $base_url . '/' . $ajax_path,
        'uploadExtraData' => array(
          'form_build_id' => array_pop($ajax_path_parts),
        ),
      );

      drupal_add_js(array(
        'caffelatte' => array(
          'file' => array('elements' => array('#' . $element['#id'] => $element_options)),
        ),
      ), 'setting');

      drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/managed-file.js', array('scope' => 'footer', 'weight' => 21));

      $element['upload']['#access'] = TRUE;
      $element['upload_button']['#access'] = TRUE;
      if (isset($element['filename'])) {
        $element['filename']['#access'] = FALSE;
        $element['remove_button']['#access'] = TRUE;
      }
    }
    if ($render_buttons) {
      $output .= theme('bootstrap_input_group_btn', array(
        'suffix' => drupal_render($element['upload_button']),
        'input' => drupal_render($element['upload']),
      ));
    }
  }
  $output .= drupal_render_children($element);
  $output .= '</div>';
  return $output;
}
