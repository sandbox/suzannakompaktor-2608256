<?php
/**
 * @file
 * input-tag.tpl.php
 *
 * Markup input tag.
 *
 * Variables
 * - $type: the type <input> element to display.
 * - $attributes: HTML attributes.
 */

?>
<input type="<?php print $type ?>"<?php print $attributes ?> />
