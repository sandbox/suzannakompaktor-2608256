<?php
/**
 * @file
 * form.vars.php
 */

/**
 * Implements hook_preprocess_form().
 *
 * @see template_preprocess_form()
 */
function caffelatte_preprocess_form(&$variables) {
  $element = &$variables['element'];

  if (module_exists('clientside_validation')) {
    drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/clientside_validation.js');
  }

  $variables['has_client_validation'] = FALSE;

  if ($element['#client_validation']) {
    caffelatte_load_plugin_validate();
    $element['#attributes']['class'][] = 'client-validation';
    $variables['has_client_validation'] = TRUE;
    $variables['error_id'] = str_replace('_', '-', $element['#form_id']) . '-error';
  }

  if (isset($element['#action'])) {
    $element['#attributes']['action'] = drupal_strip_dangerous_protocols($element['#action']);
  }
  element_set_attributes($element, array('method', 'id'));
  if (empty($element['#attributes']['accept-charset'])) {
    $element['#attributes']['accept-charset'] = "UTF-8";
  }

  $element['#attributes']['class'][] = 'clearfix';

  $variables['attributes'] = drupal_attributes($element['#attributes']);
  $variables['elements'] = $element['#children'];
}
