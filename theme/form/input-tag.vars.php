<?php
/**
 * @file
 * input-tag.vars.php
 */

/**
 * Implements hook_preprocess_input_tag().
 *
 * @see template_preprocess_input_tag()
 */
function caffelatte_preprocess_input_tag(&$variables) {
  if (!isset($variables['element']['#attributes']['class'])) {
    $variables['element']['#attributes']['class'] = array();
  }

  if (isset($variables['element']['#required']) && $variables['element']['#required']) {
    element_set_attributes($variables['element'], array('required'));
  }

  if (!in_array('form-control', $variables['element']['#attributes']['class'])) {
    $variables['element']['#attributes']['class'][] = 'form-control';
  }
  $variables['type'] = $variables['element']['#attributes']['type'];
  unset($variables['element']['#attributes']['type']);
}

/**
 * Implements hook_process_input_tag().
 */
function caffelatte_process_input_tag(&$variables) {
  $variables['attributes'] = drupal_attributes($variables['element']['#attributes']);
}
