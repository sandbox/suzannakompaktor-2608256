<?php
/**
 * @file
 * switchfield.tpl.php
 *
 * Markup for switchfield.
 *
 * Variables
 * - $switch_handle: switch-handle attributes (eg.: data-content-on, data-content-off).
 * - $styles: Bootstrap styles.
 * - $attributes: HTML attributes.
 */
?>
<div class="switch <?php print $styles; ?>">
  <input<?php print $attributes ?> />
  <div<?php print $switch_handle ?>>
    <span class="switcher"> </span>
  </div>
</div>
