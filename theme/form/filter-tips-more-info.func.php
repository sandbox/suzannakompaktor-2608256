<?php
/**
 * @file
 * filter-tips-more-info.func.php
 */

/**
 * Overrides theme_filter_tips_more_info().
 */
function caffelatte_filter_tips_more_info($variables) {
  $title = theme('icon_fw', array('icon' => 'question-circle')) . t('More information about text formats');
  $link = l($title, 'filter/tips', array(
    'attributes' => array(
      'target' => '_blank',
      'class' => array('text-muted', 'text-sm'),
    ),
    'html' => TRUE,
  ));
  return '<div class="col-xs-12"><p class="text-right">' . $link . '</p></div>';
}
