<?php
/**
 * @file
 * rangefield.vars.php
 */

/**
 * Implements hook_preprocess_rangefield().
 *
 * @see template_preprocess_rangefield()
 */
function caffelatte_preprocess_rangefield(&$variables) {
  $variables['element']['#attributes']['type'] = 'range';

  element_set_attributes($variables['element'], array(
      'id',
      'name',
      'value',
      'step',
      'min',
      'max',
  ));
  _form_set_class($variables['element'], array('form-range'));

  if (theme_get_setting('caffelatte_allow_slider')) {
    caffelatte_load_plugin_slider();
    $variables['element']['#attributes']['class'][] = 'hidden';
    $variables['element']['#attributes']['type'] = 'number';
  }
}
