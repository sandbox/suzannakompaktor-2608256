<?php
/**
 * @file
 * custom-checkbox.vars.php
 */

/**
 * Implements hook_preprocess_custom_checkbox().
 *
 * @see template_preprocess_custom_checkbox()
 */
function caffelatte_preprocess_custom_checkbox(&$variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'checkbox';
  element_set_attributes($element, array(
    'id',
    'name',
    '#return_value' => 'value',
  ));

  // Unchecked checkbox has #value of integer 0.
  if (!empty($element['#checked'])) {
    $element['#attributes']['checked'] = 'checked';
  }
  _form_set_class($element, array('form-checkbox'));

  $element['#styles'] = !empty($element['#styles']) ? $element['#styles'] : (array) theme_get_setting('caffelatte_checkbox_default_style');
  if (theme_get_setting('caffelatte_checkbox_inverse')) {
    $element['#styles'][] = 'inverse';
  }
  $variables['styles'] = _caffelatte_create_styles('checkbox', $element['#styles']);
  $label = '';
  if (isset($element['#title'])) {
    $label = theme('form_element_label', $variables);
  }
  $variables['label'] = !empty($label) ? $label : '<label for="' . $element['#id'] . '"></label>';
  $variables['attributes'] = drupal_attributes($element['#attributes']);
}
