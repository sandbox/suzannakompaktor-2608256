<?php
/**
 * @file
 * form-required-marker.func.php
 */

/**
 * Overrides theme_form_required_marker().
 */
function caffelatte_form_required_marker($variables) {
  // This is also used in the installer, pre-database setup.
  $t = get_t();
  $attributes = array(
    'class' => 'required',
    'title' => $t('This field is required.'),
    'aria-required' => "true",
  );
  return '<span' . drupal_attributes($attributes) . '>*</span>';
}
