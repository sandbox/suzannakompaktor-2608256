<?php
/**
 * @file
 * fileinput-file-preview.func.php
 */

/**
 * Implements theme_fileinput_file_preview().
 */
function caffelatte_fileinput_file_preview($variables) {

  $file = $variables['file'];
  $output = "";

  $mime = explode('/', check_plain($file->filemime));
  if ($mime[0] == 'image') {
    $output .= theme('image', array(
      'path' => $file->uri,
      'alt' => $file->filename,
      'attributes' => array(
        'class' => array('file-preview-image'),
      ),
    ));
  }
  else {
    $output .= '<div class="file-preview-other-frame"><div class="file-preview-other"><div class="file-icon-4x">' . theme('file_icon', array('file' => $file)) . '</div></div></div>';
  }

  return $output;
}
