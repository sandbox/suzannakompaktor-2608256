<?php
/**
 * @file
 * searchfield.func.php
 */

/**
 * Overrides theme_searchfield().
 */
function caffelatte_searchfield($variables) {
  $element = $variables['element'];
  return theme('input_tag', array('element' => $element));
}
