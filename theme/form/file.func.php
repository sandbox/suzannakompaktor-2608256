<?php
/**
 * @file
 * file.func.php
 */

/**
 * Overrides theme_file().
 */
function caffelatte_file($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'file';
  element_set_attributes($element, array('id', 'name', 'size'));
  _form_set_class($element, array('form-file'));

  return theme('input_tag', array('element' => $element));
}
