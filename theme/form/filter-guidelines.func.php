<?php
/**
 * @file
 * filter-guidelines.func.php
 */

/**
 * Overrides theme_filter_guidelines().
 */
function caffelatte_filter_guidelines($variables) {
  $format = $variables['format'];
  $attributes['class'][] = 'filter-guidelines-item';
  $attributes['class'][] = 'filter-guidelines-' . $format->format;
  $attributes['class'][] = 'mb-md';
  $attributes['class'][] = 'mt-sm';
  $attributes['class'][] = 'clear';
  $output = '<div' . drupal_attributes($attributes) . '>';
  $output .= '<h3>' . check_plain($format->name) . '</h3>';
  $output .= theme('filter_tips', array('tips' => _filter_tips($format->format, FALSE)));
  $output .= '</div>';
  return $output;
}
