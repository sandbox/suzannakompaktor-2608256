<?php
/**
 * @file
 * help-block.tpl.php
 *
 * Variables
 * - $description: The description/help text of form item.
 * - $attributes: HTML attributes.
 */

?>

<?php if(!empty($description)): ?>
  <span class="help-block"<?php print $attributes?>><?php print $description ?></span>
<?php endif; ?>
