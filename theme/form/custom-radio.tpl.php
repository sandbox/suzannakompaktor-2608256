<?php
/**
 * @file
 * custom-radio.tpl.php
 *
 * Variables
 * - $styles: Bootstrap styles (radio-default, radio-primary, radio-info, radio-success, radio-warning, radio-danger, radio-inline).
 * - $label
 * - $attributes: HTML attributes for input.
 */

?>

<div class="radio-custom <?php print $styles ?>">
  <input<?php print $attributes ?> />
  <?php print $label ?>
</div>
