<?php
/**
 * @file
 * file-widget.func.php
 */

/**
 * Implements theme_file_widget().
 */
function caffelatte_file_widget($variables) {
  global $base_url;

  $render_buttons = TRUE;
  $element = $variables['element'];

  $element['upload_button']['#attributes']['class'][] = "file-managed-file-upload-button";
  if (isset($element['remove_button'])) {
    $element['remove_button']['#attributes']['class'][] = "file-managed-file-remove-button";
  }

  $attributes = array();

  if (!empty($element['#attributes']['class'])) {
    $attributes['class'] = (array) $element['#attributes']['class'];
  }
  $attributes['class'][] = 'file-widget';
  $attributes['class'][] = 'form-managed-file';

  $element['upload_button']['#ajax']['progress'] = array('type' => 'none');

  $output = '';
  $output .= '<div' . drupal_attributes($attributes) . '>';
  // Add preview.
  if ($element['fid']['#value'] != 0) {
    caffelatte_load_plugin_lightbox();
    $element['filename']['#markup'] = theme('file_link', array(
     'file' => $element['#file'],
     '#has_icon' => FALSE,
     '#attributes' => array('data-plugin-lightbox' => TRUE, 'data-plugin-lightbox-type' => 'iframe'),
    ));

    $preview = '<div class="file-preview-other-frame">';
    $preview .= '  <div class="file-preview-other">';
    $preview .= !empty($element['preview']) ? drupal_render($element['preview']) : '<div class="file-icon-4x">' . theme('file_icon', array('file' => $element['#file'])) . '</div>';
    $preview .= '  </div>';
    $preview .= '</div>';

    $vars = array(
      'preview' => $preview,
      'caption' => drupal_render($element['filename']),
    );
    if (isset($element['description'])) {
      $vars['additional'] = '<div class="row">';
      $vars['additional'] .= '<div class="col-xs-12">' . drupal_render($element['description']) . '</div>';
      $vars['additional'] .= '</div>';
    }

    $output .= theme('file_preview', $vars);
  }
  else {
    if (theme_get_setting('caffelatte_allow_fileinput_file_widget')) {
      $render_buttons = FALSE;

      caffelatte_load_plugin_fileinput();

      $allowed_extensions = isset($element['#upload_validators']['file_validate_extensions']) ? explode(" ", $element['#upload_validators']['file_validate_extensions'][0]) : array(
        'jpg',
        'jpeg',
        'gif',
        'png',
        'txt',
        'doc',
        'xls',
        'pdf',
        'ppt',
        'pps',
        'odt',
        'ods',
        'odp',
      );

      $ajax_path = $element['upload_button']['#ajax']['path'];
      $ajax_path_parts = explode('/', $ajax_path);
      $element_options = array(
        'maxFileCount' => 1,
        'autoReplace' => TRUE,
        'allowedFileExtensions' => $allowed_extensions,
        'uploadUrl' => $base_url . '/' . $ajax_path,
        'uploadExtraData' => array(
          'form_build_id' => array_pop($ajax_path_parts),
        ),
      );

      drupal_add_js(array(
        'caffelatte' => array(
          'file' => array('elements' => array('#' . $element['#id'] => $element_options)),
        ),
      ), 'setting');

      drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/managed-file.js');

      $element['upload']['#access'] = TRUE;
      $element['upload_button']['#access'] = TRUE;
      if (isset($element['filename'])) {
        $element['filename']['#access'] = FALSE;
        $element['remove_button']['#access'] = TRUE;
      }
    }
    if ($render_buttons) {
      $output .= theme('bootstrap_input_group_btn', array(
        'suffix' => drupal_render($element['upload_button']),
        'input' => drupal_render($element['upload']),
      ));
    }
  }
  $output .= drupal_render_children($element);
  $output .= '</div>';
  return $output;
}
