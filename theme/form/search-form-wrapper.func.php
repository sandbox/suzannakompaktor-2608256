<?php
/**
 * @file
 * search-form-wrapper.func.php
 */

/**
 * Theme function implementation for theme_search_form_wrapper.
 */
function caffelatte_search_form_wrapper($variables) {
  $output = '<div class="input-group input-search">';
  $output .= $variables['element']['#children'];
  $output .= '</div>';
  return $output;
}
