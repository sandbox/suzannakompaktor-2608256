<?php
/**
 * @file
 * timefield.func.php
 */

/**
 * Overrides theme_timefield().
 */
function caffelatte_timefield($variables) {
  $element = $variables['element'];
  return theme('input_tag', array('element' => $element));
}
