<?php
/**
 * @file
 * Numberield.func.php.
 */

/**
 * Overrides theme_numberfield().
 */
function caffelatte_numberfield($variables) {
  $element = $variables['element'];
  $input = theme('input_tag', array('element' => $element));

  if (theme_get_setting('caffelatte_allow_spinbox')) {
    $attributes = array(
      'data-initialize' => 'spinbox',
      'id' => 'spinbox-' . $element['#id'],
    );
    if (isset($element['#default_value'])) {
      $attributes['data-value'] = $element['#default_value'];
    }
    if (isset($element['#min'])) {
      $attributes['data-min'] = $element['#min'];
    }
    if (isset($element['#max'])) {
      $attributes['data-max'] = $element['#max'];
    }
    if (isset($element['#step'])) {
      $attributes['data-step'] = $element['#step'];
    }

    if (isset($element['#attributes'])) {
      if (isset($element['#attributes']['class'])) {
        if (in_array('input-sm', $element['#attributes']['class'])) {
          $attributes['class'][] = 'spinbox-sm';
        }

        if (in_array('input-lg', $element['#attributes']['class'])) {
          $attributes['class'][] = 'spinbox-lg';
        }
      }
    }

    if (isset($element['#disabled'])) {
      $attributes['data-disabled'] = $element['#disabled'];
    }

    $increase_icon = theme('icon', array('icon' => theme_get_setting('caffelatte_spinbox_increase_icon')));
    $decrease_icon = theme('icon', array('icon' => theme_get_setting('caffelatte_spinbox_decrease_icon')));
    $increase_text = t(theme_get_setting('caffelatte_spinbox_increase_text'));
    $decrease_text = t(theme_get_setting('caffelatte_spinbox_decrease_text'));
    return theme('spinbox', array(
          'attributes' => $attributes,
          'input' => $input,
          'increase_icon' => $increase_icon,
          'increase_text' => $increase_text,
          'decrease_icon' => $decrease_icon,
          'decrease_text' => $decrease_text,
    ));
  }

  return $input;
}
