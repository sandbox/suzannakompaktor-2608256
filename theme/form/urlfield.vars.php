<?php
/**
 * @file
 * urlfield.vars.php
 */

/**
 * Implements hook_preprocess_urlfield().
 *
 * @see template_preprocess_urlfield()
 */
function caffelatte_preprocess_urlfield(&$variables) {
  $variables['element']['#attributes']['type'] = 'url';

  element_set_attributes($variables['element'], array(
      'id',
      'name',
      'value',
      'size',
      'maxlength',
  ));
  _form_set_class($variables['element'], array('form-url'));
}
