<?php
/**
 * @file
 * form-group.tpl.php
 *
 * Variables
 * - $item: form item.
 * - $label: label of form item.
 * - $title_display: before, after, invisible, none, attribute.
 * - $group_attributes: HTML attributes for group.
 * - $attributes: HTML attributes for form-group.
 */

?>

<div<?php print $attributes ?>>
  
  <?php if(!empty($group_attributes)): ?>
  <div<?php print $group_attributes ?>>
  <?php endif; ?>

  <?php
  switch ($title_display) {
    case 'before':
      if (!empty($label)) {print $label;}
      print $item;
      break;

    case 'after':
      print $item;
      if (!empty($label)) {print $label;}
      break;

    case 'none':
    case 'attribute':
      print $item;
      break;

    default:
      print $item;
      break;

  }
  ?>

  <?php if(!empty($group_attributes)): ?>
  </div>
  <?php endif; ?>

</div>
