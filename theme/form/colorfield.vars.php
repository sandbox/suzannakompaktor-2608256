<?php
/**
 * @file
 * colorfield.vars.php
 */

/**
 * Implements hook_preprocess_colorfield().
 *
 * @see template_preprocess_colorfield()
 */
function caffelatte_preprocess_colorfield(&$variables) {
  caffelatte_load_plugin_colorpicker();

  if (!isset($variables['element']['#color_format'])) {
    $variables['element']['#color_format'] = theme_get_setting('caffelatte_colorpicker_color_format');
  }

  switch ($variables['element']['#color_format']) {
    case 'hex':
      $variables['element']['#maxlength'] = 7;
      break;

    case 'rgb':
      $variables['element']['#maxlength'] = 16;
      break;

    case 'rgba':
      $variables['element']['#maxlength'] = 20;
      break;
  }

  $variables['element']['#attributes']['type'] = 'text';
  if (!theme_get_setting('caffelatte_colorpicker_icon')) {
    $variables['element']['#attributes']['data-plugin-colorpicker'] = TRUE;
  }

  element_set_attributes($variables['element'], array(
    'id',
    'name',
    'value',
    'size',
    'maxlength',
  ));
  _form_set_class($variables['element'], array('form-color'));

  if (isset($variables['element']['#required']) && $variables['element']['#required']) {
    element_set_attributes($variables['element'], array('required'));
  }
}
