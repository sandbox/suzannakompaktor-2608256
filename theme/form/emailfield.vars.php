<?php
/**
 * @file
 * emailfield.vars.php
 */

/**
 * Implements hook_preprocess_emailfield().
 *
 * @see template_preprocess_emailfield()
 */
function caffelatte_preprocess_emailfield(&$variables) {
  $variables['element']['#attributes']['type'] = 'email';

  element_set_attributes($variables['element'], array(
      'id',
      'name',
      'value',
      'size',
      'maxlength',
  ));
  _form_set_class($variables['element'], array('form-email'));
}
