<?php
/**
 * @file
 * file.vars.php
 */

/**
 * Implements hook_preprocess_file().
 *
 * @see template_preprocess_file()
 */
function caffelatte_preprocess_file(&$variables) {
  $element = $variables['element'];

  if ($element['#file_input_type'] == 'file') {
    if (theme_get_setting('caffelatte_allow_fileinput_file')) {
      // Add Bootstrap File Input.
      caffelatte_load_plugin_fileinput();
      drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/plugins/fileinput.js', array('scope' => 'footer', 'weight' => 20));
      $variables['element']['#attributes']['data-plugin-fileinput'] = TRUE;
      $variables['element']['#attributes']['class'][] = 'fileinput';
      $variables['element']['#attributes']['class'][] = 'file-loading';
    }
  }
}
