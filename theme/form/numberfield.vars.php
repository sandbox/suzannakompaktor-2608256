<?php
/**
 * @file
 * numberfield.vars.php
 */

/**
 * Implements hook_preprocess_numberfield().
 *
 * @see template_preprocess_numberfield()
 */
function caffelatte_preprocess_numberfield(&$variables) {
  $variables['element']['#attributes']['type'] = 'number';

  element_set_attributes($variables['element'], array(
      'id',
      'name',
      'value',
      'step',
      'min',
      'max',
  ));
  _form_set_class($variables['element'], array('form-number'));

  if (theme_get_setting('caffelatte_allow_spinbox')) {
    caffelatte_load_plugin_spinner();
    $variables['element']['#attributes']['class'][] = 'spinbox-input';
  }
}
