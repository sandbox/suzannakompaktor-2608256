<?php
/**
 * @file
 * slider.vars.php
 */

/**
 * Implements hook_preprocess_slider().
 */
function caffelatte_preprocess_slider(&$variables) {
  if (isset($variables['attributes']['class'])) {
    $variables['classes'] = $variables['attributes']['class'];
    unset($variables['attributes']['class']);
  }
}

/**
 * Implements hook_process_slider().
 */
function caffelatte_process_slider(&$variables) {
  $variables['attributes'] = drupal_attributes($variables['attributes']);

  if (isset($variables['classes'])) {
    if (!empty($variables['classes'])) {
      $variables['classes'] = ' ' . implode(' ', $variables['classes']);
    }
  }

  $variables['styles'] = _caffelatte_create_styles('slider', isset($variables['styles']) ? $variables['styles'] : '', theme_get_setting('caffelatte_slider_default_style'));
}
