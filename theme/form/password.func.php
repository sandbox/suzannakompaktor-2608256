<?php
/**
 * @file
 * password.func.php
 */

/**
 * Overrides theme_password().
 */
function caffelatte_password($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'password';
  element_set_attributes($element, array('id', 'name', 'size', 'maxlength'));
  _form_set_class($element, array('form-text'));

  return theme('input_tag', array('element' => $element));
}
