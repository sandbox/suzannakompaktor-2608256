<?php
/**
 * @file
 * textarea.tpl.php
 *
 * Markup for textarea.
 *
 * Variables
 * - $wrapper_attributes: Wrapper attributes for textarea wrapper div.
 * - $attributes: HTML attributes for textarea.
 */

?>
<div<?php print $wrapper_attributes ?>>
  <textarea<?php print $attributes ?>><?php print $value ?></textarea>
</div>
