<?php
/**
 * @file
 * spinbox.tpl.php
 */

?>
<div class="fuelux">
	<div class="spinbox<?php print $classes ?>"<?php print $attributes ?>>
	    <?php print $input ?>
	    <div class="spinbox-buttons btn-group btn-group-vertical">
	      <button class="btn btn-default spinbox-up btn-xs" type="button">
	        <?php print $increase_icon ?><span class="sr-only"><?php print $increase_text?></span>
	      </button>
	      <button class="btn btn-default spinbox-down btn-xs" type="button">
	        <?php print $decrease_icon ?><span class="sr-only"><?php print $decrease_text?></span>
	      </button>
	    </div>
	  </div>
</div>
