<?php
/**
 * @file
 * datefield.func.php
 */

/**
 * Overrides theme_datefield().
 */
function caffelatte_datefield($variables) {
  $element = $variables['element'];
  $datepicker = '';

  $attributes = array();
  if (isset($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  if (!empty($element['#attributes']['class'])) {
    $attributes['class'] = (array) $element['#attributes']['class'];
  }
  $attributes['class'][] = 'form-inline';

  if ($element['#datepicker']) {
    caffelatte_load_plugin_datepicker();
    $datepicker_attributes = array(
      'data-plugin-datepicker' => TRUE,
      'type' => 'text',
    );

    $default_value = $element['#default_value'];
    if (isset($element['#datepicker_format'])) {
      $format = $element['#datepicker_format'];
      $datepicker_attributes['data-plugin-datepicker-format'] = $format;
    }
    else {
      $format = theme_get_setting('caffelatte_datepicker_format');
    }

    if (isset($element['#datepicker_skin'])) {
      $datepicker_attributes['data-plugin-skin'] = $element['#datepicker_skin'];
    }

    $php_format = _caffelatte_dateformat_js_to_php($format);
    $formatted_default = date($php_format, mktime(0, 0, 0, $default_value['month'], $default_value['day'], $default_value['year']));
    $datepicker_attributes['value'] = $formatted_default;

    $element['#type'] = 'hidden';
    $element['#value'] = $element['#default_value'];

    $date_element = array(
      '#type' => 'textfield',
      '#default_value' => $formatted_default,
      '#attributes' => $datepicker_attributes,
    );

    $attributes['class'][] = 'form-date-wrapper';
    $attributes['class'][] = 'hidden';

    $datepicker .= theme('input_tag', array('element' => $date_element));
  }

  $field = '<div' . drupal_attributes($attributes) . '>' . drupal_render_children($element) . '</div>';
  // Don't use hidden items as input groups last or first item.
  if (isset($element['#field_prefix'])) {
    $output = $field . $datepicker;
  }
  else {
    $output = $datepicker . $field;
  }

  return $output;
}
