<?php
/**
 * @file
 * form.tpl.php
 *
 * Variables
 * - $elements: Form items.
 * - $attributes: HTML attributes for <form>.
 */

?>
<form<?php print $attributes ?>>
  <?php if($has_client_validation): ?>
  <div id="<?php print $error_id ?>" class="form-error form-error-container"><div class="content"></div></div>
	<?php endif; ?>
  <?php print $elements ?>
</form>
