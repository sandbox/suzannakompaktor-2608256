<?php
/**
 * @file
 * text-format-wrapper.vars.php
 */

/**
 * Implements hook_preprocess_text_format_wrapper().
 *
 * @see template_preprocess_text_format_wrapper()
 */
function caffelatte_preprocess_text_format_wrapper(&$variables) {
  $element = $variables['element'];

  $variables['element']['format']['#attributes']['class'][] = 'form-inline';
  $variables['element']['format']['format']['#attributes']['class'][] = 'input-sm';
  $variables['element']['format']['help']['#attributes']['class'][] = 'pull-right';
  $variables['element']['format']['help']['#attributes']['class'][] = 'mb-unset';

  unset($variables['element']['format']['#children']);
  unset($variables['element']['format']['format']['#children']);
  unset($variables['element']['#children']);
  $variables['element']['#printed'] = FALSE;
  foreach (element_children($variables['element']) as $key) {
    $variables['element'][$key]['#printed'] = FALSE;
    foreach (element_children($variables['element'][$key]) as $k) {
      $variables['element'][$key][$k]['#printed'] = FALSE;
    }
  }
  $variables['element']['#children'] = drupal_render_children($variables['element']);
}
