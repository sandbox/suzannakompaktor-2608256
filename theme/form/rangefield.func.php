<?php
/**
 * @file
 * rangefield.func.php
 */

/**
 * Overrides theme_rangefield().
 */
function caffelatte_rangefield($variables) {
  $element = $variables['element'];
  $input = theme('input_tag', array('element' => $element));

  if (theme_get_setting('caffelatte_allow_slider')) {
    $attributes = array(
      'data-plugin-slider' => TRUE,
      'data-plugin-slider-tooltip' => 1,
      'data-plugin-slider-output' => '#' . $element['#id'],
    );
    if (isset($element['#default_value'])) {
      $attributes['data-plugin-slider-value'] = $element['#default_value'];
    }
    if (isset($element['#min'])) {
      $attributes['data-plugin-slider-min'] = $element['#min'];
    }
    if (isset($element['#max'])) {
      $attributes['data-plugin-slider-max'] = $element['#max'];
    }
    if (isset($element['#step'])) {
      $attributes['data-plugin-slider-step'] = $element['#step'];
    }

    return theme('slider', array(
          'attributes' => $attributes,
          'input' => $input,
      'styles' => isset($element['#styles']) ? $element['#styles'] : '',
    ));
  }

  return $input;
}
