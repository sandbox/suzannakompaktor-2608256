<?php
/**
 * @file
 * custom-radio.vars.php
 */

/**
 * Implements hook_preprocess_custom_radio().
 *
 * @see template_preprocess_custom_radio()
 */
function caffelatte_preprocess_custom_radio(&$variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'radio';
  element_set_attributes($element, array(
    'id',
    'name',
    '#return_value' => 'value',
  ));

  if (isset($element['#return_value']) && $element['#value'] !== FALSE && $element['#value'] == $element['#return_value']) {
    $element['#attributes']['checked'] = 'checked';
  }
  _form_set_class($element, array('form-radio'));

  $element['#styles'] = !empty($element['#styles']) ? $element['#styles'] : (array) theme_get_setting('caffelatte_radio_default_style');
  $variables['styles'] = _caffelatte_create_styles('radio', $element['#styles']);
  $label = '';
  if (isset($element['#title'])) {
    $label = theme('form_element_label', $variables);
  }
  $variables['label'] = !empty($label) ? $label : '<label for="' . $element['#id'] . '"></label>';
  $variables['attributes'] = drupal_attributes($element['#attributes']);
}
