<?php
/**
 * @file
 * form-element.func.php
 */

/**
 * Overrides theme_form_element().
 */
function caffelatte_form_element(&$variables) {
  caffelatte_load_plugin_form_description();

  $element = &$variables['element'];
  $attributes = isset($element['#wrapper_attributes']) ? $element['#wrapper_attributes'] : array();
  $group_attributes = isset($element['#group_attributes']) ? $element['#group_attributes'] : array();
  $label_attributes = isset($element['#label_attributes']) ? $element['#label_attributes'] : array();
  $item_attributes = isset($element['#item_attributes']) ? $element['#item_attributes'] : array();
  $output = '';

  // This function is invoked as theme wrapper, but the rendered form element
  // may not necessarily have been processed by form_builder().
  $element += array(
    '#title_display' => 'before',
  );

  // Add element #id for #type 'item'.
  if (isset($element['#markup']) && !empty($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }

  // Check for errors and set correct error class.
  if (isset($element['#parents']) && form_get_error($element)) {
    $attributes['class'][] = 'error';
  }

  if (!empty($element['#type'])) {
    $attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');
  }
  if (!empty($element['#name'])) {
    $attributes['class'][] = 'form-item-' . strtr($element['#name'], array(
        ' ' => '-',
        '_' => '-',
        '[' => '-',
        ']' => '',
      ));
  }
  // Add a class for disabled elements to facilitate cross-browser styling.
  if (!empty($element['#attributes']['disabled'])) {
    $attributes['class'][] = 'form-disabled';
  }
  if (!empty($element['#autocomplete_path']) && drupal_valid_path($element['#autocomplete_path'])) {
    $attributes['class'][] = 'form-autocomplete';
  }
  if (isset($element['#input'])) {
    $attributes['class'][] = 'form-item';
    $attributes['class'][] = 'form-group';
  }
  if (!empty($element['#attributes']['class']) && is_array($element['#attributes']['class'])) {
    if (in_array('container-inline', $element['#attributes']['class'])) {
      $attributes['class'][] = 'form-inline';
    }
  }

  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }

  $title_display = $element['#title_display'];
  // Create label.
  $label = theme('form_element_label', $variables);

  if (isset($element['#theme'])) {
    // Set empty label for custom checkboxes/radios.
    if (theme_get_setting('caffelatte_custom_checkbox') && ($element['#theme'] == 'checkbox' || $element['#theme'] == 'radio')) {
      $label = '';
    }

    if ($element['#theme'] == 'colorfield' && $element['#type'] != 'colorfield') {
      _caffelatte_process_colorfield($element);
    }
  }
  // Create field prefix/suffix.
  $prefix = '';
  $suffix = '';
  if (isset($element['#field_prefix']) || isset($element['#field_suffix'])) {
    if (isset($element['#field_prefix'])) {
      if (preg_match("/^<div/", $element['#field_prefix'])) {
        $element['#input_group'] = '';
      }
      $prefix = $element['#field_prefix'];
    }
    $suffix = (isset($element['#field_suffix']) && !empty($element['#field_suffix'])) ? trim($element['#field_suffix']) : '';
  }

  if (!empty($prefix) || !empty($suffix)) {
    if (!empty($element['#input_group'])) {
      $input_group_type = is_string($element['#input_group']) ? $element['#input_group'] : 'addon';
      // Create input group with prefix/suffix.
      $item = theme('bootstrap_input_group', array(
        'prefix' => $prefix,
        'suffix' => $suffix,
        'input' => $element['#children'],
        'attributes' => isset($element['#input_group_attributes']) ? $element['#input_group_attributes'] : '',
      ));
    }
    else {
      $item = $prefix . $element['#children'] . $suffix;
    }
  }
  else {
    $item = $element['#children'];
  }

  // Create help block.
  if (isset($element['#description'])) {
    $description = $element['#description'];
    if (isset($element['#type'])) {
      $help_block_attributes = array('data-form-description' => TRUE);
      switch ($element['#type']) {
        case 'checkboxes':
        case 'radios':
            $description_display = theme_get_setting('caffelatte_descriptions_type_default');
          $help_block_attributes['data-form-description-input'] = '#' . $element['#id'];
          break;

        case 'textfield':
        case 'textarea':
            $description_display = theme_get_setting('caffelatte_descriptions_type_textfield');
          $help_block_attributes['data-form-description-input'] = '#' . $element['#id'];
          break;

        default:
            $description_display = theme_get_setting('caffelatte_descriptions_type_default');
          $help_block_attributes['data-form-description-input'] = '#' . $element['#id'];
          break;
      }

      if ($description_display == 'after-label' && ($title_display == 'none' || $title_display == 'invisible' || $title_display == 'attribute')) {
        $help_block_attributes = array();
      }
      elseif (isset($element['#form_description_display'])) {
        $help_block_attributes['data-form-description-display'] = $element['#form_description_display'];
      }
      else {
        $help_block_attributes['data-form-description-display'] = $description_display;
      }
    }

    $help_block = theme('help_block', array(
      'description' => $description,
      'attributes' => drupal_attributes($help_block_attributes),
    ));
    $item .= $help_block;
  }

  $output = theme('form_group', array(
      'attributes' => $attributes,
      'group_attributes' => $group_attributes,
      'title_display' => $title_display,
      'label' => $label,
      'label_attributes' => $label_attributes,
      'item' => $item,
      'item_attributes' => $item_attributes,
  ));

  return $output;
}
