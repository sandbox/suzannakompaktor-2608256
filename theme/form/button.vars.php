<?php
/**
 * @file
 * button.vars.php
 */

/**
 * Implements hook_preprocess_button().
 */
function caffelatte_preprocess_button(&$variables) {
  $element = $variables['element'];
  $styles = array();
  $icon = '';
  $element['#attributes']['type'] = 'submit';
  element_set_attributes($element, array(
    'id',
    'name',
    'value',
   ));

  $element['#attributes']['class'][] = 'form-' . $element['#button_type'];
  if (!empty($element['#attributes']['disabled'])) {
    $element['#attributes']['class'][] = 'form-button-disabled';
  }
  $element['#value_attributes']['class'][] = 'button-text';

  if (isset($element['#styles'])) {
    $styles[] = $element['#styles'];
  }
  elseif (isset($style)) {
    $styles[] = $style;
  }
  // Set styles by text if no styles setted.
  if (empty($styles)) {
    $styles[] = _caffelatte_colorize_button_by_text($element['#value']);
  }

  if (isset($element['#icon'])) {
    $icon = $element['#icon'];
  }
  // Set icon by text if no icon setted.
  if (empty($icon)) {
    $icon = _caffelatte_iconize_button_by_text($element['#value']);
  }

  if (!empty($icon)) {
    $element['#value_attributes']['class'][] = 'hidden-on-overflow';
    $element['#attributes']['aria-label'] = isset($element['#value']) ? $element['#value'] : '';
  }
  $variables['styles'] = _caffelatte_create_styles('btn', $styles);

  $variables['classes'] = !empty($element['#attributes']['class']) ? ' ' . implode(' ', $element['#attributes']['class']) : '';
  unset($element['#attributes']['class']);

  $variables['value'] = isset($element['#value']) ? $element['#value'] : '';
  $variables['value_attributes'] = isset($element['#value_attributes']) ? drupal_attributes($element['#value_attributes']) : '';
  $variables['icon'] = (!empty($icon)) ? theme('icon_fw', array('icon' => $icon)) : '';
  unset($element['#value']);

  $variables['attributes'] = drupal_attributes($element['#attributes']);
}
