<?php
/**
 * @file
 * switchfield.vars.php
 */

/**
 * Implements hook_preprocess_switchfield().
 *
 * @see template_preprocess_switchfield()
 */
function caffelatte_preprocess_switchfield(&$variables) {
  drupal_add_js(drupal_get_path('theme', 'caffelatte') . '/js/switchfield.js');

  $element = $variables['element'];
  $element['#title_display'] = 'before';
  $element['#attributes']['type'] = 'checkbox';
  element_set_attributes($element, array(
    'id',
    'name',
    '#return_value' => 'value',
  ));

  // Unchecked checkbox has #value of integer 0.
  if (!empty($element['#checked'])) {
    $element['#attributes']['checked'] = 'checked';
  }
  _form_set_class($element, array('form-checkbox'));

  $variables['styles'] = _caffelatte_create_styles('switch', $element['#styles'], theme_get_setting('caffelatte_switchfield_default_style'));

  // On/Off texts to display.
  if (!array_key_exists('#switch_handle', $element)) {
    $element['#switch_handle'] = array(
      'data-content-on' => t(theme_get_setting('caffelatte_switchfield_on_text')),
      'data-content-off' => t(theme_get_setting('caffelatte_switchfield_off_text')),
    );
  }
  $variables['switch_handle'] = drupal_attributes($element['#switch_handle']);

  $variables['attributes'] = drupal_attributes($element['#attributes']);
}
