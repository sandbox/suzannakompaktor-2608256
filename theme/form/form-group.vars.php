<?php
/**
 * @file
 * form-group.vars.php
 */

/**
 * Implements hook_preprocess_form_group().
 *
 * @see template_preprocess_form_group()
 */
function caffelatte_preprocess_form_group(&$variables) {

}

/**
 * Implements hook_process_form_group().
 */
function caffelatte_process_form_group(&$variables) {
  if (!empty($variables['label_attributes'])) {
    $variables['label'] = '<div' . drupal_attributes((array) $variables['label_attributes']) . '>' . $variables['label'] . '</div>';
  }
  if (!empty($variables['item_attributes'])) {
    $variables['item'] = '<div' . drupal_attributes((array) $variables['item_attributes']) . '>' . $variables['item'] . '</div>';
  }
  if (is_array($variables['attributes'])) {
    $variables['attributes'] = drupal_attributes((array) $variables['attributes']);
  }
  if (is_array($variables['group_attributes'])) {
    $variables['group_attributes'] = drupal_attributes((array) $variables['group_attributes']);
  }
}
