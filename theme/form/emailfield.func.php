<?php
/**
 * @file
 * emailfield.func.php
 */

/**
 * Overrides theme_emailfield().
 */
function caffelatte_emailfield($variables) {
  $element = $variables['element'];
  return theme('input_tag', array('element' => $element));
}
