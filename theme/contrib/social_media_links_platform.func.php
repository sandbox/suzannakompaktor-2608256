<?php
/**
 * @file
 * social_media_links_platform.func.php
 */

/**
 * Overrides theme_social_media_links_platform().
 */
function caffelatte_social_media_links_platform($variables) {
  $output = '';

  if (theme_get_setting('caffelatte_social_media_links_block_use_caffelatte')) {
    $name = $variables['name'];
    if ($name == 'googleplus') {
      $name = 'google-plus';
    }

    if ($name == 'youtube_channel') {
      $name = 'youtube';
    }

    $options = array(
      'icon' => $name,
    );

    if ($name == 'contact' || $name == 'email') {
      $options['icon'] = 'envelope-o';
    }

    $style = theme_get_setting('caffelatte_social_media_links_block_style');

    if (!empty($style)) {
      $options['styles'][] = $style;
    }

    $icon = theme('icon', $options);
  }
  else {
    $icon = theme_image(array(
      'path' => $variables['icon_path'],
      'alt' => $variables['icon_alt'],
      'attributes' => array(),
    ));
  }

  $options = array();
  $options['attributes'] = $variables['attributes'];
  $options['html'] = TRUE;

  $output .= l($icon, $variables['link'], $options);

  if (!empty($variables['appearance']['show_name'])) {
    if ($variables['appearance']['orientation'] == 'h') {
      $output .= '<br />';
    }

    $title = check_plain($variables['attributes']['title']);
    $output .= '<span>' . l($title, $variables['link'], $options) . '</span>';
  }

  return $output;
}
