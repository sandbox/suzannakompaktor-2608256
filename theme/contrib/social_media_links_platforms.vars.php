<?php
/**
 * @file
 * social_media_links_platforms.vars.php
 */

/**
 * Implements hook_preprocess_social_media_links_platforms().
 */
function caffelatte_preprocess_social_media_links_platforms(&$variables) {
  if (theme_get_setting('caffelatte_social_media_links_block_use_caffelatte')) {
    $variables['attributes']['class'][] = 'social-icons';
    if (theme_get_setting('caffelatte_social_media_links_block_inverse')) {
      $variables['attributes']['class'][] = 'social-icons-inverse';
    }
    if (theme_get_setting('caffelatte_social_media_links_block_rounded')) {
      $variables['attributes']['class'][] = 'social-icons-rounded';
    }
  }
}
