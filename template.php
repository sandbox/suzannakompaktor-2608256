<?php
/**
 * @file
 * template.php
 *
 * Due to a bug in Drush, these includes must live inside the 'theme' folder
 * instead of something like 'includes'. If a module or theme has an 'includes'
 * folder, Drush will think it is trying to bootstrap core when it is invoked
 * from inside the particular extension's directory.
 *
 * @see https://drupal.org/node/2102287
 */

/**
 * Include common functions used through out theme.
 */
include_once dirname(__FILE__) . '/theme/common.inc';

/**
 * Implements hook_theme().
 *
 * @see bootstrap_theme()
 * @see hook_theme()
 */
function caffelatte_theme(&$existing, $type, $theme, $path) {
  // If we are auto-rebuilding the theme registry, warn about the feature.
  if (
    // Only display for site config admins.
    isset($GLOBALS['user']) && function_exists('user_access') && user_access('administer site configuration')
    && theme_get_setting('caffelatte_rebuild_registry')
    // Always display in the admin section, otherwise limit to three per hour.
    && (arg(0) == 'admin' || flood_is_allowed($GLOBALS['theme'] . '_rebuild_registry_warning', 3))
  ) {
    flood_register_event($GLOBALS['theme'] . '_rebuild_registry_warning');
    drupal_set_message(t('For easier theme development, the theme registry is being rebuilt on every page request. It is <em>extremely</em> important to <a href="!link">turn off this feature</a> on production websites.', array('!link' => url('admin/appearance/settings/' . $GLOBALS['theme']))), 'warning', FALSE);
  }

  // Custom theme hooks:
  // Do not define the `path` or `template`.
  $hook_theme = array(
    'bootstrap_alert' => array(
      'variables' => array(
        'styles' => '',
        'content' => '',
        'classes' => '',
        'attributes' => '',
      ),
    ),
    'bootstrap_badge' => array(
      'variables' => array(
        'styles' => '',
        'content' => '',
      ),
    ),
    'bootstrap_button_dropdown' => array(
      'variables' => array(
        'styles' => '',
        'label' => '',
        'direction' => 'down',
        'attributes' => array(),
        'links' => array(),
      ),
    ),
    'bootstrap_dropdown_btn' => array(
      'variables' => array(
        'styles' => '',
        'label' => '',
        'direction' => 'down',
        'attributes' => array(),
        'links' => array(),
      ),
    ),
    'bootstrap_dropdown' => array(
      'variables' => array(
        'styles' => '',
        'label' => '',
        'direction' => 'down',
        'attributes' => array(),
        'links' => array(),
      ),
    ),
    'bootstrap_input_group' => array(
      'variables' => array(
        'styles' => '',
        'prefix' => '',
        'prefix_type' => '',
        'prefix_styles' => '',
        'suffix' => '',
        'suffix_type' => '',
        'suffix_styles' => '',
        'input' => '',
        'attributes' => '',
      ),
    ),
    'bootstrap_input_group_btn' => array(
      'variables' => array(
        'styles' => '',
        'prefix' => '',
        'prefix_styles' => '',
        'suffix' => '',
        'suffix_styles' => '',
        'input' => '',
      ),
    ),
    'bootstrap_input_group_icon' => array(
      'variables' => array(
        'styles' => '',
        'prefix' => '',
        'prefix_type' => '',
        'prefix_styles' => '',
        'suffix' => '',
        'suffix_type' => '',
        'suffix_styles' => '',
        'input' => '',
      ),
    ),
    'bootstrap_label' => array(
      'variables' => array(
        'styles' => '',
        'content' => '',
      ),
    ),
    'bootstrap_panel' => array(
      'variables' => array(
        'attributes' => array(),
        'styles' => '',
        'heading' => '',
        'is_heading_transparent' => FALSE,
        'is_transparent' => FALSE,
        'content' => '',
        'footer' => '',
        'contextual_links' => array(),
      ),
    ),
    'bootstrap_panel_heading' => array(
      'variables' => array(
        'title' => NULL,
        'subtitle' => NULL,
        'actions' => '',
      ),
    ),
    'bootstrap_well' => array(
      'variables' => array(
        'styles' => '',
        'content' => '',
        'classes' => '',
        'attributes' => '',
      ),
    ),
    'ui_accordation' => array(
      'variables' => array(
        'elements' => array('title' => '', 'content' => ''),
        'attributes' => '',
        'accordation_id' => '',
      ),
    ),
    'ui_modal' => array(
      'variables' => array(
        'id' => '',
        'styles' => '',
        'anim' => '',
        'heading' => '',
        'icon' => NULL,
        'content' => '',
        'footer' => '',
      ),
    ),
    'form_group' => array(
      'variables' => array(
        'attributes' => '',
        'group_attributes' => '',
        'title_display' => '',
        'label' => '',
        'label_attributes' => '',
        'item' => '',
        'item_attributes' => '',
      ),
    ),
    'input_group' => array(
      'render element' => 'element',
    ),
    'input_tag' => array(
      'render element' => 'element',
    ),
    'urlfield' => array(
      'render element' => 'element',
    ),
    'emailfield' => array(
      'render element' => 'element',
    ),
    'telfield' => array(
      'render element' => 'element',
    ),
    'colorfield' => array(
      'render element' => 'element',
    ),
    'numberfield' => array(
      'render element' => 'element',
    ),
    'rangefield' => array(
      'render element' => 'element',
    ),
    'datefield' => array(
      'render element' => 'element',
    ),
    'timefield' => array(
      'render element' => 'element',
    ),
    'searchfield' => array(
      'render element' => 'element',
    ),
    'search_form_wrapper' => array(
      'render element' => 'element',
    ),
    'custom_checkbox' => array(
      'render element' => 'element',
      'variables' => array(
        'styles' => '',
        'attributes' => '',
        'label' => '',
      ),
    ),
    'custom_radio' => array(
      'render element' => 'element',
      'variables' => array(
        'styles' => '',
        'attributes' => '',
        'label' => '',
      ),
    ),
    'switchfield' => array(
      'render element' => 'element',
    ),
    'spinbox' => array(
      'variables' => array(
        'classes' => '',
        'attributes' => '',
        'input' => '',
        'increase_icon' => '',
        'increase_text' => '',
        'decrease_icon' => '',
        'decrease_text' => '',
      ),
    ),
    'slider' => array(
      'variables' => array(
        'styles' => '',
        'classes' => '',
        'attributes' => '',
        'input' => '',
      ),
    ),
    'help_block' => array(
      'variables' => array(
        'description' => '',
        'attributes' => '',
      ),
    ),
    'icon' => array(
      'variables' => array(
        'icon' => '',
        'styles' => '',
        'attributes' => array(),
      ),
    ),
    'icon_fw' => array(
      'variables' => array(
        'icon' => '',
        'styles' => '',
        'attributes' => array(),
      ),
    ),
    'fileinput_file_preview' => array(
      'variables' => array(
        'file' => '',
      ),
    ),
    'file_preview' => array(
      'variables' => array(
        'preview' => '',
        'caption' => '',
        'buttons' => NULL,
        'additional' => NULL,
      ),
    ),
    'go_up' => array(
      'variables' => array(),
    ),
  );

  // Process custom. This should be used again for any sub-themes.
  caffelatte_hook_theme_complete($hook_theme, $theme, $path . '/theme');

  // Process existing registry. Only invoke once from base theme.
  caffelatte_hook_theme_complete($existing, $theme, $path . '/theme');

  return $hook_theme;
}

/**
 * Discovers and fills missing elements in the theme registry.
 *
 * Adds the following:
 *  - `includes` `*.vars.php` if variables file is found.
 *  - `includes` `*.func.php` if function file is found.
 *  - `function` if the function for $theme is found.
 *  - `path` if template file is found.
 *  - `template` if template file is found.
 */
function caffelatte_hook_theme_complete(&$registry, $theme, $path) {
  $registry = drupal_array_merge_deep(
    $registry,
    caffelatte_find_theme_includes($registry, '.vars.php', $path),
    caffelatte_find_theme_includes($registry, '.func.php', $path),
    drupal_find_theme_functions($registry, array($theme)),
    drupal_find_theme_templates($registry, '.tpl.php', $path)
  );

  // Post-process the theme registry.
  foreach ($registry as $hook => $info) {
    // Core find functions above does not carry over the base `theme path` when
    // finding suggestions. Add it to prevent notices for `theme` calls.
    if (!isset($info['theme path']) && isset($info['base hook'])) {
      $registry[$hook]['theme path'] = $path;
    }
    // Setup a default "context" variable. This allows #context to be passed
    // to every template and theme function.
    // @see https://drupal.org/node/2035055
    if (isset($info['variables']) && !isset($info['variables']['context'])) {
      $registry[$hook]['variables'] += array(
        'context' => array(),
      );
    }
  }
}

/**
 * Discovers and sets the path to each `THEME-HOOK.$extension` file.
 */
function caffelatte_find_theme_includes($registry, $extension, $path) {
  $regex = '/' . str_replace('.', '\.', $extension) . '$/';
  $files = drupal_system_listing($regex, $path, 'name', 0);

  $hook_includes = array();
  foreach ($files as $name => $file) {
    // Chop off the remaining extension.
    if (($pos = strpos($name, '.')) !== FALSE) {
      $name = substr($name, 0, $pos);
    }
    // Transform "-" in filenames to "_" to match theme hook naming scheme.
    $hook = strtr($name, '-', '_');
    // File to be included by core's theme function when the hook is invoked.
    // This only applies to base hooks. When hook derivatives are called
    // (those with a double "__"), it checks for the base hook, calls its
    // variable processors and ignores anything specific to the derivative.
    // Due to the way it works, It becomes redundant to give it a path that is
    // not a base hook.
    // @see https://drupal.org/node/939462
    if (isset($registry[$hook]) && !isset($registry[$hook]['base hook'])) {
      // Include the file so core can discover any containing functions.
      include_once DRUPAL_ROOT . '/' . $file->uri;
      $hook_includes[$hook]['includes'][] = $file->uri;
    }
  }

  return $hook_includes;
}

/**
 * Includes.
 */
include_once dirname(__FILE__) . '/theme/alter.inc';
include_once dirname(__FILE__) . '/theme/plugin.inc';
