<?php
/**
 * @file
 * theme-settings.php
 *
 * Provides theme settings
 *
 * @see theme/settings.inc
 */

/**
 * Include common functions.
 */
include_once dirname(__FILE__) . '/theme/common.inc';

/**
 * Implements hook_form_FORM_ID_alter().
 */
function caffelatte_form_system_theme_settings_alter(&$form, $form_state, $form_id = NULL) {
  // Work-around for a core bug affecting admin themes.
  // @see https://drupal.org/node/943212
  if (isset($form_id)) {
    return;
  }
  // Include theme settings file.
  caffelatte_include('caffelatte', 'theme/settings.inc');
  // Alter theme settings form.
  _caffelatte_settings_form($form, $form_state);
}
